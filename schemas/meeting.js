var Joi = require('joi');
//note: commented fields cannot be send in creation of commitment.
module.exports = Joi.object().required().keys({
    authorityName: Joi.string().required(),
    authorityRole: Joi.string(),
    meetingTitle: Joi.string(),
    country: Joi.string(),
    shortSummary: Joi.string(),
    deadline: Joi.date(),
    agenda: Joi.array().items({
        title: Joi.string(),
        description: Joi.string()
    }),
    commitments: Joi.array().items({
        title: Joi.string().required(),
        assignedTo: Joi.string().required(),
        assignedBy: Joi.string(),
        deadline: Joi.date(),
        urgency: Joi.boolean().default(false),
        category: Joi.string().required(),
        meetingAssociated: Joi.string(),
        status: Joi.string().default("ONGOING"),
        tags: Joi.array().items(Joi.string())
    })
    
    
});