var Joi = require('joi');
//note: commented fields cannot be send in creation of commitment.
module.exports = Joi.object().required().keys({
    authorityName: Joi.string(),
    authorityRole: Joi.string(),
    meetingTitle: Joi.string(),
    country: Joi.string(),
    shortSummary: Joi.string(),
    deadline: Joi.date(),
    agenda: Joi.array().items({
        title: Joi.string(),
        description: Joi.string()
    })
});