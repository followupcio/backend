var Joi = require('joi');
//note: commented fields cannot be send in creation of commitment.
module.exports = Joi.object().required().keys({    
    username: Joi.string().required(),
    password: Joi.string().required()
});