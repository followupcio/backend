var Joi = require('joi');

module.exports = Joi.object().required().keys({
    //commitmentId: Joi.string().required(),
    parentId: Joi.string(),
    text: Joi.string().required()
});