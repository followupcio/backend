var Joi = require('joi');

module.exports = Joi.object().required().keys({
    trustedUser: Joi.object({key: Joi.string()}).required()
});