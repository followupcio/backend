var Joi = require('joi');
//note: commented fields cannot be send in creation of commitment.
module.exports = Joi.object().required().keys({
    // _id: Joi.string(),
    title: Joi.string().required(),
    // assignedTo: Joi.object({ key: Joi.string().allow(null), email: Joi.string().email() }).required(),
    assignedTo: Joi.string(),
    assignedBy: Joi.string(),
    deadline: Joi.date(),
    agendaPoint: Joi.string(),
    // completedDate: Joi.date(),
    urgency: Joi.boolean().default(false),
    category: Joi.string().required(),
    subcategory: Joi.string(),
    meetingAssociated: Joi.string(),
    description: Joi.string(),
    status: Joi.string().default("ONGOING"),
    source: Joi.string().default("STAFF"),
    // finalApproval: Joi.boolean().default(false),
    tags: Joi.array().items(Joi.string()),
    //attachments: Joi.array().items(Joi.string())
    attachmentId: Joi.string(),
    parent_id: Joi.string(),
    parent: Joi.boolean().default(false)

});