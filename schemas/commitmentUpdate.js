var Joi = require('joi');
//note: commented fields cannot be send in editing of commitment.
module.exports = Joi.object().required().keys({
    // _id: Joi.string(),
    title: Joi.string(),
    // assignedTo: Joi.object({ key: Joi.string().allow(null), email: Joi.string().email() }),
    assignedTo: Joi.string().required(),
    assignedBy: Joi.string(),
    updatedBy: Joi.string().required(),
    deadline: Joi.date(),
    completedDate: Joi.date(),
    urgency: Joi.boolean(),
    category: Joi.string(),
    subcategory: Joi.string().allow(null),
    meetingAssociated: Joi.string().allow(null),
    status: Joi.string(),
    finalApproval: Joi.boolean(),
    tags: Joi.array().items(Joi.string())
   
});