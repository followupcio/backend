var Joi = require('joi');

module.exports = Joi.object().required().keys({
    trustedUser: Joi.object({ key: Joi.string().allow(null), email: Joi.string().email() }).required()
});