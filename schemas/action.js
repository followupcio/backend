var Joi = require('joi');
//note: commented fields cannot be send in creation of commitment.
module.exports = Joi.object().required().keys({    
    assignedTo: Joi.string(),
    createdBy: Joi.string().required(),
    type: Joi.string().insensitive().valid('SUBMIT', 'REJECT', 'REOPEN', 'APPROVE', 'COMMENT', 'DELETE' ).required(),
    description: Joi.string(), 
    attachmentId: Joi.string()
});