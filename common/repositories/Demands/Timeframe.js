let moment = require("moment");

class Timeframe {

  static calculate(model, today) {

    let varTimeframe;
    today = today || new Date();
    let startOfToday = moment(today).startOf('day');
    let deadline = moment(model.deadline).startOf('day');
    let diff = startOfToday.diff(deadline, 'days');

    if ( -2 <= diff && diff <= 0 ) {
      varTimeframe = "SOON_TO_EXPIRE";
    } else if (diff > 0) {
      varTimeframe = "EXPIRED";
    } else if (diff < 0) {
      varTimeframe = "ON_TIME";
    }

    return varTimeframe;

  }

  /**
   * Given the Demand's deadline
   * It calculates it's status
   * @return {[type]} [description]
   */
  static get(Demand) {

    if (Demand.status !== 'ONGOING') {
      return Demand.status;
    }

    return this.calculate(Demand);

  }
}

module.exports = Timeframe;
