let App = req('server/server');
let Promise = require('bluebird');
let Event = req('common/repositories/Events/Event');
let Email = req('common/repositories/Emails/Email');
let moment = require('moment');
let _ = require('lodash');

const includeWithDemand = {
  include: [
    {relation: "issuer"},
    {relation: "receiver"},
    {relation: "readBy"},
    {relation: "iterations"},
    {relation: "loadedBy"},
    //{relation: "attachments"},
    {
      relation: "children", scope: {
        include: [
          //{relation: "attachments"},
          {relation: "issuer"},
          {relation: "iterations"},
          {relation: "readBy"},
          {relation: "receiver"},
          {relation: "children"},
          {relation: "requester"},
          {relation: "category"},
          {relation: "subcategory"},
          {relation: "events", scope: {include: ["author", "attachments"]}},
        ]
      }
    },
    {relation: "requester"},
    {relation: "category"},
    {relation: "subcategory"},
    {relation: "events", scope: {include: ["author", "attachments", "iteration"]}},
  ]
};

class Demand {

  /**
   * [constructor description]
   * @param  {[type]} id          [description]
   * @param  {[type]} author_id   [description]
   * @param  {[type]} description [description]
   * @return {[type]}             [description]
   */
  constructor(id, author_id, description) {
    this.id = id;
    this.author_id = author_id;
    this.description = description;
  }

  /**
   * Given the Demand's deadline
   * It calculates it's status
   * @return {[type]} [description]
   */
  read() {
    return new Promise((resolve, reject) => {
      // Set reader Object
      let reader = {
        id: this.author_id,
        last_read: new Date().getTime()
      };

      if (!this.id) {
        resolve();
        return;
      }
      this.find(this.id)
        .then((demand) => {

          if (!demand) {
            resolve();
            return;
          }
          //Destroy previous relation
          demand.readBy.destroy(this.author_id);
          //Create the new Read
          demand.readBy.create(reader);
          // Set the read attribute if needed
          if (demand.assigned_to === this.author_id && !demand.read) {
            this.updateAttributes({read: new Date().getTime()}).then((model) => {
              this.trackEvent({type: "READ"}).then((updatedModel) => {
                resolve(updatedModel)
              })
            })
          } else {
            this.updateAttributes({}).then((updatedModel) => {
              resolve(updatedModel)
            })
          }
        })
    })

  }

  /**
   * Given the Demand's deadline
   * It calculates it's status
   * @return {[type]} [description]
   */
  trackLoad() {
    return new Promise((resolve, reject) => {
      // Set reader Object
      let loader = {
        id: this.author_id,
        last_load: new Date().getTime()
      };
      this.find(this.id)
        .then((demand) => {
          //Destroy previous relation
          demand.loadedBy.destroy(this.author_id);
          //Create the new Loader
          demand.loadedBy.create(loader);
          this.updateAttributes({})
            .then((updatedModel) => {
              resolve(updatedModel)
            })
        })
    })

  }

  /**
   * [submit description]
   * @return {[type]} [description]
   */
  submit() {
    return this.updateAttributes({status: "ONGOING", submitted: true})
  }

  /**
   * [approve description]
   * @return {[type]} [description]
   */
  approve(event) {

    return this.find()
      .then(parent => {

        let promises = [this.updateAttributes({status: "COMPLETED", submitted: false})];

        return parent.children.find()
          .then(result => {
            const children = JSON.parse(JSON.stringify(result)) || [];
            children.forEach(c => {

              let demand = new Demand(c.id, this.author_id);

              let p = demand.find()
                .then(u => {
                  //update only children that are ongoing
                  if (u.status === "ONGOING") {
                    return u
                      .updateAttributes({status: "COMPLETED", submitted: false})
                      .then(x => {

                        x = new Demand(x.id, this.author_id);

                        x.trackEvent(event)


                      })
                  }
                });

              promises.push(p);

            });

            return Promise.all(promises)
              .then(() => {
                return this.find()
              })

          })
      })
  }

  /**
   * [delete description]
   * @return {[type]} [description]
   */
  delete() {

    return this.find()
      .then(parent => {

        let promises = [this.updateAttributes({status: "DELETED", submitted: false})];

        return parent.children.find()
          .then(result => {
            const children = JSON.parse(JSON.stringify(result)) || [];
            children.forEach(c => {

              let demand = new Demand(c.id, this.author_id);
              promises.push(demand.updateAttributes({status: "DELETED", submitted: false}));
              promises.push(demand.trackEvent({type: "DELETE"}));
            });

            return Promise.all(promises)
              .then(() => {
                return this.find()
              })

          })
      })
  }

  undelete() {

    return this.find()
      .then(parent => {

        let promises = [this.updateAttributes({status: "ONGOING", submitted: false})];

        return parent.children.find()
          .then(result => {
            const children = JSON.parse(JSON.stringify(result)) || [];
            children.forEach(c => {

              let demand = new Demand(c.id, this.author_id);
              promises.push(demand.updateAttributes({status: "ONGOING", submitted: false}));
              promises.push(demand.trackEvent({type: "UNDELETE"}));
            });

            return Promise.all(promises)
              .then(() => {
                return this.find()
              })

          })

      })
  }


  /**
   * [archive description]
   * @return {[type]} [description]
   */
  archive() {

    return this.find()
      .then(parent => {

        let promises = [this.updateAttributes({status: "ARCHIVED", submitted: false})];

        return parent.children.find()
          .then(result => {
            const children = JSON.parse(JSON.stringify(result)) || [];
            children.forEach(c => {

              let demand = new Demand(c.id, this.author_id);
              promises.push(demand.updateAttributes({status: "ARCHIVED", submitted: false}));
              promises.push(demand.trackEvent({type: "ARCHIVE"}));
            });

            return Promise.all(promises)
              .then(() => {
                return this.find()
              })

          })

      });

  }

  unarchive() {

    return this.find()
      .then(parent => {

        let promises = [this.updateAttributes({status: "COMPLETED", submitted: false})];

        return parent.children.find()
          .then(result => {
            const children = JSON.parse(JSON.stringify(result)) || [];
            children.forEach(c => {

              let demand = new Demand(c.id, this.author_id);
              promises.push(demand.updateAttributes({status: "COMPLETED", submitted: false}));
              promises.push(demand.trackEvent({type: "UNARCHIVE"}));
            });

            return Promise.all(promises)
              .then(() => {
                return this.find()
              })

          })

      });

  }

  /**
   * [reject description]
   * @return {[type]} [description]
   */
  reject() {
    return this.updateAttributes({status: "ONGOING", submitted: false})
      .then(() => {
        return this.find()
      })
  }

  /**
   * [reopen description]
   * @return {[type]} [description]
   */
  reopen() {
    return this.updateAttributes({status: "ONGOING", submitted: false})
      .then(() => {
        return this.find()
      })

  }

  /**
   * [trackEvent description]
   * @param  {[type]} obj    [description]
   * @param  {[type]} status [description]
   * @return {[type]}        [description]
   */
  trackEvent(obj, status) {

    return new Promise((resolve, reject) => {
      let event = {
        type: obj.type.toUpperCase(),
        demand_id: this.id,
        author_id: this.author_id, ///options.accessToken.userId
        description: obj.description,
        tracking: obj.tracking
      };

      Event.create(event).then(() => {

        this.find()
          .then(demand => {
            event.demand = JSON.parse(JSON.stringify(demand));
            if (status !== "DRAFT") {
              Email.send(event);
            }

            resolve(demand)
          })

      })

    })

  }

  /**
   * [updateAttributes description]
   * @param  {[type]} changes [description]
   * @return {[type]}            [description]
   */
  updateAttributes(changes) {
    let attributes = Object.assign(changes, {updated_at: moment().toISOString()});

    return new Promise((resolve, reject) => {

      this.find()
        .then(demand => {

          demand.updateAttributes(attributes)
            .then(response => {

              if (!demand.parent_id) {
                resolve(response);
                return;
              }

              this.find(demand.parent_id)
                .then(parent => {

                  return parent.children.find()
                    .then(result => {

                      const children = JSON.parse(JSON.stringify(result)) || [];
                      const amount = children.length;
                      const completed = _.filter(children, o => {
                        return o.status === "COMPLETED"
                      }).length;

                      //If each child is completed update parent
                      if (amount === completed) {

                        parent.updateAttributes({
                          status: "COMPLETED"
                        })
                          .then(() => {
                            resolve(response)
                          });

                      }
                      else {

                        if (parent.status === "COMPLETED" && !!attributes.status && attributes.status === "ONGOING") {

                          parent.updateAttributes({
                            status: "ONGOING"
                          }).then(() => {
                            resolve(response)
                          });
                        }
                        else {
                          resolve(response)
                        }
                      }
                    })
                })
            });
        })
    })
  }

  /**
   * Get Demand
   * @param  {[type]} id [description]
   * @return {[type]}    [description]
   */
  find(id, include) {

    return new Promise((resolve, reject) => {

      let Demand = App.models.Demand;

      Demand.findById(id || this.id, include || includeWithDemand)
        .then(demand => {
          if (!demand) {
            let error = new Error();
            error.message = 'Demand not found';
            reject(error)
          }
          resolve(demand)

        })
        .catch((error) => {
          reject(error)
        })


    })

  }

  activateDraft(data, authUserId) {

    const assignees = data.assigned_to.slice(0) || [];
    let payload;
    let isSingle = assignees.length <= 1;

    if (isSingle) {

      payload = Object.assign({}, data, {
        draft_assigned_to: null,
        assigned_to: assignees[0],
        parent: false,
        status: "ONGOING",
        submitted: false
      });

      return this.updateAttributes(payload)
        .then(() => {
          return this.trackEvent({type: "CREATE"})
            .then(demand => {

              Demand.createIterationsModel(demand);

              return Demand.addIterations({demand})
                .then(() => {

                  let d = new Demand(demand.id, authUserId);
                  return d.find();
                })

            })

        })

    }
    else {

      payload = Object.assign({}, data, {
        parent: true,
        draft_assigned_to: null,
        assigned_to: null,
        status: "ONGOING",
        submitted: false,
      });

      return this.updateAttributes(payload)
        .then(() => {
          return this.trackEvent({type: "CREATE"})
            .then(demand => {

              delete payload.parent;
              delete payload.id;

              payload.assigned_to = assignees;
              payload.created_by = authUserId;
              payload.status = "ONGOING";

              Demand.createIterationsModel(payload);

              return Demand.createChildrenDemands({parent: demand, model: payload, authUserId})
                .then(() => {

                  let d = new Demand(demand.id, authUserId);
                  return d.find();
                });

            })
        })
    }
  }

  /**
   * [filterByQAndTimeframe description]
   * @param  {[type]} demands   [description]
   * @param  {[type]} q         [description]
   * @param  {[type]} timeframe [description]
   * @return {[type]}           [description]
   */
  static filterByQAndTimeframe(demands, q, timeframe) {

    let result = JSON.parse(JSON.stringify(demands));

    if (Array.isArray(result) && timeframe) {
      result = result.filter(i => i.timeframe === timeframe);
    }

    if (Array.isArray(result) && q) {
      q = q.toLowerCase();

      result = result.filter(i => {

        let search = "";
        search = i.title ? search.concat(i.title) : search;
        search = i.description ? search.concat(i.description) : search;
        search = i.requester && i.requester.display_name ? search.concat(i.requester.display_name) : search;
        search = i.receiver && i.receiver.display_name ? search.concat(i.receiver.display_name) : search;
        search = i.category && i.category.label ? search.concat(i.category.label) : search;
        search = i.subcategory && i.subcategory.label ? search.concat(i.subcategory.label) : search;
        return search.toLowerCase().indexOf(q) >= 0;
      })
    }

    return result;

  };

  static getMostRecentEvent({demand, types}) {

    const events = demand.events || [];
    const ordered = [];
    const indexes = [];

    types.forEach(type => {
      const event = events.find(e => e.type === type) || {};
      let index = events.indexOf(event);
      if (index === -1) {
        index = Infinity;
      }
      ordered[index] = event;
      indexes.push(index);
    });

    const mostRecentEvent = ordered[Math.min.apply(null, indexes)] || {};

    return {
      description: mostRecentEvent.description,
      attachments: mostRecentEvent.attachments || [],
      event: mostRecentEvent
    };

  };


  // custom create

  static normalizeDemand(dirty) {

    let model = {
      id: dirty.id,
      title: dirty.title,
      description: dirty.description,
      assigned_to: dirty.assigned_to,
      deadline: dirty.deadline,
      category_id: dirty.category_id,
      subcategory_id: dirty.subcategory_id,
      created_by: dirty.created_by,
      assigned_by: dirty.assigned_by,
      parent: dirty.parent,
      parent_id: dirty.parent_id,
      status: dirty.status,
      draft_assigned_to: dirty.draft_assigned_to,
      repeat: dirty.repeat,
      weeklyFrequency: dirty.weeklyFrequency,
      monthlyFrequency: dirty.monthlyFrequency,
      repeatDates: dirty.repeatDates,
      visibleToOtherAssignees: !!dirty.visibleToOtherAssignees
    };

    return model;
  };

  static getAllIsoWeekBeforeDeadline({deadline, isoWeek}) {
    if (!moment(deadline ? deadline : "not valid").isValid()) {
      return [];
    }

    let now = moment();
    isoWeek = parseInt(isoWeek);
    deadline = moment(deadline);

    //If deadline is passed already
    if (now.diff(deadline) > 0) {
      return [];
    }

    let dates = [];

    while (now.diff(deadline) < 0) {

      if (now.day() === isoWeek) {
        dates.push(new moment(now).toISOString());
      }
      now.add(1, 'day');
    }

    return dates;

  };

  static getAllMonthDayBeforeDeadline({deadline, monthDay}) {
    if (!moment(deadline ? deadline : "not valid").isValid()) {
      return [];
    }

    let now = moment();
    monthDay = parseInt(monthDay);
    deadline = moment(deadline);

    //If deadline is passed already
    if (now.diff(deadline) > 0) {
      return [];
    }

    let dates = [];

    while (now.diff(deadline) < 0) {
      if (now.date() === monthDay) {
        dates.push(new moment(now).toISOString());
      }
      now.add(1, 'day');
    }

    return dates;

  };

  static getAllLastDayOfMonthBeforeDeadline({deadline}) {
    if (!moment(deadline ? deadline : "not valid").isValid()) {
      return [];
    }

    let endOfMonth = moment().endOf('month');

    deadline = moment(deadline);

    //If deadline is passed already
    if (endOfMonth.diff(deadline) > 0) {
      return [];
    }

    let dates = [];

    while (endOfMonth.diff(deadline) < 0) {
      dates.push(new moment(endOfMonth).toISOString());
      endOfMonth = endOfMonth.add(1, 'day').endOf('month');
    }

    return dates;

  };

  static createIterationsModel(demand) {

    let dates;

    switch (demand.repeat) {
      case "weekly" :

        if (!Array.isArray(demand.weeklyFrequency)) {
          return demand
        }

        dates = [];
        demand.weeklyFrequency.forEach(isoWeek => {
          dates = dates.concat(this.getAllIsoWeekBeforeDeadline({
            deadline: demand.deadline,
            isoWeek
          }))
        });

        demand.repeatDates = dates;

        return demand;

      case "monthly" :
        if (!Array.isArray(demand.monthlyFrequency)) {
          return demand
        }

        dates = [];

        const last = !!_.find(demand.monthlyFrequency, (i) => {
          return i === 'last'
        });

        demand.monthlyFrequency = _.without(demand.monthlyFrequency, 'last');

        demand.monthlyFrequency.forEach(monthDay => {

          dates = dates.concat(this.getAllMonthDayBeforeDeadline({
            deadline: demand.deadline,
            monthDay
          }));

        });

        if (last) {
          dates = dates.concat(this.getAllLastDayOfMonthBeforeDeadline({
            deadline: demand.deadline,
          }));
        }

        demand.repeatDates = dates;

        return demand;

      default :
        return demand;
    }


  };

  static addIterations({demand}) {

    let promises = [];

    if (demand.status && demand.status.toUpperCase() !== "DRAFT") {
      let dates = Array.isArray(demand.repeatDates) ? demand.repeatDates : [];

      dates.forEach(date => {
        promises.push(this.addIteration({demand, model: {demand_id: demand.id, deadline: date}}))
      });
    }

    return Promise.all(promises);

  };

  static addIteration({model, demand}) {
    const Iteration = App.models.iteration;
    model = Object.assign({}, model, {demand_id: demand.id});

    return Iteration.create(model);
  };

  static fetchDemand(id) {
    let Demand = App.models.Demand;
    return Demand.findById(id, includeWithDemand)
  };

  static trackCreateEvent(id, authUserId) {

    let demand = new Demand(id, authUserId);

    return demand.find()
      .then(loaded => {

        const status = loaded.status;

        if (status === "DRAFT") {
          return demand.trackEvent({type: "DRAFT"}, status)
        }
        else {
          return demand.trackEvent({type: "CREATE"}, status)
        }

      })

  }

  static createDemandPromise({model, authUserId}) {
    let Demand = App.models.Demand;
    return Demand.create(this.normalizeDemand(model))
      .then(saved => {
        return this.trackCreateEvent(saved.id, authUserId)
          .then(() => {
            return this.fetchDemand(saved.id);
          });
      })
  };

  static createSingleDemand({model, authUserId}) {

    const assignees = model.assigned_to || [];
    delete model.attachments;

    model = Object.assign({}, model, {parent: false, assigned_to: assignees[0]});
    delete model.parent_id;

    return this.createDemandPromise({model, authUserId})
      .then(demand => {
        return this.addIterations({demand})
          .then(() => {
            return this.fetchDemand(demand.id)
          })
      })

  };

  static createParentDemand({model, authUserId}) {

    const clone = Object.assign({}, model);

    clone.parent = true;
    delete clone.assigned_to;

    return this.createDemandPromise({model: clone, authUserId})

  };

  static createChildrenDemands({parent, model, authUserId}) {

    let promises = [];
    const assignees = model.assigned_to || [];
    const assigneesCount = assignees.length;

    for (let i = 0; i < assigneesCount; i++) {
      const child = Object.assign({}, model);
      child.parent_id = parent.id;
      child.assigned_to = child.assigned_to[i];
      child.repeatDates = model.repeatDates;


      promises.push(
        this.createDemandPromise({model: child, authUserId})
          .then(saved => {
            return this.addIterations({demand: saved})
          }))
    }

    return Promise.all(promises);
  };

  static smartCreate(model, authUserId) {

    const assignees = model.assigned_to || [];
    const assigneesCount = assignees.length;
    const isSingleDemand = assigneesCount <= 1;

    model.created_by = authUserId;
    this.createIterationsModel(model);

    //create a single demand
    if (isSingleDemand) {
      return this.createSingleDemand({model, authUserId})
    }

    else {

      /**
       *  create multiple demand
       *  1 - create a fake parent
       *  2 - add parent.id to each child
       */
      return this.createParentDemand({model, authUserId})
        .then(parent => {

          if (parent.status.toUpperCase() === "DRAFT") {
            return parent;
          }

          return this.createChildrenDemands({parent, model, authUserId})
            .then(() => {
              return this.fetchDemand(parent.id)
            });
        })
    }


  }

  // Update

  updateAttributesAndTrackChange({newValues, oldValues, changedFields, authUserId}) {

    return this.updateAttributes(newValues)
      .then(result => {

        let promises = [];
        let demand = new Demand(this.id, authUserId);
        let type;

        //Track update event
        if (result.status && result.status !== "DRAFT") {
          type = "UPDATE";
        }
        else {
          type = "DRAFT_UPDATE";
        }

        promises.push(demand.trackEvent({
          type: type,
          tracking: {
            previous: oldValues,
            next: newValues,
            diff: changedFields
          }
        }));

        return Promise.all(promises)

      })
  }

  smartUpdate(data, authUserId) {

    return this.find(null, {})
      .then(found => {

        found = JSON.parse(JSON.stringify(found));
        let newValues = {};
        let oldValues = {};
        let changedFields = _.reduce(data, function (result, value, key) {

          let newV = value;
          let oldV = found[key];
          let areEqual;

          try {
            if (moment(newV).isValid()) {
              areEqual = moment(newV).unix() === moment(oldV).unix();
            }
            else {
              areEqual = _.isEqual(newV, oldV);
            }
          } catch (e) {
            // no handled
          }

          if (areEqual || (typeof newV !== 'boolean' && !newV)) {
            return result
          } else {
            newValues[key] = newV;
            oldValues[key] = oldV;
            return result.concat(key);
          }


        }, []);

        return this.updateAttributesAndTrackChange({
          newValues,
          oldValues,
          changedFields,
          authUserId
        })
          .then(demands => {
            let promises = [];
            demands = JSON.parse(JSON.stringify(demands));

            demands.forEach(d => {

              if (Array.isArray(d.children)) {
                d.children.forEach(c => {
                  let child = new Demand(c.id, authUserId);
                  promises.push(child.updateAttributesAndTrackChange({
                    newValues,
                    oldValues,
                    changedFields,
                    authUserId
                  }))

                })
              }

            });


            return Promise.all(promises).then(() => {
              return this.find(this.id)
            })
          })
      })
  }


}


module.exports = Demand;
