let Promise = require('bluebird');
let App = req('server/server');
let moment = require("moment");
class GetExpired {

  static get() {
    let Demand = App.models.demand;
    return new Promise((resolve, reject) => {

      let queryJson = {status: 'ONGOING'};

      Demand.find({
        where: queryJson, include: [
          {relation: "receiver"},
          {relation: "children"},
          {relation: "requester"},
          {relation: "category"},
          {relation: "subcategory"},
          {relation: "events", scope: {include: ["author", "attachments"]}}
        ]
      }).then(function (result) {

        result = result.filter(i => i.timeframe === 'EXPIRED');
        result = result.filter(i => !i.submitted);
        result = result.filter(i => !i.parent);

        console.log('# of demands EXPIRED:', result.length);

        resolve(result);
      });
    })
  }
}
module.exports = GetExpired;
