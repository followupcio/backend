let Promise = require('bluebird');
let App = req('server/server')
let moment = require('moment');

class GetExpireToday {

  static get() {
    let Demand = App.models.demand;
    return new Promise((resolve, reject) => {

      let queryJson = {status: 'ONGOING'};
      let assignee = [];

      Demand.find({
        where: queryJson, include: [
          {relation: "receiver"},
          {relation: "children"},
          {relation: "requester"},
          {relation: "category"},
          {relation: "subcategory"},
          {relation: "events", scope: {include: ["author", "attachments"]}}

        ]
      }).then(function (result) {

        let startOfToday = moment().startOf('day');

        result.forEach(function (commitment) {

          let startOfDeadline = moment(commitment.deadline).startOf('day');
          let expiresToday = startOfToday.isSame(startOfDeadline, "day");

          if (expiresToday && !commitment.submitted && !commitment.parent) {
            assignee.push(commitment);
          }
        });

        console.log('# of demands TODAY:', assignee.length);

        resolve(assignee);

      });

    })
  }
}
module.exports = GetExpireToday;
