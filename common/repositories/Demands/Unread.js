let _ = require('lodash')
let moment = require('moment')

class Unread {

  static getUnread(Demand, userId) {
    //
    Demand = JSON.parse(JSON.stringify(Demand));

    let lastRead = _.filter(Demand.readBy, function (user) {
      return user.id === userId;
    });

    lastRead = lastRead.length === 0 ? 0 : lastRead[0].last_read;

    let unreadEvents = _.filter(Demand.events, function (event) {
      let eventCreated = moment(event.created_at);
      lastRead = moment(lastRead);
      return eventCreated.diff(lastRead) > 0 && event.author_id !== userId
    });

    let gruppedUnread = _.groupBy(unreadEvents, 'type');

    return gruppedUnread

  }

  /**
   * Given the Demand's deadline
   * It calculates it's status
   * @return {[type]} [description]
   */
  static get(Demand, userId) {
    return this.getUnread(Demand, userId);

  }

  /**
   * [getGrouped description]
   * @param  {[type]} Demands [description]
   * @return {[type]}         [description]
   */
  static getGrouped(Demands) {
    let unreadCount = _.reduce(Demands, function (totalCount, demand) {
      let count = 0;
      _.forEach(demand.unread, function (category) {
        count = count + _.reduce(category, function (eventCount) {
            return eventCount + 1;
          }, 0);
      });

      return totalCount + count;

    }, 0);

    let groupedUnreadEvents = [];
    _.forEach(Demands, function (demand) {
      if (!(_.isEmpty(demand.unread))) {
        groupedUnreadEvents.push(demand.unread)
      }
    });


    let mergedUnreadEvents = [];
    _.forEach(groupedUnreadEvents, function (events, key) {
      if (key + 1 <= groupedUnreadEvents.length) {
        mergedUnreadEvents = _.merge(events, mergedUnreadEvents)
      }
    });

    return {
      amount: unreadCount,
      events: mergedUnreadEvents
    }

  }
}
module.exports = Unread;
