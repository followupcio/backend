class Stats {
  /**
   * Given the Demand's deadline
   * It calculates it's status
   * @return {[type]} [description]
   */
  static getTimeFrameCount({employee, authUserId, toFilter}) {
    let demands = JSON.parse(JSON.stringify(employee)).received || [];

    if (!!toFilter) {
      demands = demands.filter(d => d.created_by === authUserId || d.assigned_by === authUserId || d.assigned_to === authUserId);
    }
    let stats = {};
    stats.submitted = demands.filter(d => !!d.submitted).length;
    stats.expired = this.countTimeFrame(demands, "EXPIRED" );
    stats.soonToExpire = this.countTimeFrame(demands, "SOON_TO_EXPIRE" );
    stats.onTime = this.countTimeFrame(demands, "ON_TIME" );

    return stats

  }


  /** Reducer to count */
   static countTimeFrame( demands, timeframe) {
     return demands
       .filter(d => !d.submitted)
       .reduce((total, d)=>{
       return d.timeframe === timeframe ? total + 1 : total
     }, 0)
   }

  /** Reducer to count */
  static countSubmitted( demands) {
    return demands.filter(d => !!d.submitted).length
  }

}


module.exports = Stats;





