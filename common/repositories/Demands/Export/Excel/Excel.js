var Exceljs = require('exceljs');
const uuidV4 = require('uuid/v4');
const Promise = require("bluebird");
let moment = require('moment');
let _ = require('lodash');
let getMostRecentEvent = req("common/getMostRecentEvent");

class Excel {

  static export(demands) {

    return new Promise((resolve, reject) => {
      demands = JSON.parse(JSON.stringify(demands));
      //Build the excel from those demands
      var workbook = new Exceljs.Workbook();
      var worksheet = workbook.addWorksheet('Demands Report');

      worksheet.columns = [
        {header: 'MULTIPLE ASSIGNEES', key: 'multipleassignees', width: 10},
        {header: 'CREATED BY', key: 'createdby', width: 30},
        {header: 'FOLLOWED UP BY', key: 'assignedby', width: 30},
        {header: 'ASSIGNED TO', key: 'assignedto', width: 30},
        {header: 'STATUS', key: 'status', width: 20},
        {header: 'TITTLE OF THE ASSIGNMENT', key: 'title', width: 100},
        {header: 'DESCRIPTION OF THE ASSIGNMENT', key: 'description', width: 100},
        {header: 'DEADLINE', key: 'deadline', width: 15},
        {header: 'CATEGORY', key: 'category', width: 20},
        {header: 'SUB-CATEGORY', key: 'subcategory', width: 20},
        {header: 'CREATION DATE', key: 'createdat', width: 15},
        {header: 'READ AT', key: 'readat', width: 15},
        {header: 'SUBMITTED AT', key: 'submittedat', width: 15},
        {header: 'COMPLETED AT', key: 'completedat', width: 15}
      ];

      const createRow = (demand) =>  {
        let category, subcategory = '';

        if (demand.category && demand.category.label) {
          category = demand.category.label
        }
        if (demand.subcategory && demand.subcategory.label) {
          subcategory = demand.subcategory.label
        }

        let completed = _.filter(demand.events, {'type': 'APPROVE'});
        if (completed.length === 0) {
          completed = "NOT APPROVED"
        } else {
          completed = moment(new Date(completed[0].created_at)).format('DD MMMM YY')
        }

        let submitted = _.filter(demand.events, {'type': 'SUBMIT'}) || [];
        submitted = submitted.sort((a, b) => {
          if (a.created_at > b.created_at)
            return -1;
          if (a.created_at < b.created_at)
            return 1;
          return 0;

        });
        submitted = submitted.length > 0 ? moment(new Date(submitted[0].created_at)).format('DD MMMM YY') : "NOT SUBMITTED";

        let read = getMostRecentEvent({
          demand : demand,
          types : ["READ"]
        }).event || {};

        //Format the row
        let row = {
          multipleassignees: demand.parent_id ? "YES" : "NO",
          createdby: demand.issuer.display_name,
          assignedby: (demand.requester && demand.issuer && demand.requester.id !== demand.issuer.id) ? demand.requester.display_name : "",
          assignedto: demand.receiver ? demand.receiver.display_name : "Multiple responsible",
          status: demand.timeframe,
          title: demand.title,
          description: demand.description,
          deadline: moment(new Date(demand.deadline)).format('DD MMMM YY'),
          category: category,
          subcategory: subcategory,
          createdat: moment(new Date(demand.created_at)).format('DD MMMM YY'),
          completedat: completed,
          submittedat: submitted,
          readat : read.created_at ? moment(new Date(read.created_at)).format('DD MMMM YY') : "",
        };

        worksheet.addRow(row);

      };

      //Create the Rows
      demands.map((demand) => {

        if (!demand.parent) {
          createRow(demand);
        } else {

          const children = demand.children || [];
          children.forEach(createRow)
        }

      });

      //Generate the excel file in the container
      let filename = uuidV4() + '_report.xlsx';
      workbook.xlsx.writeFile('./files/demands/' + filename)
        .then((file) => {
          const url = 'containers/demands/download/' + filename;
          //Return the location of the file
          resolve({url: url})
        })
        .catch((error) => {
          reject(error)
        });

    })


  }

}

module.exports = Excel;
