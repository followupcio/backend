let Wso2 = req('common/repositories/Auth/SupportedMethods/Wso2');
let Promise = require('bluebird');

class Auth {
    /**
     * It triggers the Wso2 login only In production
     * @param  {[type]} credentials [description]
     * @return {[type]}              [description]
     */
    static attempt(credentials) {
        return new Promise((resolve, reject) => {
            //Dont ask for credentials in DEV
            if (process.env.APP_ENV != 'production') {
                resolve(true)
            }
            Wso2.login(credentials).then((val) => {
                resolve(val)
            }).catch((error) => {
                reject(error)
            })
        })
    }
}
module.exports = Auth;
