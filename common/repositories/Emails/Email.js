let commentedEmail = req('mail/Demands/Commented');
let readEmail = req('mail/Demands/Read');
let rejectedEmail = req('mail/Demands/Rejected');
let reopenedEmail = req('mail/Demands/Reopened');
let approvedEmail = req('mail/Demands/Approved');
let submittedEmail = req('mail/Demands/Submitted');
let createdEmail = req('mail/Demands/Created');
let deletedEmail = req('mail/Demands/Deleted');

class Email {

  static send(event) {

    //console.log('The event is :' + JSON.stringify(event));
    switch (event.type) {
      case 'READ':
        //console.log('Sending read email');
        //(new readEmail(event)).send();
        break;
      case 'APPROVE':
        (new approvedEmail(event)).send();
        break;
      case 'SUBMIT':
        (new submittedEmail(event)).send();
        break;
      case 'REOPEN':
        (new reopenedEmail(event)).send();
        break;
      case 'REJECT':
        (new rejectedEmail(event)).send();
        break;
      case 'COMMENT':
        (new commentedEmail(event)).send();
        break;
      case 'CREATE':
        if (!event.demand.parent) {
          (new createdEmail(event)).send();
        }
        break;
      case 'DELETE':
        //console.log('Sending DELETE email');
        //(new deletedEmail(event)).send();
        break;
      case 'ITERATION_APPROVE':
        (new approvedEmail(event)).send();
        break;
      case 'ITERATION_SUBMIT':
        (new submittedEmail(event)).send();
        break;
      case 'ITERATION_REOPEN':
        (new reopenedEmail(event)).send();
        break;
      case 'ITERATION_REJECT':
        (new rejectedEmail(event)).send();
        break;

    }
  }

}

module.exports = Email;
