let App = req('server/server');
let Promise = require('bluebird');
let Event = req('common/repositories/Events/Event');
let Email = req('common/repositories/Emails/Email');
let Demand = req('common/repositories/Demands/Demand');
let moment = require("moment");

class Iteration {

  /**
   * [constructor description]
   * @param  {[type]} id          [description]
   * @param  {[type]} author_id   [description]
   * @return {[type]}             [description]
   */
  constructor(id, author_id) {
    this.id = id;
    this.author_id = author_id;
  }


  /**
   * [submit description]
   * @return {[type]} [description]
   */
  submit() {
    return this.updateAttributes({status: "ONGOING", submitted: true})
  }

  /**
   * [approve description]
   * @return {[type]} [description]
   */
  approve() {
    return this.updateAttributes({status: "COMPLETED", submitted: false})
  }

  /**
   * [delete description]
   * @return {[type]} [description]
   */
  delete() {
    return this.updateAttributes({status: "DELETED", submitted: false})
  }

  /**
   * [reject description]
   * @return {[type]} [description]
   */
  reject() {
    return this.updateAttributes({status: "ONGOING", submitted: false})
  }

  /**
   * [reopen description]
   * @return {[type]} [description]
   */
  reopen() {
    return this.updateAttributes({status: "ONGOING", submitted: false})
  }

  /**
   * [trackEvent description]
   * @param  {[type]} obj    [description]
   * @param  {[type]} status [description]
   * @return {[type]}        [description]
   */
  trackEvent(obj, status) {

    return new Promise((resolve, reject) => {
      let event = {
        type: obj.type.toUpperCase(),
        iteration_id: this.id,
        demand_id: obj.demand_id,
        author_id: this.author_id,
        description: obj.description
      };

      Event.create(event).then((response) => {

        this.find()
          .then((iteration) => {

          event.iteration = JSON.parse(JSON.stringify(iteration));

          let demandInstance = new Demand(event.iteration.demand_id);

          demandInstance.find(event.iteration.demand_id)
            .then((demand) => {

            event.demand = JSON.parse(JSON.stringify(demand));
            if (status !== "DRAFT") {
              Email.send(event);
            }
            resolve(iteration)
          })


        })

      })

    })

  }

  /**
   * [updateAttributes description]
   * @param  {[type]} changes [description]
   * @return {[type]}            [description]
   */
  updateAttributes(changes) {
    let attributes = Object.assign(changes, {updated_at: moment().toISOString()});

    return new Promise((resolve, reject) => {

      this.find()
        .then((iteration) => {

          iteration.updateAttributes(attributes).then((response) => {

            resolve(response);

          });
        })
    })
  }

  /**
   * Get Iteration
   * @param  {[type]} id [description]
   * @return {[type]}    [description]
   */
  find(id) {
    let Iteration = App.models.Iteration;

    return new Promise((resolve, reject) => {

      Iteration.findById(id || this.id)
        .then((demand) => {
          if (!demand) {
            let error = new Error();
            error.message = 'Iteration not found';
            reject(error)
          }
          resolve(demand)

        })
        .catch((error) => {
          reject(error)
        })

    })
  }

}

module.exports = Iteration;
