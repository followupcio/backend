let Promise = require('bluebird');
let App = req('server/server');
let moment = require('moment');
let _ = require('lodash')

class GetExpireToday {

  static get() {

    let Iteration = App.models.iteration;
    return new Promise((resolve, reject) => {

      let queryJson = {status: 'ONGOING'};
      let demands = [];

      Iteration.find({
        where: queryJson, include: [
          {
            relation: "demand",
            scope: {
              include: [
                {relation: "issuer"},
                {relation: "receiver"},
                {relation: "requester"},
                {relation: "category"}
              ]
            }
          }
        ]
      }).then(function (result) {

        let startOfToday = moment().startOf('day');
        result = JSON.parse(JSON.stringify(result));

        result.forEach(i =>{

          let startOfDeadline = moment(i.deadline).startOf('day');
          let expiresToday = startOfToday.isSame(startOfDeadline, "day");

          if (expiresToday && !i.submitted) {
            demands.push(i);
          }
        });

        demands = _.map(demands, i => i.demand);
        demands = _.filter(demands, d => d.status === "ONGOING");
        demands = _.uniqBy(demands, d => d.id);

        console.log('# of iterations that expire TODAY:', demands.length);

        resolve(demands);
      });

    })
  }
}
module.exports = GetExpireToday;
