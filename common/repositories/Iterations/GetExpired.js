let Promise = require('bluebird');
let App = req('server/server');
let moment = require("moment");
let _ = require('lodash');

class GetExpired {

  static get() {
    let Iteration = App.models.iteration;
    return new Promise((resolve, reject) => {

      let queryJson = {status: 'ONGOING'};

      Iteration.find({
        where: queryJson, include: [
          {
            relation: "demand",
            scope: {
              include: [
                {relation: "issuer"},
                {relation: "receiver"},
                {relation: "requester"},
                {relation: "category"}
              ]
            }
          }
        ]
      }).then(function (result) {

        let startOfToday = moment().startOf('day');

        result = result.filter(i => i.timeframe === 'EXPIRED');
        result = result.filter(i => !i.submitted);

        result = result.filter(i => {

          let startOfDeadline = moment(i.deadline).startOf('day');
          let expiresToday = startOfToday.isSame(startOfDeadline, "day");
          return !expiresToday;
        });

        result = JSON.parse(JSON.stringify(result));
        let demands = _.map(result, i => i.demand);
        demands = _.filter(demands, d => d.status === "ONGOING");
        demands = _.uniqBy(demands, d => d.id);

        console.log('# of iterations EXPIRED:', demands.length);

        resolve(demands);
      });
    })
  }
}
module.exports = GetExpired;
