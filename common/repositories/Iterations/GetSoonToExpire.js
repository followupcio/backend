let Promise = require('bluebird');
let App = req('server/server');
let moment = require("moment");
let _ = require('lodash');

class GetSoonToExpire {

  static get() {

    let Iteration = App.models.iteration;
    return new Promise((resolve, reject) => {

      let queryJson = {status: 'ONGOING'};

      Iteration.find({
        where: queryJson, include: [
          {
            relation: "demand",
            scope: {
              include: [
                {relation: "issuer"},
                {relation: "receiver"},
                {relation: "requester"},
                {relation: "category"}
              ]
            }
          }
        ]
      }).then(function (result) {

        let startOfInTwoDays = moment().add(2, 'days').startOf('day');

        result = result.filter(i => i.timeframe === 'SOON_TO_EXPIRE');
        result = result.filter(i => !i.submitted);
        result = result.filter(i => {
          let startOfDeadline = moment(i.deadline).startOf('day');
          return startOfInTwoDays.isSame(startOfDeadline, "day");
        });

        let demands = JSON.parse(JSON.stringify(result));
        demands = _.map(demands, i => i.demand);
        demands = _.filter(demands, d => d.status === "ONGOING");
        demands = _.uniqBy(demands, (d) => d.id);

        console.log('# of iterations that expire SOON:', demands.length);
        resolve(demands);

      });
    })
  }
}
module.exports = GetSoonToExpire;
