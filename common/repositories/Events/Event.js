let App = req('server/server');

class Event {

  static create(event) {
    let Event = App.models.Event;
    return Event.create(event);
  }

}

module.exports = Event;
