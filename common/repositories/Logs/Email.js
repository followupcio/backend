

class Email {

  static attempt(job, content) {
    const App = req('server/server');
    const Log = App.models.Log;

    const processedJob =  Object.assign({}, JSON.parse(JSON.stringify(job)));
    const processedJobData = processedJob.data || {};
    const processedJobDataParameter = processedJobData.parameters || {};
    const logBody = Object.assign({}, {
      type: "EMAIL",
      status: "ATTEMPT",
      job: processedJob,
      mail_type: processedJobDataParameter.type,
      demand_id: processedJobDataParameter.demand_id,
      content: content
    });

    delete logBody._id;
    delete logBody.id;

    return Log.create(logBody)

  }

  static success(job, message) {
    const App = req('server/server');
    const Log = App.models.Log;
    const processedJob =  Object.assign({}, JSON.parse(JSON.stringify(job)));
    const processedJobData = processedJob.data || {};
    const processedJobDataParameter = processedJobData.parameters || {};
    const logBody = Object.assign({}, {
      type: "EMAIL",
      status: "SUCCESS",
      job: processedJob,
      message : message,
      mail_type: processedJobDataParameter.type,
      demand_id: processedJobDataParameter.demand_id
    });
    delete logBody._id;
    delete logBody.id;
    return Log.create(logBody);

  }


  static logError(job, error) {
    const App = req('server/server');
    const Log = App.models.Log;

    const processedJob = JSON.parse(JSON.stringify(job)) || {};
    const processedJobData = processedJob.data || {};
    const processedJobDataParameter = processedJobData.parameters || {};
    const logBody = Object.assign({}, {
      type: "EMAIL",
      status: "ERROR",
      error: error,
      job: processedJob,
      job_id: processedJob.id,
      mail_type: processedJobDataParameter.type,
      demand_id: processedJobDataParameter.demand_id
    });
    delete logBody._id;
    delete logBody.id;

    console.log('==========================> Error on send email')

    return Log.create(logBody)

  }

}

module.exports = Email;
