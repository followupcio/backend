module.exports = ({demand, types}) => {

  const events = demand.events || [];
  const ordered = [];
  const indexes = [];

  types.forEach(type => {
    const event = events.find(e => e.type === type) || {};
    let index = events.indexOf(event);
    if (index === -1) {
      index = Infinity;
    }
    ordered[index] = event;
    indexes.push(index);
  });

  const mostRecentEvent = ordered[Math.min.apply(null, indexes)] || {};
  return {
    description: mostRecentEvent.description,
    attachments: mostRecentEvent.attachments || [],
    event: mostRecentEvent
  };

};
