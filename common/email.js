module.exports = {
  PROJECT_NAME: process.env.MAIL_PROJECT_NAME,
  PROJECT_ACRONYM: process.env.MAIL_PROJECT_ACRONYM,
  BASE_URL: process.env.MAIL_BASE_URL,
  FROM: process.env.MAIL_FROM,
  CC: process.env.MAIL_CC,
  BCC: process.env.MAIL_BCC,
  SENDER_NAME: process.env.MAIL_SENDER_NAME,
};
