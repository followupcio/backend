'use strict';
let Timeframe = req('common/repositories/Demands/Timeframe');
let iterationModel = req('common/repositories/Iterations/Iteration');

module.exports = function (Iteration) {

  Iteration.observe('loaded', function (ctx, next) {

    // Add timeframe

    ctx.data.timeframe = Timeframe.get(ctx.data);

    next();

  });

  //demand_id

  Iteration.prototype.setReopen = function (options, callback) {

    let iteration = new iterationModel(this.id, options.accessToken.userId);

    iteration.reopen().then((model) => {
      iteration.trackEvent({type: "ITERATION_REOPEN", demand_id: model.demand_id}).then((updatedModel) => {
        callback(null, updatedModel)
      })

    })

  };

  Iteration.prototype.setApprove = function (options, data, callback) {

    let iteration = new iterationModel(this.id, options.accessToken.userId);

    iteration.approve().then((model) => {

      iteration.trackEvent({type: "ITERATION_APPROVE", description: data.description, demand_id: model.demand_id})
        .then((updatedModel) => {
          callback(null, updatedModel)
        })


    })

  };

  Iteration.prototype.setReject = function (options, data, callback) {

    let iteration = new iterationModel(this.id, options.accessToken.userId);

    iteration.reject().then((model) => {

      iteration.trackEvent({type: "ITERATION_REJECT", description: data.description, demand_id: model.demand_id}).then((updatedModel) => {
        callback(null, updatedModel)
      })


    })

  };

  Iteration.prototype.setSubmit = function (options, data, callback) {

    let iteration = new iterationModel(this.id, options.accessToken.userId);

    iteration.submit().then((model) => {
      iteration.trackEvent({type: "ITERATION_SUBMIT", description: data.description, demand_id: model.demand_id}).then((updatedModel) => {
        callback(null, updatedModel)
      })

    })

  };


};
