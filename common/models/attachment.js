'use strict';
let CONTAINERS_URL = '/api/containers/';
let CONTAINERS_FOLDER = 'demands';
let App = req("server/server");
const Promise = require("bluebird");
let getMostRecentEvent = req("common/getMostRecentEvent");


module.exports = function (Attachment) {

  Attachment.upload = function (ctx, options, cb) {
    const Container = App.models.container;
    const Demand = App.models.demand;
    const Event = App.models.event;

    if (!options) options = {};

    ctx.req.params.container = process.env.STORAGE_TYPE === "S3" ? process.env.AWS_BUCKET_NAME : CONTAINERS_FOLDER;

    Container.upload(ctx.req, ctx.result, options, function (err, fileObj) {

      if (err) {
        cb(err);
      } else {

        let fileInfo = fileObj.files.file[0];
        let demand_id = fileObj.fields.demand_id;
        let event_id = fileObj.fields.event_id;

        let attachment = {
          name: fileInfo.name,
          size: fileInfo.size,
          originalName: fileInfo.originalFilename,
          type: fileInfo.type,
          container: fileInfo.container,
          url: CONTAINERS_URL + fileInfo.container + '/download/' + fileInfo.name
        };


        let ids;
        let promise = [];

        if (demand_id) {
          ids = [];
          ids.push(demand_id);

          Demand.find({parent_id: demand_id})
            .then(children => {

              console.log("Demand found")

              if (Array.isArray(children)) {
                children.forEach(c => {
                  ids.push(c.id);
                })
              }

              ids.forEach(id => {
                promise.push(Attachment.create(Object.assign({}, attachment, {demand_id: id})))
              })

            });

        }

        if (event_id) {
          ids = [];
          ids.push(event_id[0]);

          Event.findById(event_id[0])
            .then(e => {

              const TYPE = e.type;

              Demand.findById(e.demand_id)
                .then(parent => {

                  const evt = getMostRecentEvent({
                    demand: JSON.parse(JSON.stringify(parent)),
                    types: [TYPE]
                  }).event;

                  Demand.find({
                    where: {parent_id: e.demand_id},
                    include: [{relation: "events", scope: {include: ["author", "attachments", "iteration"]}}]
                  })
                    .then(children => {

                      if (Array.isArray(children)) {
                        children.forEach(c => {
                          const evt = getMostRecentEvent({
                            demand: JSON.parse(JSON.stringify(c)),
                            types: [TYPE]
                          }).event;
                          ids.push(evt.id);
                        })
                      }

                      ids.forEach(id => {
                        promise.push(Attachment.create(Object.assign({}, attachment, {event_id: id})))
                      })

                    });
                })

            });

        }

        Promise.all(promise)
          .then(() => {
            cb(null, {status: "ok"});
          })

      }
    });
  };

  Attachment.remoteMethod(
    'upload',
    {
      description: 'Uploads a file',
      accepts: [
        {arg: 'ctx', type: 'object', http: {source: 'context'}},
        {arg: 'options', type: 'object', http: {source: 'query'}}
      ],
      returns: {
        arg: 'fileObject', type: 'object', root: true
      },
      http: {verb: 'post'}
    }
  );

};
