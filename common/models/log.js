'use strict';
let moment = require('moment');
let _ = require('lodash');

module.exports = function (Log) {
  Log.emails = function (options, callback) {
    // let currentDate = moment(new Date()).format('X');

      Log.find({
        limit: 10
      }).then((logs) => {
      let stats = [];

      logs = _.orderBy(logs, ['job.timestamp'], ['asc'])

      let gruppedLogs = _.mapValues(logs, function (log) {
        if (log.job && log.job.timestamp) {
          log.job.date = moment(log.job.timestamp).format("MMM Do YY");
        }

      });
      gruppedLogs = _.mapValues(_.groupBy(logs, 'job.date'));

      delete gruppedLogs.undefined;

      _.forEachRight(gruppedLogs, function (dayLog, index) {
        let uniqueKeys = _.uniqBy(dayLog, 'job.id');
        let details = _.countBy(dayLog, 'status');
        let failedEmails = _.filter(dayLog, {status: "ERROR"})

        stats.push({
          number_of_emails: uniqueKeys.length,
          date: index,
          details: details,
          all_email_sent: details.SUCCESS === uniqueKeys.length,
          status_message: details.SUCCESS === uniqueKeys.length ? "ALL GOOD FOR TODAY" : "SOMETHING WRONG",
          //failed: failedEmails
        })
      });

      callback(null, stats)


    })


  }

};
