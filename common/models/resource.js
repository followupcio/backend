'use strict';

module.exports = function(Resource) {

  /**
   * Clone id to value attribute
   * When it is retrieve from the DB
   * @param  {Object} ctx     [Query Element]
   * @param  {[type]} next){  ctx.data.value [description]
   * @return {[type]}         [description]
   */
  Resource.observe('loaded', function(ctx, next) {
    ctx.data.value = ctx.data.id;
    next();
  });

};
