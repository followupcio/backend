'use strict';
let Auth = req('common/repositories/Auth/Auth');
let Fds = req('common/repositories/Auth/Fds');
let Stats = req('common/repositories/Demands/Stats');
let App = req('server/server');

module.exports = function (Employee) {
  /**
   * Catch the loging Attemp and force
   * Custom Auth
   * @param  {[type]} ctx   [description]
   * @param  {[type]} model [description]
   * @param  {[type]} next) {                     let attempt [description]
   * @return {[type]}       [description]
   */
  Employee.beforeRemote('login', function (ctx, model, next) {
    ctx.req.body.ttl = 600; //10 minutes
    ctx.req.body.username = ctx.req.body.username.toUpperCase();
    ctx.req.body.username = ctx.req.body.username.replace(/[|&;$%@"<>()+.:/\,]/g, "");

    let username = ctx.req.body.username;
    const password = ctx.req.body.password;
    let credentials = {username, password};

    Auth.attempt(credentials)
      .then(result => {

        if (result !== true) {
          next(result);
        } else {

          Employee.find({where: {username: username}})
            .then((employees) => {

              if (employees.length === 0) {
                next(new Error(`Username ${username} not found in local database`));
                return
              }

              const authUser = employees[0];

              authUser
                .updateAttributes({
                  password
                })
                .then(() => {
                  next();
                })

            })
        }

        // return 401 if WSO2 login is OK but cannot find the user in the app employee db
      })
      .catch((error) => {
        console.log("catch error")
        next(error)
      })
  });

  /**
   * [description]
   * @param  {[type]} ctx       [description]
   * @param  {[type]} employees [description]
   * @param  {[type]} next)     {                   employees.map((employee) [description]
   * @return {[type]}           [description]
   */
  Employee.afterRemote('*.__get__trustedList', function (ctx, employees, next) {
    const authUserId = ctx.req.accessToken.userId;

    Employee.findById(authUserId).then(employee => {

      const roles = employee.role;

      const isDirector = roles.indexOf("DIRECTOR") > -1;
      //const isDG = role.indexOf("DIRECTOR-GENERAL") > -1;
      //const isFollower = role.indexOf("FOLLOWER") > -1;
      const isStaff = roles.indexOf("STAFF") > -1;

      const toFilter = (isDirector || isStaff );

      // create stats for each user
      employees.map((employee) => {
        employee.stats = Stats.getTimeFrameCount({employee, authUserId, toFilter})
      });

      next();


    })


  });


  /**
   * Stats about employee
   * @param {Function(Error, object)} callback
   */
  Employee.prototype.getStats = function (options, filter, callback) {

    const Demand = App.models.Demand;

    Employee.findById(this.id, function (error, employee) {

      const role = employee && Array.isArray(employee.role) && employee.role[0];

      // if role doesn't exist
      if (!role) {
        callback(null, {
          "submitted": {"key": "SUBMITTED", "count": 0, "percentage": "0"},
          "soonToExpire": {"key": "SOON_TO_EXPIRE", "count": 0, "percentage": "0"},
          "expired": {"key": "EXPIRED", "count": 0, "percentage": "0"},
          "onTime": {"key": "ON_TIME", "count": 0, "percentage": "0"},
          "total": 0
        });

      }

      Demand.find(filter).then(demands => {
        const stats = {};
        const decimals = 1;

        const expiredCount = Stats.countTimeFrame(demands, "EXPIRED");
        const soonToExpireCount = Stats.countTimeFrame(demands, "SOON_TO_EXPIRE");
        const onTimeCount = Stats.countTimeFrame(demands, "ON_TIME");
        const submitted = Stats.countSubmitted(demands);

        stats.total = expiredCount + soonToExpireCount + onTimeCount + submitted;

        stats.expired = {};
        stats.expired.count = expiredCount;
        stats.expired.percentage = parseFloat((stats.expired.count * 100 / stats.total).toFixed(decimals));

        stats.soonToExpire = {};
        stats.soonToExpire.count = soonToExpireCount;
        stats.soonToExpire.percentage = parseFloat((stats.soonToExpire.count * 100 / stats.total).toFixed(decimals));

        stats.onTime = {};
        stats.onTime.count = onTimeCount;
        stats.onTime.percentage = parseFloat((stats.onTime.count * 100 / stats.total).toFixed(decimals));

        stats.submitted = {};
        stats.submitted.count = submitted;
        stats.submitted.percentage = parseFloat((stats.submitted.count * 100 / stats.total).toFixed(decimals));

        callback(null, stats);
      })

    });

  };

  /**
   * [search description]
   * @param  {[type]}   options   [description]
   * @param  {[type]}   email     [description]
   * @param  {[type]}   last_name [description]
   * @param  {Function} callback  [description]
   * @return {[type]}             [description]
   */
  Employee.search = function (options, email, last_name, callback) {

    let params = {};

    if (last_name && last_name !== '') {
      params.lastName = last_name
    }
    if (email && email !== '') {
      params.email = email
    }

    Fds.getUsers(params)
      .then((users) => {
        callback(null, users)
      })
      .catch((error) => {
        callback(null, error)
      })

  };

  /**
   * [add description]
   * @param {[type]}   options  [description]
   * @param {[type]}   email    [description]
   * @param {Function} callback [description]
   */
  Employee.add = function (options, email, callback) {
    let App = req('server/server');
    let Trust = App.models.trust;

    if (!email) {
      callback(null, {error: "Email not valid"});
      return;
    }

    // get user form local db
    Employee.find({where: {email: email}})
      .then((users) => {
        const localUser = Array.isArray(users) && users[0];

        // User already exist in local db
        if (localUser) {

          const model = {
            truster_id: options.accessToken.userId,
            trustee_id: localUser.id,

          };

          //check if trust already exist
          Trust.find({where: model}).then(trusts => {

            const trust = Array.isArray(trusts) && trusts[0]

            // if exist return it
            if (trust && trust.id) {
              callback(null, localUser);
            } else {
              // create trust and return user
              Trust.create(model).then((createdTrust) => {
                if (createdTrust && createdTrust.id) {
                  callback(null, localUser)

                }
              }).catch((error) => {
                callback(null, error)
              })
            }
          })

        }
        else {

          // Fetch user from FSF
          Fds.getUsers({email})
            .then((users) => {

              if (users && users[0] && users[0].email !== '') {

                Employee.create(users[0]).then((createdUser) => {

                  let trust = {
                    truster_id: options.accessToken.userId,
                    trustee_id: createdUser.id,
                  };
                  Trust.create(trust).then((createdTrust) => {

                    if (createdTrust && createdTrust.id) {
                      callback(null, createdUser)
                    }
                  }).catch((error) => {
                    callback(null, error)
                  })

                })
                  .catch((error) => {
                    callback(null, error)
                  })

              } else {
                callback(null, {error: "Impossible to find a user with the given email"});
              }

            })
            .catch((error) => {
              callback(null, error);
            })
        }
      })
      .catch((error) => {
        callback(null, error)
      });


  }

};
