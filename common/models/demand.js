'use strict';
let Timeframe = req('common/repositories/Demands/Timeframe');
let Unread = req('common/repositories/Demands/Unread');
let demandModel = req('common/repositories/Demands/Demand');
let Attachment = req('common/repositories/Demands/Attachment');
const Promise = require("bluebird");
let Excel = req('common/repositories/Demands/Export/Excel/Excel');
let _ = require('lodash');
let moment = require("moment");
let App = req("server/server");

module.exports = function (Demand) {

  //Disable the default create remote method
  Demand.disableRemoteMethodByName("create");

  //Implement a custom create method
  Demand.smartCreate = function (data, options, callback) {

    const authUserId = options.accessToken.userId;

    demandModel.smartCreate(data, authUserId)
      .then(demand => {
        callback(null, demand)
      });

  };

  Demand.prototype.smartUpdate = function (data, options, callback) {

    const authUserId = options.accessToken.userId;
    const id = this.id;
    //ctx.methodString remote method name to hooks

    let demand = new demandModel(id, options.accessToken.userId);

    demand.smartUpdate(data, authUserId)
      .then(demand => {
        callback(null, demand)
      });

  };

  Demand.observe('before delete', (ctx, next) => {
    const Iteration = App.models.iteration;
    const deleteIteration = (demand_id, promises) => {

      Iteration.find({
        where: {
          demand_id: demand_id
        }
      })
        .then(iterations => {

          if (!Array.isArray(iterations)) {
            next();
            return
          }

          iterations.forEach(i => {
            const pro = Iteration.destroyById(i.id);
            promises.push(pro);
          });

        })

    };
    let promises = [];

    deleteIteration(ctx.where.id, promises);

    Demand.find({
      where: {
        parent_id: ctx.where.id
      }
    }).then(demands => {

      demands.forEach(d => {
        promises.push(Demand.destroyById(d.id));
        promises.push(deleteIteration(d.id, promises));
      });

      Promise.all(promises).then(() => {
        next();
      })

    })

  });


  /**
   * Loads the timeframe to the Demand Model
   * When it is retrieve from the DB
   * @param  {Object} ctx     [Query Element]
   * @param  {[type]} next){  ctx.data.timeframe [description]
   * @return {[type]}         [description]
   */
  Demand.observe('loaded', function (ctx, next) {
    let promises = [];

    // Add timeframe
    ctx.data.timeframe = Timeframe.get(ctx.data);

    if (ctx.data.status && ctx.data.status.toUpperCase() === "DRAFT" && Array.isArray(ctx.data.draft_assigned_to) && ctx.data.draft_assigned_to.length === 1) {

      promises.push(App.models.employee.find({
        where: {
          id: ctx.data.draft_assigned_to[0]
        }
      })
        .then((users) => {
          ctx.data.receiver = users[0];
        }));


    }

    /*
    * return the first 10 submitted or return 10 iteration as
    * submitted + ongoing
    * */
    if (Array.isArray(ctx.data.iterations) && typeof ctx.data.iterations.filter === "function") {

      let iterations = ctx.data.iterations;

      let submitted = iterations.filter(i => {
        return i.submitted === true;
      });

      let amountSubmitted = submitted.length;
      let ongoing = [];

      if (amountSubmitted < 10) {
        ongoing = iterations.filter(i => {
          return i.status === 'ONGOING';
        });
      }

      let toReturn = [].concat(submitted, ongoing);
      toReturn = _.uniqBy(toReturn, 'id');

      ctx.data.iterations = toReturn.slice(0, 9);
      if (toReturn.length > 9 && iterations[iterations.length - 1] && iterations[iterations.length - 1].status === "ONGOING") {
        ctx.data.iterations.push(iterations[iterations.length - 1])
      }

    }

    if (Array.isArray(ctx.data.events)) {
      ctx.data.events = ctx.data.events.sort((a, b) => {
        if (a.created_at > b.created_at)
          return -1;
        if (a.created_at < b.created_at)
          return 1;
        return 0;

      });
    }

    Promise.all(promises)
      .then(() => {
        next();
      })

  });

  /**
   * [description]
   * @param  {[type]}   ctx   [description]
   * @param  {[type]}   model [description]
   * @param  {Function} next) {                   let newValues [description]
   * @return {[type]}         [description]
   */
  Demand.afterRemote('find', function (ctx, model, next) {

    if (Array.isArray(ctx.result)) {
      let promises = [];
      const authUserId = ctx.args.options.accessToken.userId;

      ctx.result.forEach(d => {
        //Add unread notification
        //d.unread_notifications = Unread.getGrouped(ctx.result);
        d.unread = Unread.get(d, authUserId);

        if (Array.isArray(d.children)) {
          d.children.forEach((c) => {
            c.unread = Unread.get(c, authUserId);
          })
        }

        let demand = new demandModel(d.id, ctx.req.accessToken.userId);
        promises.push(demand.trackLoad());

      });

      Promise.all(promises).then(() => {
        next();
      })

    } else {
      next();
    }

  });


  Demand.beforeRemote('findById', function (ctx, model, next) {

    let demand = new demandModel(ctx.args.id, ctx.args.options.accessToken.userId);

    demand.read()
      .then(() => {
        next();
      })

  });

  /**
   * Filter
   * @param  {[type]} ctx   [description]
   * @param  {[type]} user  [description]
   * @param  {Object} next) {               if (ctx.args.filter && ctx.args.filter.where) {      ctx.dynamicFilter [description]
   * @return {[type]}       [description]
   */
  Demand.beforeRemote('**', function (ctx, user, next) {

    if (ctx.args.filter && ctx.args.filter.where) {
      ctx.dynamicFilter = {
        timeframe: ctx.args.filter.where.timeframe,
        q: ctx.args.filter.where.q,
      };
      delete ctx.args.filter.where.timeframe;
      delete ctx.args.filter.where.q;
    }

    if (ctx.args.filter && ctx.args.filter.order) {
      ctx.args.filter.order = ctx.args.filter.order.replace(/[|&;$%@"<>()+.:/\,]/g, "");
    }

    next();
  });

  Demand.afterRemote('**', function (ctx, demand, next) {

    ctx.result = demandModel.filterByQAndTimeframe(ctx.result, ctx.dynamicFilter && ctx.dynamicFilter.q, ctx.dynamicFilter && ctx.dynamicFilter.timeframe);

    next();
  });

  // End filtering

  Demand.prototype.activateDraft = function (options, data, callback) {

    const authUserId = options.accessToken.userId;

    let demand = new demandModel(this.id, authUserId);

    demand.activateDraft(data, authUserId)
      .then(result => {
        callback(null, result);
      });

  };

  Demand.prototype.setReopen = function (options, callback) {

    let demand = new demandModel(this.id, options.accessToken.userId);

    demand.reopen().then(() => {

      demand.trackEvent({type: "REOPEN"}).then((updatedModel) => {
        callback(null, updatedModel)
      })

    })

  };

  Demand.prototype.setApprove = function (options, data, callback) {

    let demand = new demandModel(this.id, options.accessToken.userId);
    let event = {type: "APPROVE", description: data.description};

    demand.approve(event)
      .then(mod => {

        if (mod.parent) {
          callback(null, mod)
        } else {
          demand.trackEvent(event)
            .then(updatedModel => {
              callback(null, updatedModel)
            })
        }
      })

  };

  Demand.prototype.setReject = function (options, data, callback) {

    let demand = new demandModel(this.id, options.accessToken.userId);

    demand.reject().then(() => {

      demand.trackEvent({type: "REJECT", description: data.description}).then((updatedModel) => {
        callback(null, updatedModel)
      })


    })

  };

  Demand.prototype.setSubmit = function (options, data, callback) {

    let demand = new demandModel(this.id, options.accessToken.userId);

    demand.submit().then(() => {

      demand.trackEvent({type: "SUBMIT", description: data.description}).then((updatedModel) => {
        callback(null, updatedModel)
      })

    })

  };

  Demand.prototype.setDraftSubmit = function (options, data, callback) {

    let demand = new demandModel(this.id, options.accessToken.userId);

    demand.trackEvent({type: "DRAFT_SUBMIT", description: data.description}, "DRAFT").then((updatedModel) => {
      callback(null, updatedModel)
    })

  };

  Demand.prototype.setDelete = function (options, callback) {

    let demand = new demandModel(this.id, options.accessToken.userId);

    demand.delete().then(() => {
      demand.trackEvent({type: "DELETE"})
        .then((updatedModel) => {
          callback(null, updatedModel)
        })

    })

  };

  Demand.prototype.setUndelete = function (options, callback) {

    let demand = new demandModel(this.id, options.accessToken.userId);

    demand.undelete().then(() => {
      demand.trackEvent({type: "UNDELETE"})
        .then((updatedModel) => {
          callback(null, updatedModel)
        })

    })

  };

  Demand.prototype.setArchive = function (options, callback) {

    let demand = new demandModel(this.id, options.accessToken.userId);

    demand.archive().then(() => {
      demand.trackEvent({type: "ARCHIVE"})
        .then((updatedModel) => {
          callback(null, updatedModel)
        })

    })

  };

  Demand.prototype.setUnarchive = function (options, callback) {

    let demand = new demandModel(this.id, options.accessToken.userId);

    demand.unarchive().then(() => {
      demand.trackEvent({type: "UNARCHIVE"}).then((updatedModel) => {
        callback(null, updatedModel)
      })

    })

  };

  Demand.prototype.setComment = function (options, data, callback) {
    let demand = new demandModel(this.id, options.accessToken.userId);

    demand.trackEvent({
      type: "COMMENT",
      description: data.description,
      attachment_id: data.attachment_id
    }).then((updatedModel) => {
      callback(null, updatedModel)
    })

  };

  Demand.download = function (type, filter, options, callback) {
    //Query the demands theat we want to export
    filter = filter && JSON.parse(filter || {where: {}});
    let q = filter.where.q;
    let timeframe = filter.where.timeframe;

    delete filter.where.q;
    delete filter.where.timeframe;

    Demand.find(filter).then(demands => {

      demands = demandModel.filterByQAndTimeframe(demands, q, timeframe);

      Excel.export(demands)
        .then((url) => {
          callback(null, url);
        })
        .catch((error) => {
          callback(null, error);
        })
    })
  };

  Demand.getStatsByFilter = function (where, userId) {
    let q = where.q;
    let timeframe = where.timeframe;

    delete where.q;
    delete where.timeframe;

    return Demand.find({
      where: where, include: [
        {relation: "loadedBy"},
        {relation: "events", scope: {include: ["author", "attachments"]}},
      ]
    }).then(result => {

      let demands = demandModel.filterByQAndTimeframe(result, q, timeframe);

      demands = demands.filter(d => {

        const approve = demandModel.getMostRecentEvent({
          demand: d,
          types: ["CREATE", "DRAFT", "APPROVE", "ARCHIVE", "UNARCHIVE", "DELETE", "UNDELETE"]
        }).event;
        const loader = _.find(d.loadedBy, {id: userId}) || {};
        const last_load = loader.last_load || 0;

        return moment(last_load).diff(approve.created_at) < 0;

      });

      return {
        new: demands.length,
        amount: result.length
      };
    })

  };

  Demand.stats = function (where, options, callback) {

    where = where && JSON.parse(where || {});
    let promises = [];
    let result = {};
    let statuses = ["draft", "ongoing", "completed", "deleted", "archived"];

    statuses.forEach(s => {
      const filter = Object.assign({status: s.toUpperCase()}, where);

      promises.push(Demand.getStatsByFilter(filter, options.accessToken.userId)
        .then(stats => {
          result[s] = stats;
        }));

    });

    Promise.all(promises)
      .then(() => {
        callback(null, result);
      });
  }

};

