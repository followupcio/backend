const path = require('path');
const _ = require("underscore");

// Load models from json
const DEMANDS = require(path.join(__dirname, "./json/demands.json"));
const EMPLOYEES = require(path.join(__dirname, "./json/employees.json"));
const TRUSTS = require(path.join(__dirname, "./json/trusts.json"));

// Group demand by assigned_by and created_by
const demandsByAssignedBy = _.groupBy(DEMANDS, "assigned_by");
const demandsByCreatedBy = _.groupBy(DEMANDS, "created_by");
const assignedById =  _.uniq(DEMANDS.map( t => t.assigned_by));
const createdById =  _.uniq(DEMANDS.map( t => t.created_by));

// Group trust by truster_id
const trustByEmployee =  _.groupBy(TRUSTS, "truster_id");

const trusters_id = _.uniq(TRUSTS.map( t => t.truster_id));
const trustees_id = _.uniq(TRUSTS.map( t => t.trustee_id));
const employees_id = _.uniq(EMPLOYEES.map( t => t.id));
const demands_id = _.uniq(DEMANDS.map( t => t.id));

// Models are loaded
describe('Models are loaded', () => {
  test('Demands are loaded', () => {
    expect(DEMANDS).toBeDefined();
  });

  test('Employees are loaded', () => {
    expect(EMPLOYEES).toBeDefined();
  });

  test('Trusts are loaded', () => {
    expect(TRUSTS).toBeDefined();
  });
});

// on demands
describe('Valid demands', () => {
  test('Each demand has a valid "created_by"', () => {
    createdById.forEach(c => {
      expect(employees_id).toContain(c);
    })
  });

  test('Each demand has a valid "assigned_by"', () => {
    assignedById.forEach(c => {
      expect(employees_id).toContain(c);
    })
  });

  test('Each non-parent demand has a valid "assigned_to"', () => {
    DEMANDS.forEach(d => {
      if (!d.parent) {
        expect(employees_id).toContain(d.assigned_to);
      }
    })
  });

  test('Each demand has "assigned_by" equal to "created_by"', () => {
    DEMANDS.forEach(d => {
        expect(d.assigned_by).toContain(d.created_by);
    })
  });

  test('Each demand has "created_by" equal to "assigned_by"', () => {
    DEMANDS.forEach(d => {
      expect(d.created_by).toContain(d.assigned_by);
    })
  });

  test('Each child demand has a valid parent ', () => {
    DEMANDS.forEach(d => {
      if (d.parent_id) {
        expect(demands_id).toContain(d.parent_id);
      }
    })
  });
});

// on trust
describe('Valid trusted lists', () => {
  test('Each trusted list does not contains duplicated', () => {
    _.map(trustByEmployee, (list, employees) => {
      expect(list.length).toEqual(_.uniq(list).length);
    })
  });

  test('Each not-parent demand has "assigned_to" as part of the "created_by" trusted list', () => {
    DEMANDS.forEach(d => {
      if (!d.parent) {
        const trusted_list = trustByEmployee[d.created_by] || [];
        const mapped_trusted_list = trusted_list.map( t => t.trustee_id);
        expect(mapped_trusted_list).toContain(d.assigned_to);
      }
    })
  });
});
