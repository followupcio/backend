require('app-module-path').addPath(__dirname);
let restify = require('restify');
let bunyan = require('bunyan');
let helmet = require('helmet');
let https = require('https');
let fs = require("fs");
let jwt = require('jsonwebtoken');
let config = require('./resources/common/config');
let Agenda = require('./lib/Jobs/Agenda');
let Mails = require('lib/Mail/Mailable');
global.dd = require('dump-die');
require('dotenv-safe').load({allowEmptyValues: true});

/**
 * Processes All Jobs defined By the agenda.
 */
Agenda.process();
Mails.process();

var options = {
    name: "Follow-up API",
    log: bunyan.createLogger({
        name: 'generalLogger',
        streams: [{
            type: 'rotating-file',
            path: './logs/generalLogger.log',
            period: '1d',   // daily rotation
            count: 10        // keep 10 back copies
        }]
    }),
//key: fs.readFileSync('/etc/nginx/ssl/nginx.key'), 
//certificate: fs.readFileSync('/etc/nginx/ssl/nginx.crt')
};

var server = restify.createServer(options);

server.use(helmet());

server.use(restify.CORS({ credentials: true }));

//server.use(restify.bodyParser({

// maxBodySize: 10 * 1024,
// mapParams: false,
// multipartFileHandler: function(part) {
//     part.on('data', function(data) {

//     });
// },
//}));

server.use(restify.jsonBodyParser());

server.use(restify.queryParser());

server.use(function authentication(req, res, next) {
    console.log("req.getPath()   :  "+req.getPath());

     console.log(req.getPath().search("/\/api-docs\/?.*/"));

    if (req.getPath() === "/login" || req.getPath().startsWith("/api-docs")) {
        next();
    } else {
        if (req.method === 'OPTIONS') {
            var headers = {};
            // IE8 does not allow domains to be specified, just the *
            // headers["Access-Control-Allow-Origin"] = req.headers.origin;
            headers["Access-Control-Allow-Origin"] = "*";
            headers["Access-Control-Allow-Methods"] = "POST, GET, PUT, DELETE, OPTIONS";
            headers["Access-Control-Allow-Credentials"] = false;
            headers["Access-Control-Max-Age"] = '86400'; // 24 hours
            headers["Access-Control-Allow-Headers"] = "X-Requested-With, X-HTTP-Method-Override, Content-Type, Accept";
            res.writeHead(200, headers);
            res.end();
        } else {
            if (req.headers.id_token) {
                jwt.verify(req.headers.id_token, config.secret, function (err, decoded) {
                    if (err) {
                        res.send(401, err);
                    } else if (decoded.user) {
                        req.authUserFromToken = decoded.user;
                        next();
                    } else {
                        console.log(" First" );
                        res.send(401, "Unauthorizd request");
                    }
                });
            } else {
               //  console.log(" Second" );
               // res.send(401, "Unauthorizd request"); //To remove comment once the cliet is ready to send token
               next();
            }
        }

    }
});

/*Swagger API DOCS*/
server.get(/\/api-docs\/?.*/, restify.serveStatic({
    directory: './public',
    default: 'index.html'
}));

server.use(function (req, res, next) {
    var log = bunyan.createLogger({
        name: 'request',
        streams: [{
            type: 'rotating-file',
            path: './logs/requestLog.log',
            period: '1d',   // daily rotation
            count: 5        // keep 5 back copies
        }]
    });
    log.info({ req_headers: req.headers, req_url: req.url, req_path: req.route }); //log all request

    next();
});

server.on('uncaughtException', function (req, res, route, err) {
    var log = bunyan.createLogger({
        name: 'uncaughtExceptionLogger',
        streams: [{
            type: 'rotating-file',
            path: './logs/uncaughtException.log',
            period: '1d',   // daily rotation
            count: 30        // keep 30 back copies
        }]
    });
    log.error({ err: err });
    // console.error(err);
    try {
        res.send(500, "Unexpected error occured");
    } catch (err1){
         log.error({ err1: err1 });
    }
});

server.on('after', restify.auditLogger({
    log: bunyan.createLogger({
        name: 'auditLogger',
        streams: [{
            type: 'rotating-file',
            path: './logs/auditLogger.log',
            period: '1d',   // daily rotation
            count: 5        // keep 5 back copies
        }]
    })
}));

module.exports = server;

require('./routes');
