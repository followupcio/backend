var ErrorHandler = require('../../common/error');
var Database = require('../../common/database');
var BasicValidation = require('../../common/basicValidation');

module.exports = function deleteAgendaItem(req, res, next) {

  var itemKey = req.params.id;
  var mid = req.params.meetingid;

  var objectID = require('mongodb').ObjectID;
  BasicValidation.userHasPermision().then(
    function (hasPermission) {
      Database().connect()
        .then(delAgenda)
        .then(returnResult)
        .catch(ErrorHandler.handleError(res));
    });

  function delAgenda(db, meeting) {

    return db.collection('meetings').update({ _id: new objectID(mid) },
      { $pull: { "agenda": { key: itemKey } } },
      false,
      true)
      .then(function (result) { db.close(); return result; },
      function (err) { db.close(); ErrorHandler.handleError(err); })
      .catch(ErrorHandler.handleError(res));
  }

  function returnResult(result) {
    res.send({ message: 'Agenda updated' });
    next();
  }
}