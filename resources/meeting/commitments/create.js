var ErrorHandler = require('../../common/error');
var Database = require('../../common/database');
var BasicValidation = require('../../common/basicValidation');
var _ = require('lodash');
var ConstantKeys = require('../../common/constantKeys');

module.exports = function createMeetingCommitment(req, res, next) {
  var cid = req.params.meetingid;
  var objectID = require('mongodb').ObjectID;
  var body = req.body;

  BasicValidation.userHasPermision().then(
    function (hasPermission) {
      BasicValidation.documentExists("meetings", cid).then(
        function (validData) {
          BasicValidation.createCommitmentValidation(body).then(
            function (validData) {
              Database().connect()
                .then(insertCommitment)
                .then(updateMeeting)
                .then(returnResult)
                .catch(ErrorHandler.handleError(res));
            }, ErrorHandler.handleCustomError(res, 412));//basic data validation fail
        }, ErrorHandler.handleCustomError(res, 412));//meeting doesnt exist
    }, ErrorHandler.handleCustomError(res, 401));//user doesnt have permission

  function prepareDataToInsert(db) {

    if (!body.hasOwnProperty('assignedBy')) {
      body.assignedBy = new objectID("58a3085f7d994f79a0cbecb1"); //TODO: get from json token
    } else {
      body.assignedBy = new objectID(body.assignedBy);
    }

    if (_.has(ConstantKeys.demand_source, _.toUpper(body.source))) {
      body.source = _.toUpper(body.source);
    } else {
      body.source = ConstantKeys.demand_source.MEETING;
    }

    body.assignedTo = new objectID(body.assignedTo);
    body.createdBy = new objectID("58a3085f7d994f79a0cbecb1"); //TODO: get from json token
    body.createdDate = new Date();

    return db;
  }

  function insertCommitment(db) {
    return db.collection('commitments').insertOne(body).then(function (insResult) {
      return { db: db, id: insResult.insertedId };
    });

  }

  function updateMeeting(params) {
    return params.db.collection('meetings').updateOne({ _id: new objectID(cid) },
      { $push: { "commitments": new objectID(params.id) } }, { upsert: false })
      .then(function (updateResult) { params.db.close(); return params.id },
      function (err) { params.db.close(); ErrorHandler.handleError(err); })
      .catch(ErrorHandler.handleError(res));
  }

  function returnResult(item) {
    res.send({ message: 'commitment added ' + item });
    next();
  }
};