var _ = require('lodash');
var ErrorHandler = require('../../common/error');
var Database = require('../../common/database');
var BasicValidation = require('../../common/basicValidation');

module.exports = function listCommitments(req, res, next) {

	var meetingid = req.params.meetingid;
	var objectID = require('mongodb').ObjectID;

	BasicValidation.userHasPermision().then(
		function (hasPermission) {
			Database().connect()
				.then(findDocument)
				.then(returnResult)
				.catch(ErrorHandler.handleError(res));
		}, ErrorHandler.handleCustomError(res, 401)
	);

	function findDocument(db) {
		return db.collection('meetings').aggregate([
			{ $match: { _id: new objectID(meetingid) } },
			{ $lookup: { from: "commitments", localField: "commitments", foreignField: "_id", as: "commitments" } },
			{ $unwind: { path: "$commitments", preserveNullAndEmptyArrays: true } },
			{ $lookup: { from: "users", localField: "commitments.assignedTo", foreignField: "_id", as: "commitments.assignedTo" } },
			{ $unwind: { path: "$commitments.assignedTo", preserveNullAndEmptyArrays: true } },
			{ $replaceRoot: { newRoot: "$commitments" } },
			{ $project: { "assignedTo.defaultRoles": 0, "assignedTo.trustedList": 0 } }
		])
			// .skip(parseInt(pageSize, 10) * (parseInt(pageNo, 10) - 1))
			// .limit(parseInt(pageSize, 10))
			.toArray().then(function (result) { db.close(); return result; },
			function (err) { db.close(); ErrorHandler.handleError(err); })
			.catch(ErrorHandler.handleError(res));
	}

	function returnResult(result) {
		res.send(result);
		next();
	}
}