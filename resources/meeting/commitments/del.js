var ErrorHandler = require('../../common/error');
var Database = require('../../common/database');
var BasicValidation = require('../../common/basicValidation');

module.exports = function delCommitment(req, res, next) {
  var meetingid = req.params.meetingid;
  var id = req.params.id;
  var objectID = require('mongodb').ObjectID;
  // console.log("meetingid",meetingid);
  // console.log("id",id);

  BasicValidation.userHasPermision().then(
    function (hasPermission) {
      BasicValidation.documentExists("meetings", meetingid).then(
        function (validData) {
          BasicValidation.documentExists("commitments", id).then(
            function (validData) {
              Database().connect()
                .then(deleteAssociatedComments)
                .then(deleteCommitment)
                .then(updateMeeting)
                .then(returnResult)
                .catch(ErrorHandler.handleError(res));
            }, ErrorHandler.handleCustomError(res, 412));
        }, ErrorHandler.handleCustomError(res, 412));
    }, ErrorHandler.handleCustomError(res, 401));

  function deleteAssociatedComments(db) {
    return db.collection('comments').remove({ "commitmentId": new objectID(id) })
      .then(function (result) { return db; },
      function (err) { db.close(); ErrorHandler.handleError(err); })
      .catch(ErrorHandler.handleError(res));
  }

  function deleteCommitment(db) {
    db.collection('commitments').deleteOne({ _id: new objectID(id) });
    return db;
  }

  function updateMeeting(db) {
    return db.collection('meetings').updateOne({ _id: new objectID(meetingid) },
      { $pull: { "commitments": new objectID(id) } }, { upsert: false })
      .then(function (updateResult) { db.close(); return updateResult },
      function (err) { db.close(); ErrorHandler.handleError(err); })
      .catch(ErrorHandler.handleError(res));
  }

  function returnResult(result) {
    if (result.result.n == 1 && result.result.ok == 1) {
      res.send({ message: 'commitment deleted' });
    } else {
      res.statusCode = 404;
      res.send({ message: 'commitment Not Fount' });
    }
    next();
  }
}