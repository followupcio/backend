var _ = require('lodash');
var ErrorHandler = require('../common/error');
var Database = require('../common/database');
var BasicValidation = require('../common/basicValidation');
var ConstantKeys = require('../common/constantKeys');

module.exports = function listCommitments(req, res, next) {
	var pageNo = req.query.page;
	var pageSize = req.query.per_page;
	var objectID = require('mongodb').ObjectID;

	var queryJson = {};

	if (pageNo == undefined || parseInt(pageNo, 10) <= 0 || isNaN(parseInt(pageNo, 10))) {
		pageNo = 1;
	}
	if (pageSize == undefined || parseInt(pageSize, 10) <= 0 || parseInt(pageSize, 10) > 100 || isNaN(parseInt(pageSize, 10))) {
		pageSize = 10;
	}

	BasicValidation.userHasPermision().then(
		function (hasPermission) {
			Database().connect()
				.then(findDocument)
				.then(returnResult)
				.catch(ErrorHandler.handleError(res));
		}, ErrorHandler.handleCustomError(res, 401)
	);

	function findDocument(db) {
		return db.collection('meetings').find( queryJson ,{"commitments":0})
			.skip(parseInt(pageSize, 10) * (parseInt(pageNo, 10) - 1))
			.limit(parseInt(pageSize, 10))
			.toArray().then(function (result) { db.close(); return result; },
			function (err) { db.close(); ErrorHandler.handleError(err); })
			.catch(ErrorHandler.handleError(res));
	}

	function returnResult(result) {
		res.send(result);
		next();
	}
}