var ErrorHandler = require('../common/error');
var Database = require('../common/database');
var FsdAPI = require('../common/fsd-api');
var BasicValidation = require('../common/basicValidation');
var Promise = require('bluebird');
var uuid = require('mongo-uuid');
var _ = require('lodash');

module.exports = function updateMeeting(req, res, next) {
  var body = req.body;
  var cid = req.params.id;
  var objectID = require('mongodb').ObjectID;

  body.updatedBy = new objectID("58a3085f7d994f79a0cbecb1");//TODO: get from json token  
  body.updatedDate = new Date();

  BasicValidation.userHasPermision()
    .then(function (hasPermission) {
      // BasicValidation.updateCommitmentValidation(body, cid).then(
      //   function (validData) {
      Database().connect()
        .then(updateMeetingAgendaIfExist)
        .then(updateMeeting)
        .then(returnResult)
        .catch(ErrorHandler.handleError(res));
      // }, ErrorHandler.handleCustomError(res, 412));
    }, ErrorHandler.handleCustomError(res, 401));

  function updateMeetingAgendaIfExist(db) {
    if (body.agenda != undefined) {
     
     // Adding an id "key" for any agenda's item
       for (var i = 0; i < body.agenda.length; i++){
         body.agenda[i].key = uuid.stringify(uuid.create());
       }


      return db.collection('meetings').updateOne({ _id: new objectID(cid) },
        { $push: { "agenda": { $each: body.agenda } } }, { upsert: false })
        .then(function (updateResult) { delete body.agenda; return db },
        function (err) { db.close(); ErrorHandler.handleError(err); })
        .catch(ErrorHandler.handleError(res));
    } else {
      return db;
    }

  }

  function updateMeeting(db) {


    return db.collection('meetings').updateOne({ _id: new objectID(cid) }, { $set: body }, { upsert: false })
      .then(function (updateResult) { db.close(); return updateResult },
      function (err) { db.close(); ErrorHandler.handleError(err); })
      .catch(ErrorHandler.handleError(res));
  }

  function returnResult(item) {
    //console.log(item);
    if (item.matchedCount == 1 && item.modifiedCount > 0) {
      res.send({ message: 'Meeting updated' });
    } else {
      res.statusCode = 404;
      res.send({ message: 'Meeting Not Found' });
    }
    next();
  }
}