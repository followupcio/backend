var Promise = require('bluebird');
var _ = require('lodash');
var ErrorHandler = require('../common/error');
var Database = require('../common/database');
var BasicValidation = require('../common/basicValidation');
var ConstantKeys = require('../common/constantKeys');
var uuid = require('mongo-uuid');

module.exports = function createCommitment(req, res, next) {
  var objectID = require('mongodb').ObjectID;
  var body = req.body;

  body.createdBy = new objectID("58a3085f7d994f79a0cbecb1");//TODO: get from json token  
  body.createdDate = new Date();

  BasicValidation.userHasPermision()
    .then(
    function (hasPermission) {
      Database().connect()
        .then(insertCommitments)
        .then(checkAgenda)
        .then(insertMeeting)
        .then(returnResult)
        .catch(ErrorHandler.handleError(res));
    }, ErrorHandler.handleCustomError(res, 401));

  function insertCommitments(db) {
    if (body.commitments != undefined && body.commitments.length > 0) {

      return Promise.reduce(body.commitments, function (total, commitment) {
        return BasicValidation.createCommitmentValidation(commitment)
          .then(function () {
            if (!commitment.hasOwnProperty('assignedBy')) {
              commitment.assignedBy = new objectID("58a3085f7d994f79a0cbecb1"); //TODO: get from json token
            } else {
              commitment.assignedBy = new objectID(body.assignedBy);
            }

            if (_.has(ConstantKeys.demand_source, _.toUpper(commitment.source))) {
              commitment.source = _.toUpper(commitment.source)
            } else {
              commitment.source = ConstantKeys.demand_source.MEETING; 
            }
            commitment.assignedTo = new objectID(commitment.assignedTo);
            commitment.finalApproval = false;
            commitment.createdBy = new objectID("58a3085f7d994f79a0cbecb1"); //TODO: get from json token
            commitment.createdDate = new Date();
            
            return commitment;
          });
      }, 0).then(function (total) {
        return db.collection('commitments').insert(body.commitments).then(function (insResult) {
          var newIds = [];
          for (var id in insResult.insertedIds) {
            newIds.push(new objectID(insResult.insertedIds[id]));

          }
          body.commitments = newIds;
          return db;
        });
      }).catch(function (err) { throw err; });
    } else{
      return db;
    }
  }

  function checkAgenda(db){
     if(body.agenda!=undefined){

     for (var i = 0; i < body.agenda.length; i++){
         body.agenda[i].key = uuid.stringify(uuid.create());
       } 
     }
    return db;
  }

  function insertMeeting(db) {
    return db.collection('meetings').insertOne(body)
      .then(function (insResult) { db.close(); return { meetingId: insResult.insertedId, demandIds: body.commitments }; },
      function (err) { db.close(); ErrorHandler.handleError(err); })
      .catch(ErrorHandler.handleError(res));
  }

  function returnResult(params) {
    res.send({ message: 'meeting added', params });
    next();
  }
}