var ErrorHandler = require('../common/error');
var Database = require('../common/database');
var BasicValidation = require('../common/basicValidation');

module.exports = function listMeeting(req, res, next) {
	var cid = req.params.id;
	var objectID = require('mongodb').ObjectID;

	BasicValidation.userHasPermision()
		.then(function (hasPermission) {
			Database().connect()
				.then(findDocument)
				.then(returnResult)
				.catch(ErrorHandler.handleError(res));
		}, ErrorHandler.handleCustomError(res, 401)
		);

	function findDocument(db) {
		// return db.collection('commitments').findOne({ _id: new objectID(cid) });
		return db.collection('meetings').aggregate([
			{ $match: { _id: new objectID(cid) } },
			{ $lookup: { from: "commitments", localField: "commitments", foreignField: "_id", as: "commitments" } },
			{ $unwind: { path: "$commitments", preserveNullAndEmptyArrays: true } },
			{ $lookup: { from: "users", localField: "commitments.assignedTo", foreignField: "_id", as: "commitments.assignedTo" } },
			{ $unwind: { path: "$commitments.assignedTo" , preserveNullAndEmptyArrays: true } },
			{
				$group: {
					"_id": "$_id",
					"authorityName": { $first: "$authorityName" },
					"authorityRole": { $first: "$authorityRole" },
					"country": { $first: "$country" },
					"deadline": { $first: "$deadline" },
					"shortSummary": { $first: "$shortSummary" },
					"agenda": { $first: "$agenda" },
					"commitments": { "$push": "$commitments" }
				}
			},
			{ $project: { "commitments.assignedTo.defaultRoles": 0, "commitments.assignedTo.trustedList": 0 } }
		]).toArray().then(function (result) { db.close(); return result[0]; },
			function (err) { db.close(); ErrorHandler.handleError(err); })
			.catch(ErrorHandler.handleError(res));
	}

	function returnResult(result) {
		res.send(result);
		next();
	}
};