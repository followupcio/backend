var ErrorHandler = require('../common/error');
var Database = require('../common/database');
var BasicValidation = require('../common/basicValidation');
var ConstantKeys = require('../common/constantKeys');

module.exports = function demandStatByDate(req, res, next) {

	var assignedBy = req.query.assignedBy;
	var assignedTo = req.query.assignedTo;

	var objectID = require('mongodb').ObjectID;

	var queryJson = {"status" : ConstantKeys.demand_status.ONGOING};
	var queryUserJson = {};

	BasicValidation.userHasPermision().then(
		function (hasPermission) {

			if(!(assignedBy==undefined && assignedTo==undefined) ){
				Database().connect()
					.then(prepareQuery)
					.then(findDocument)
					.then(returnResult)
					.catch(ErrorHandler.handleError(res));
			} else {
				res.send({"total":0})
			}
		}, ErrorHandler.handleCustomError(res, 401)
	);

	function prepareQuery(db) {
		queryUserJson['user2'] = "$assignedBy"; //default
		if (assignedBy != null && assignedBy != "" && assignedBy != undefined) {
			queryJson['assignedBy'] = { "$in": [new objectID(assignedBy)] };
		}
		if (assignedTo != null && assignedTo != "" && assignedTo != undefined) {
			queryJson['assignedTo'] = { "$in": [new objectID(assignedTo)] };
			queryUserJson['user2'] = "$assignedTo";
		}


		return db;
	}

	function findDocument(db) {


			return db.collection('commitments').aggregate([
				{ $match: queryJson },
				{
					$group: {
						_id: queryUserJson,

						// completedCommitments: {
						// 	$sum: { $cond: { if: { $eq: ["$status", ConstantKeys.demand_status.COMPLETE] }, then: 1, else: 0 } }
						// },
						// archivedCommitments: {
						// 	$sum: { $cond: { if: { $eq: ["$status", ConstantKeys.demand_status.ARCHIVE] }, then: 1, else: 0 } }
						// },
						ontimeOngoingCommitments: {
							$sum: {
								$cond: {
									if: {
										$and: [
                                            { $eq: ["$parent", false] },
											{ $eq: ["$status", ConstantKeys.demand_status.ONGOING] },
											{ $gte: ["$deadline", new Date(new Date().getTime() + 1000 * 3600 * 24 * 3)] }
										]
									}, then: 1, else: 0
								}
							}
						},//ONTIME: { $gte: new Date(new Date().getTime() + 1000 * 3600 * 24 * 15) },
						nearDeadlineCommitments: {
							$sum: {
								$cond: {
									if: {
										$and: [
                                            { $eq: ["$parent", false] },
											{ $eq: ["$status", ConstantKeys.demand_status.ONGOING] },
											{ $lt: ["$deadline", new Date(new Date().getTime() + 1000 * 3600 * 24 * 3)] },
											{ $gte: ["$deadline", new Date()] }
										]
									}, then: 1, else: 0
								}
							}
						},//EXPIRING: { $lt: new Date(new Date().getTime() + 1000 * 3600 * 24 * 15), $gte: new Date() },
						expiredOngoingCommitments: {
							$sum: {
								$cond: {
									if: {
										$and: [
                                            { $eq: ["$parent", false] },
											{ $eq: ["$status", ConstantKeys.demand_status.ONGOING] },
											{ $lt: ["$deadline", new Date()] }
										]
									}, then: 1, else: 0
								}
							}
						}//	EXPIRED: { $lt: new Date() }
					}
				},
				{ $lookup: { from: "users", localField: "_id.user2", foreignField: "_id", as: "user4" } },
				{ $unwind: { path: "$user4", preserveNullAndEmptyArrays: true } },
				{ $sort: { ongoingCommitments: 1 } },
				{
					$project: {
						_id: 0, 
						// user: {"_id":"$user4._id", "firstName":"$user4.firstName", 
						// 	"lastName":"$user4.lastName","title":"$user4.title",
						// 	"fullName":"$user4.fullName","email":"$user4.email",
						// 	"phone":"$user4.phone","division":"$user4.division",
						// 	"imageUrl":"$user4.imageUrl","indexNo":"$user4.indexNo"
						// }, 
						// completed: "$completedCommitments", 
						// archived: "$archivedCommitments",
						soonToExpire: {"key": "EXPIRING", "count":"$nearDeadlineCommitments",
												"percentage" : { "$substr" : [ {"$multiply" : [{"$divide" : [100,{$add:["$nearDeadlineCommitments","$expiredOngoingCommitments","$ontimeOngoingCommitments" ]}]},"$nearDeadlineCommitments"]}, 0,5 ] }	
												},
						expired: {"key": "EXPIRED", "count":"$expiredOngoingCommitments",
												"percentage" : { "$substr" : [ {"$multiply" : [{"$divide" : [100,{$add:["$nearDeadlineCommitments","$expiredOngoingCommitments","$ontimeOngoingCommitments" ]}]},"$expiredOngoingCommitments"]}, 0,5 ] }
												},
						ontime: {"key": "ONTIME", "count":"$ontimeOngoingCommitments",
												"percentage" : { "$substr" : [ {"$multiply" : [{"$divide" : [100,{$add:["$nearDeadlineCommitments","$expiredOngoingCommitments","$ontimeOngoingCommitments" ]}]},"$ontimeOngoingCommitments"]}, 0,5 ] }	
												},
                        total: {$add:["$nearDeadlineCommitments","$expiredOngoingCommitments","$ontimeOngoingCommitments" ]}
					}
				}
			])
			.toArray().then(function (result) {db.close();

                  return result

			}, function(err){db.close(); ErrorHandler.handleError(err);})
			.catch(ErrorHandler.handleError(res));
	}



	function returnResult(result) {
		if(result!=undefined && result[0]!=null){
			res.send(result[0]);
		} else {
			res.send({"total":0})
		}

		next();
	}
};