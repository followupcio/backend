var ErrorHandler = require('../common/error');
var Database = require('../common/database');
var BasicValidation = require('../common/basicValidation');
var _ = require('lodash');

module.exports = function getBasicStat(req, res, next) {

	var status = req.query.status;
	var assignedBy = req.query.assignedBy;
	var assignedTo = req.query.assignedTo;

	var objectID = require('mongodb').ObjectID;

	var queryJson = {};

	if (status != null && status != "" && status != undefined) {
		queryJson['status'] = { "$in": [_.toUpper(status)] };
	}
	if (assignedBy != null && assignedBy != "" && assignedBy != undefined) {
		queryJson['assignedBy'] = { "$in": [new objectID(assignedBy)] };
	}
	if (assignedTo != null && assignedTo != "" && assignedTo != undefined) {
		queryJson['assignedTo'] = { "$in": [new objectID(assignedTo)] };
	}

	console.log(queryJson);

	BasicValidation.userHasPermision()
		.then(function (hasPermission) {
			Database().connect()
				.then(findDocument)
				.then(returnResult)
				.catch(ErrorHandler.handleError(res));
		}, ErrorHandler.handleCustomError(res, 401));

	function findDocument(db) {
			return db.collection('commitments').aggregate(
				[{
					$facet: {
						"tagCount": [
							{ $match: queryJson },
							{ $unwind:  { path: "$tag", preserveNullAndEmptyArrays: true } },
							{
								$group: {
									_id: '$tag',
									count: { $sum: 1 }
								}
							},
							{ $sort: { count: -1, _id: -1 } }
						],
						"categoryCount": [
							{ $match: queryJson },
							{
								$group: {
									_id: '$category',
									count: { $sum: 1 }
								}
							},
							{ $sort: { count: -1, _id: -1 } }
						]

					}
				}
				]
			).toArray().then(function (result) {db.close(); return result; }, function (err) {db.close();ErrorHandler.handleError(err); })
			.catch(ErrorHandler.handleError(res));


			// jira issue created: https://jira.mongodb.org/browse/NODE-948
			// return db.collection('commitments').aggregate({
			// 	"$facet": {
			// 		"categorizedByTags": [
			// 			{ $match: queryJson },
			// 			{ $unwind: "$tag" },
			// 			{ $sortByCount: "$tag" }
			// 		],
			// 		"categorizedByCategory": [
			// 			{ $match: queryJson },
			// 			{ $sortByCount: "$category" }
			// 		]
			// 	}
			// }).toArray().then(function (result) { return result; }, function (err) { console.log("err",err);ErrorHandler.handleError(err); });

	}

	function returnResult(result) {
		res.send(result);
		next();
	}

};