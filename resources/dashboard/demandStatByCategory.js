var ErrorHandler = require('../common/error');
var Database = require('../common/database');
var BasicValidation = require('../common/basicValidation');
var _ = require('lodash');
var ConstantKeys = require('../common/constantKeys');

module.exports = function demandStatByCategory(req, res, next) {

	var category = req.query.category;
	var assignedBy = req.query.assignedBy;
	var assignedTo = req.query.assignedTo;

	var objectID = require('mongodb').ObjectID;

	var queryJson = {"status" : ConstantKeys.demand_status.ONGOING};

	if (category != null && category != "" && category != undefined) {
		queryJson['category'] = { "$in": [_.toUpper(category)] };
	}
	
	if (assignedBy != null && assignedBy != "" && assignedBy != undefined) {
		queryJson['assignedBy'] = { "$in": [new objectID(assignedBy)] };
	} else if (assignedTo != null && assignedTo != "" && assignedTo != undefined) {
		queryJson['assignedTo'] = { "$in": [new objectID(assignedTo)] };
	}

	//console.log("demandStatByCategory: ",queryJson);

	BasicValidation.userHasPermision()
		.then(function (hasPermission) {
			Database().connect()
				.then(findDocument)
				.then(returnResult)
				.catch(ErrorHandler.handleError(res));
		}, ErrorHandler.handleCustomError(res, 401));

	function findDocument(db) {
			return db.collection('commitments').aggregate(
				[{ $facet : {
					"categories" : [
						{ $match: queryJson },
						{ $group : {
							_id : '$category',
							count : { $sum: 1 }
							}
						},
						{ $group : { 
							_id : null, 
							category : { $push : { key : "$_id", count : "$count" } },
							total : { $sum : "$count" }
							}
						},
						{ $unwind : { path : "$category", preserveNullAndEmptyArrays : true } },
						{ $lookup : { from : "resources", localField : "category.key", foreignField : "key", as : "categoryL" } },
						{ $unwind : { path : "$categoryL", preserveNullAndEmptyArrays : true } },
						{ $project : { 
							"_id" : 0,
							"category" : { "key" : "$categoryL.key", "label" : "$categoryL.label", "count" : "$category.count",
									"percentage" : {"$concat" : [ { "$substr" : [ {"$multiply" : [{"$divide" : [100,"$total"]},"$category.count"]}, 0,5 ] }, "", "%" ]}
								}
							}
						}
					],
					"subCategories": [
						{ $match: queryJson },
						{
							$group: {
								_id: '$subcategory',
								count: { $sum: 1 }
							}
						},
						{
							$group: {
								_id: null,
								subcategory: { $push: { key: "$_id", count: "$count" } },
								total: { $sum: "$count" }
							}
						},
						{ $unwind: { path: "$subcategory", preserveNullAndEmptyArrays: true } },
						{ $lookup: { from: "resources", localField: "subcategory.key", foreignField: "key", as: "subcategoryL" } },
						{ $unwind: { path: "$subcategoryL", preserveNullAndEmptyArrays: true } },
						{
							$project: {
								"_id": 0,
								"subcategory": {
									"key": "$subcategoryL.key", "label": "$subcategoryL.label", "count": "$subcategory.count",
									"percentage": { "$concat": [{ "$substr": [{ "$multiply": [{ "$divide": [100, "$total"] }, "$subcategory.count"] }, 0, 5] }, "", "%"] }
								}
							}
						}
					]
				} 
			  }]
			).toArray().then(function (result) {db.close(); return result; }, function (err) {db.close();ErrorHandler.handleError(err); })
			.catch(ErrorHandler.handleError(res));

	}

	function returnResult(result) {
		res.send(result);
		next();
	}
};