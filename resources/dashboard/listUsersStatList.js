var ErrorHandler = require('../common/error');
var Database = require('../common/database');
var BasicValidation = require('../common/basicValidation');
var ConstantKeys = require('../common/constantKeys');

module.exports = function listCommitmentUsersByUser(req, res, next) {

	var pageNo = req.query.page;
	var pageSize = req.query.per_page;
	var assignedBy = req.query.assignedBy;
	var assignedTo = req.query.assignedTo;

	var objectID = require('mongodb').ObjectID;

	var queryJson = {};

	BasicValidation.userHasPermision().then(
		function (hasPermission) {
			Database().connect()
				.then(prepareQuery)
				.then(findDocument)
				.then(returnResult)
				.catch(ErrorHandler.handleError(res));
		}, ErrorHandler.handleCustomError(res, 401)
	);

	function prepareQuery(db) {
		if (pageNo == undefined || parseInt(pageNo, 10) <= 0 || isNaN(parseInt(pageNo, 10))) {
			pageNo = 1;
		}
		if (pageSize == undefined || parseInt(pageSize, 10) <= 0 || parseInt(pageSize, 10) > 100 || isNaN(parseInt(pageSize, 10))) {
			pageSize = 10;
		}
		if (assignedBy != null && assignedBy != "" && assignedBy != undefined) {
			queryJson['assignedBy'] = { "$in": [new objectID(assignedBy)] };
		}
		if (assignedTo != null && assignedTo != "" && assignedTo != undefined) {
			queryJson['assignedTo'] = { "$in": [new objectID(assignedTo)] };
		}

		return db;
	}

	function findDocument(db) {
			return db.collection('commitments').aggregate([
				{ $match: queryJson },
				{
					$group: {
						_id: { user1: "$assignedTo", user2: "$assignedBy" },

						// completedCommitments: {
						// 	$sum: { $cond: { if: { $eq: ["$status", ConstantKeys.demand_status.COMPLETE] }, then: 1, else: 0 } }
						// },
						// archivedCommitments: {
						// 	$sum: { $cond: { if: { $eq: ["$status", ConstantKeys.demand_status.ARCHIVE] }, then: 1, else: 0 } }
						// },
						ontimeOngoingCommitments: {
							$sum: {
								$cond: {
									if: {
										$and: [
											{ $eq: ["$status", ConstantKeys.demand_status.ONGOING] },
											{ $gte: ["$deadline", new Date(new Date().getTime() + 1000 * 3600 * 24 * 3)] }
										]
									}, then: 1, else: 0
								}
							}
						},//ONTIME: { $gte: new Date(new Date().getTime() + 1000 * 3600 * 24 * 15) },
						nearDeadlineCommitments: {
							$sum: {
								$cond: {
									if: {
										$and: [
											{ $eq: ["$status", ConstantKeys.demand_status.ONGOING] },
											{ $lt: ["$deadline", new Date(new Date().getTime() + 1000 * 3600 * 24 * 3)] },
											{ $gte: ["$deadline", new Date()] }
										]
									}, then: 1, else: 0
								}
							}
						},//EXPIRING: { $lt: new Date(new Date().getTime() + 1000 * 3600 * 24 * 15), $gte: new Date() },
						expiredOngoingCommitments: {
							$sum: {
								$cond: {
									if: {
										$and: [
											{ $eq: ["$status", ConstantKeys.demand_status.ONGOING] },
											{ $lt: ["$deadline", new Date()] }
										]
									}, then: 1, else: 0
								}
							}
						}//	EXPIRED: { $lt: new Date() }
					}
				},
				{ $lookup: { from: "users", localField: "_id.user1", foreignField: "_id", as: "user3" } },
				// { $lookup: { from: "users", localField: "_id.user2", foreignField: "_id", as: "user4" } },
				{ $unwind: { path: "$user3", preserveNullAndEmptyArrays: true } },
				// { $unwind: { path: "$user4", preserveNullAndEmptyArrays: true } },
				{ $sort: { ongoingCommitments: 1 } },
				{
					$project: {
						_id: 0, 
						// assignedBy: {"_id":"$user4._id", "firstName":"$user4.firstName", 
						// 	"lastName":"$user4.lastName","title":"$user4.title",
						// 	"fullName":"$user4.fullName","email":"$user4.email",
						// 	"phone":"$user4.phone","division":"$user4.division",
						// 	"imageUrl":"$user4.imageUrl","indexNo":"$user4.indexNo"
						// }, 
						assignedBy: "$_id.user2",
						assignedTo: {"_id":"$user3._id", "firstName":"$user3.firstName", 
							"lastName":"$user3.lastName","title":"$user3.title",
							"fullName":"$user3.fullName","email":"$user3.email",
							"phone":"$user3.phone","division":"$user3.division",
							"imageUrl":"$user3.imageUrl","indexNo":"$user3.indexNo"
						}, 
						// completed: "$completedCommitments", 
						// archived: "$archivedCommitments", 
						soonToExpire: {"key": "EXPIRING", "count":"$nearDeadlineCommitments",
												"percentage" : {"$concat" : [ { "$substr" : [ {"$multiply" : [{"$divide" : [100,{$add:["$nearDeadlineCommitments","$expiredOngoingCommitments","$ontimeOngoingCommitments" ]}]},"$nearDeadlineCommitments"]}, 0,5 ] }, "", "%" ]}	
												},
						expired: {"key": "EXPIRED", "count":"$expiredOngoingCommitments",
												"percentage" : {"$concat" : [ { "$substr" : [ {"$multiply" : [{"$divide" : [100,{$add:["$nearDeadlineCommitments","$expiredOngoingCommitments","$ontimeOngoingCommitments" ]}]},"$expiredOngoingCommitments"]}, 0,5 ] }, "", "%" ]}	
												},
						ontime: {"key": "ONTIME", "count":"$ontimeOngoingCommitments",
												"percentage" : {"$concat" : [ { "$substr" : [ {"$multiply" : [{"$divide" : [100,{$add:["$nearDeadlineCommitments","$expiredOngoingCommitments","$ontimeOngoingCommitments" ]}]},"$ontimeOngoingCommitments"]}, 0,5 ] }, "", "%" ]}	
												}
						// soonToExpire: "$nearDeadlineCommitments",
						// expired: "$expiredOngoingCommitments",
						// ontime: "$ontimeOngoingCommitments"
					}
				}
			])
			.skip(parseInt(pageSize, 10) * (parseInt(pageNo, 10) - 1))
			.limit(parseInt(pageSize, 10))
			.toArray().then(function (result) {db.close(); return result;}, function(err){db.close(); ErrorHandler.handleError(err);})
			.catch(ErrorHandler.handleError(res));
	}

	function returnResult(result) {
		res.send(result);
		next();
	}
};