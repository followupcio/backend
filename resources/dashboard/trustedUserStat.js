var ErrorHandler = require('../common/error');
var Database = require('../common/database');
var BasicValidation = require('../common/basicValidation');
var ConstantKeys = require('../common/constantKeys');
var moment = require('moment');

module.exports = function listtrustedUserStat(req, res, next) {

	var ownerid = req.params.ownerid;
	var objectID = require('mongodb').ObjectID;

	BasicValidation.userHasPermision().then(
		function (hasPermission) {
			Database().connect()
				.then(findDocument)
				.then(returnResult)
				.catch(ErrorHandler.handleError(res));
		}, ErrorHandler.handleCustomError(res, 401)
	);

	function findDocument(db) {


		return db.collection('users').aggregate([
			{ $match: { "_id": new objectID(ownerid) } },
			{ $lookup: { from: "users", localField: "trustedList", foreignField: "_id", as: "trustedList" } },
			{ $unwind: { path: "$trustedList", preserveNullAndEmptyArrays: true } },
			{ $replaceRoot: { newRoot: "$trustedList" } },
			{ $lookup: { from: "commitments", localField: "_id", foreignField: "assignedTo", as: "commitments" } },
			{ $unwind: { path: "$commitments", preserveNullAndEmptyArrays: true } },
			{
				$group: {
					_id: "$_id",
					"firstName": { $first: "$firstName" },
					"lastName": { $first: "$lastName" },
					"title": { $first: "$title" },
					"displayName": { $first: "$displayName" },
					"email": { $first: "$email" },
					"phone": { $first: "$phone" },
					"division": { $first: "$division" },
					"imageUrl": { $first: "$imageUrl" },
					"indexNo": { $first: "$indexNo" },
					completedCommitments: {
						$sum: {
							$cond: {
								if: {
									//	$and: [{ $eq: ["$commitments.status", ConstantKeys.demand_status.COMPLETED] },
									//{ $eq: ["$commitments.assignedBy", new objectID(ownerid)] }]
									$eq: ["$commitments.status", ConstantKeys.demand_status.COMPLETED]
								},
								then: 1, else: 0
							}
						}
					},
					// archivedCommitments: {
					// 	$sum: {
					// 		$cond: {
					// 			if: {
					// 				$and: [{ $eq: ["$commitments.status", ConstantKeys.demand_status.ARCHIEVED] },
					// 				{ $eq: ["$commitments.assignedBy", new objectID(ownerid)] }]
					// 			},
					// 			then: 1, else: 0
					// 		}
					// 	}
					// },
					ontimeOngoingCommitments: {
						$sum: {
							$cond: {
								if: {
									$and: [{ $eq: ["$commitments.status", ConstantKeys.demand_status.ONGOING] },

									//{ $eq: ["$commitments.assignedBy", new objectID(ownerid)] },
									{ $gte: ["$commitments.deadline", new Date()] }
										// { $gte: ["$commitments.deadline", new Date(new Date().getTime() + 1000 * 3600 * 24 * 15)] }
									]

								},
								then: 1, else: 0
							}
						}
					},
					expiredOngoingCommitments: {
						$sum: {
							$cond: {
								if: {
									$and: [{ $eq: ["$commitments.status", ConstantKeys.demand_status.ONGOING] },
									//{ $eq: ["$commitments.assignedBy", new objectID(ownerid)] },
									//{ $lt: ["$commitments.deadline", new Date(new Date().getTime() + 1000 * 3600 * 24 * 1)] }
									{ $lt: ["$commitments.deadline", new Date()] }

									]

								},
								then: 1, else: 0

							}
						}
					}
				}
			}
		]).toArray()
			.then(function (result) { db.close(); return result; },
			function (err) { db.close(); ErrorHandler.handleError(err); })
			.catch(ErrorHandler.handleError(res));
	}

	function returnResult(result) {
		console.log(result);
		res.send(result || []);
		next();
	}

};