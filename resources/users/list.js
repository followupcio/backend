var ErrorHandler = require('../common/error');
var Database = require('../common/database');
var ConstantKeys = require('../common/constantKeys');

module.exports = function listCommitmentUsersByUser(req, res, next) {

    var objectID = require('mongodb').ObjectID;

    var queryJson = {};


    console.log(queryJson);

    Database().connect()
        .then(validateUserPermissions)
        .then(findDocument)
        .then(returnResult)
        .catch(ErrorHandler.handleError(res));

    function validateUserPermissions(db) {
        //TODO the user permissions 
        var userHasPermission = true;
        if (userHasPermission) {
            return db;
        } else {
            throw { "message": "User does not have permission" };
        }
    }

    function findDocument(db) {
        try {
            return db.collection('users').aggregate([
                { $match: queryJson },
                { $lookup: { from: "commitments", localField: "_id", foreignField: "assignedTo", as: "commitments" } },
                { $unwind: { path: "$commitments", preserveNullAndEmptyArrays: true } },

                {
                    $group: {
                        _id: "$_id",
                        "firstName": { $first: "$firstName" },
                        "lastName": { $first: "$lastName" },
                        "title": { $first: "$title" },
                        "displayName": { $first: "$displayName" },
                        "email": { $first: "$email" },
                        "phone": { $first: "$phone" },
                        "division": { $first: "$division" },
                        "imageUrl": { $first: "$imageUrl" },
                        "indexNo": { $first: "$indexNo" },
                        "role": { $first: "$defaultRoles" },
                        //"commitments": { $first: '$commitments' },
                        "ontimeOngoingCommitments": {
							$sum: {
								$cond: {
									if: {
										$and: [
											{ $eq: ["$commitments.status", ConstantKeys.demand_status.ONGOING] },
											{ $gte: ["$commitments.deadline", new Date(new Date().getTime() + 1000 * 3600 * 24 * 15)] }
										]
									}, then: 1, else: 0
								}
							}
						},
                        "nearDeadlineOngoingCommitments": {
							$sum: {
								$cond: {
									if: {
										$and: [
											{ $eq: ["$commitments.status", ConstantKeys.demand_status.ONGOING] },
											{ $lt: ["$commitments.deadline", new Date(new Date().getTime() + 1000 * 3600 * 24 * 15)] },
											{ $gte: ["$commitments.deadline", new Date()] }
										]
									}, then: 1, else: 0
								}
							}
						},

                        "expiredOngoingCommitments": {
                            $sum: {
                                $cond: {
                                    if: {
                                        $and: [
                                            { $eq: ["$commitments.status", ConstantKeys.demand_status.ONGOING] },                                           
											{ $lt: ["$commitments.deadline", new Date()] }
                                        ]
                                    },
                                    then: 1, else: 0
                                }
                            }
                        }
                    }
                }


            ]).toArray().then(function (result) { return result; }, function (err) { ErrorHandler.handleError(err); });
        } catch (error) {
            ErrorHandler.handleError(error);
        } finally {
            db.close();
        }
    }

    function returnResult(result) {
        res.send(result);
        next();
    }




};