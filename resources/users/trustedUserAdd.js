var ErrorHandler = require('../common/error');
var Database = require('../common/database');
var FsdAPI = require('../common/fsd-api');
var BasicValidation = require('../common/basicValidation');

module.exports = function addUserList(req, res, next) {
  var body = req.body;
  var cid = req.params.ownerId;
  var mongodb = require('mongodb');

  BasicValidation.userHasPermision()
    .then(function (hasPermission) {
      BasicValidation.userExists(cid).then(
        function (userExist) {
          Database().connect()
            .then(getUserByEmail)
            .then(insertUserIfNeeded)
            .then(updateTrustedUserList)
            .then(returnResult)
            .catch(ErrorHandler.handleError(res));
        }, ErrorHandler.handleCustomError(res, 412));
    }, ErrorHandler.handleCustomError(res, 401));

  function getUserByEmail(db) {
    var query = { "email": { "$regex": body.trustedUser.email, "$options": "i" } };
    return db.collection('users').findOne(query)
      .then(
      function (user) {
        if (user != null && user["_id"] != null) { //existing user in document
          delete body.trustedUser;
          body.trustedUser = user["_id"]; //update trustedUser by ID
          return { db: db, existingUser: user };
        } else {//retrieve from FsdAPI
          return FsdAPI.fsdUser({ "email": body.trustedUser.email })
            .then(
            function (user) {
              if(user){
                return { db: db, user: user };
              } else {
                 throw new Error("User does not exist");
              }
              
            });
        }
      });
  }

  function insertUserIfNeeded(params) {
    if (params.user != null && params.user != undefined) {//insert user
      params.user[0].defaultRoles=["Manager"];
      return params.db.collection('users').insertOne(params.user[0]).then(function (insResult) {
        delete body.trustedUser;
        body.trustedUser = insResult.insertedId; //update trustedUser by ID
        params.user._id = insResult.insertedId
        return { db: params.db, existingUser: params.user };
      });
    }
    else {
      return params;
    }
  }

  function updateTrustedUserList(params) {

    return params.db.collection('users').updateOne({ _id: new mongodb.ObjectID(cid) },
      { $push: { "trustedList": new mongodb.ObjectID(body.trustedUser) } },
      { upsert: false })
      .then(function (updateResult) { params.db.close(); return { item: updateResult, existingUser: params.existingUser }; },
      function (err) { params.db.close(); ErrorHandler.handleError(err); })
      .catch(ErrorHandler.handleError(res));

  }

  function returnResult(params) {
    // console.log(params);
    if (params.item.matchedCount == 1 && params.item.modifiedCount > 0) {
      res.send({ message: 'UserList updated', user: params.existingUser });
    } else {
      res.statusCode = 404;
      res.send({ message: 'User Not Fount' });
    }
    next();
  }

}