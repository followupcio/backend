var ErrorHandler = require('../common/error');
var Database = require('../common/database');
var FsdAPI = require('../common/fsd-api');
var BasicValidation = require('../common/basicValidation');

module.exports = function updateUserList(req, res, next) {
  var body = req.body;
  var cid = req.params.ownerId;
  var mongodb = require('mongodb');

  BasicValidation.userHasPermision()
    .then(function (hasPermission) {
      BasicValidation.userExists(cid).then(
        function (userExist) {
          Database().connect()
            .then(updateTrustedUserList)
            .then(returnResult)
            .catch(ErrorHandler.handleError(res));
        }, ErrorHandler.handleCustomError(res, 412));
    }, ErrorHandler.handleCustomError(res, 401));

  function updateTrustedUserList(db) {

    return db.collection('users').updateOne({ _id: new mongodb.ObjectID(cid) },
      { $pull: { "trustedList": new mongodb.ObjectID(body.trustedUser.key) } },
      { upsert: false })
      .then(function (updateResult) { db.close(); return { item: updateResult }; },
            function (err) { db.close(); ErrorHandler.handleError(err); })
      .catch(ErrorHandler.handleError(res));;

  }

  function returnResult(params) {
    // console.log(params);
    if (params.item.matchedCount == 1 && params.item.modifiedCount > 0) {
      res.send({ message: 'User Removed from UserList' });
    } else {
      res.statusCode = 404;
      res.send({ message: 'User does not exist in UserList' });
    }
    next();
  }

}