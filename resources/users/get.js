var ErrorHandler = require('../common/error');
var Database = require('../common/database');
var BasicValidation = require('../common/basicValidation');

module.exports = function listCommitment(req, res, next) {
	var userId = req.params.id;
	var objectID = require('mongodb').ObjectID;

	BasicValidation.userHasPermision().then(
    	function (hasPermission) {
			Database().connect()
				.then(findDocument)
				.then(returnResult)
				.catch(ErrorHandler.handleError(res));
    }, ErrorHandler.handleCustomError(res, 401));

	function findDocument(db) {
		return db.collection('users').findOne({ _id: new objectID(userId) }).then(function (result) {
			db.close();
			return result;
		}, function (err) { db.close(); ErrorHandler.handleError(err); }).catch(ErrorHandler.handleError(res));
	}

	function returnResult(result) {
		res.send(result);
		next();
	}
};