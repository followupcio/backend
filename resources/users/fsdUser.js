var FsdAPI = require('../common/fsd-api');
var ErrorHandler = require('../common/error');

module.exports = function fsdUser(req, res, next) {
	var userId = req.params.id;

	if (userId.length > 2) {
		FsdAPI.fsdUser({"lastName":userId}).then(function(body){res.send(body);}, function(err){ErrorHandler.handleError(err);});		
	} else {
		var jsonData = { "message": "string too small" };
		// res.statusCode = 203;
		res.send(jsonData);
		next();
	}
};