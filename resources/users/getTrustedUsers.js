var ErrorHandler = require('../common/error');
var Database = require('../common/database');
var BasicValidation = require('../common/basicValidation');

module.exports = function listCommitment(req, res, next) {
	var userId = req.params.ownerId;//get from token
	var objectID = require('mongodb').ObjectID;

	BasicValidation.userHasPermision().then(
    	function (hasPermission) {
			Database().connect()
				.then(findDocument)
				.then(returnResult)
				.catch(ErrorHandler.handleError(res));
    }, ErrorHandler.handleCustomError(res, 401));



	function findDocument(db) {
		return db.collection('users')
			.aggregate([
				{ $match: { "_id": new objectID(userId) } },
				{ $lookup: { from: "users", localField: "trustedList", foreignField: "_id", as: "trustedList" } },
				{ $unwind: "$trustedList" },
				{ $project: { "_id": "$trustedList._id", "firstName": "$trustedList.firstName",
							 "lastName": "$trustedList.lastName", "title": "$trustedList.title",
							 "fullName": "$trustedList.fullName", "email": "$trustedList.email",
							 "phone": "$trustedList.phone", "division": "$trustedList.division",
							 "imageUrl": "$trustedList.imageUrl", "indexNo": "$trustedList.indexNo",
							}
				}]).toArray().then(function (result) {
				db.close();
				return result;
			}, function (err) { db.close(); ErrorHandler.handleError(err); }).catch(ErrorHandler.handleError(res));
	}

	function returnResult(result) {
		res.send(result);
		next();
	}
};