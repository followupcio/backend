var _ = require('lodash');
var ErrorHandler = require('../common/error');
var Database = require('../common/database');
var Wso2Auth = require('../common/wso2AuthRequest');
var jwt = require('jsonwebtoken');
var config = require('../common/config');

module.exports = function checkAuthentication(req, res, next) {
    var body = req.body;
    var usrName = body.username;
    var objectID = require('mongodb').ObjectID;

    Database().connect().then(findDocument).catch().then(returnResult).catch(ErrorHandler.handleError(res));

    function findDocument(db) {
        if (process.env.APP_ENV != 'production') {
            return localLogin(db);
        }
        return Wso2Auth.authUser(req.body.username, req.body.password).then(function success(ret) {
            if (ret == "SuccEss") {
                console.log("wso2 return success");
                return localLogin(db);
            } else {
                console.log("wso2 return Fail" + req.body.username);
                return {
                    "invalidUser": "FAIL"
                };
            }
        }, function fail(err) {
            console.log("errrr", err);
            db.close();
            throw (err);
        });
    }

    function localLogin(db) {
        return db.collection('users').findOne({
            'userName': _.toUpper(usrName)
        }).then(function(result) {
            db.close();
            if (result === null) {
                return {
                    "invalidUser": "FAIL"
                };
            }
            result.role = result.defaultRoles;
            delete result.defaultRoles;
            return {
                "user": result,
                "createTime": new Date()
            };
        }, function(err) {
            db.close();
            ErrorHandler.handleError(err);
        }).catch(ErrorHandler.handleError(res));
    }

    function returnResult(result) {
        if (result.user) {
            res.send(201, {
                id_token: jwt.sign(result, config.secret, {
                    expiresIn: 60 * 60 * 5
                })
            });
            next();
        } else if (result.invalidUser) {
            console.log("Invalid User/Password : " + req.body.username);
            res.send(401, "Invalid User/Password");
            next();
        } else {
            res.send(403, "Unauthorised User");
            // ErrorHandler.handleError(res, 403, "User Doesnt have permission");
            next();
        }
    }
};
