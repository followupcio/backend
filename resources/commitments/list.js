var _ = require('lodash');
var ErrorHandler = require('../common/error');
var Database = require('../common/database');
var BasicValidation = require('../common/basicValidation');
var ConstantKeys = require('../common/constantKeys');
let getChildren = require('./getChildren');
var findActions = require('../common/findActions');
module.exports = function listCommitments(req, res, next) {
    var pageNo = req.query.page;
    var pageSize = req.query.per_page;
    var status = req.query.status;
    var timeframe = req.query.timeframe;
    var assignedBy = req.query.assignedBy;
    var assignedTo = req.query.assignedTo;
    var userOperator = req.query.userOperator;
    var tag = req.query.tag;
    var category = req.query.category;
    var subcategory = req.query.subcategory;
    var source = req.query.source;
    var parent = req.query.parent_id;
    // console.log("pageNo:"+pageNo+" pageSize:"+pageSize+" status:"+status+" timeframe:"+timeframe+" cmtFrom:"+cmtFrom+" cmtTo:"+cmtTo);
    var objectID = require('mongodb').ObjectID;
    var queryJson = {};
    BasicValidation.userHasPermision().then(function (hasPermission) {
        Database().connect().then(prepareQuery).then(findDocument).then(addTimeframe).then(addActions).then(nestChildrenDemands).then(returnResult).catch(ErrorHandler.handleError(res));
    }, ErrorHandler.handleCustomError(res, 401)); //user doesnt have permission
    function prepareQuery(db) {
        if (pageNo == undefined || parseInt(pageNo, 10) <= 0 || isNaN(parseInt(pageNo, 10))) {
            pageNo = 1;
        }
        if (pageSize == undefined || parseInt(pageSize, 10) <= 0 || parseInt(pageSize, 10) > 1000 || isNaN(parseInt(pageSize, 10))) {
            pageSize = 10;
        }
        if (_.has(ConstantKeys.demand_status, _.toUpper(status))) {
            if (_.toUpper(status) == ConstantKeys.demand_status.ONGOING) { //temporary, if ONGOING return voth ongoing and submitted
                queryJson['$or'] = [{
                    'status': 'ONGOING'
                }, {
                    'status': 'SUBMITTED'
                }];
            } else {
                queryJson['status'] = _.toUpper(status);
            }
            if (_.toUpper(status) == ConstantKeys.demand_status.ONGOING && _.has(ConstantKeys.timeframe_Ongoing, _.toUpper(timeframe))) {
                queryJson['deadline'] = _.get(ConstantKeys.timeframe_Ongoing, _.toUpper(timeframe));
            }
            if (_.toUpper(status) == ConstantKeys.demand_status.COMPLETED && _.has(ConstantKeys.timeframe_Complete, _.toUpper(timeframe))) {
                queryJson['completedDate'] = _.get(ConstantKeys.timeframe_Complete, _.toUpper(timeframe));
            }
        }
        // just in case if we want an OR operator for assignedBy & assignedTo
        if (userOperator != undefined && _.toUpper(userOperator) == "OR" && assignedBy != undefined && assignedTo != undefined) {
            queryJson['$or'] = [{
                "assignedBy": {
                    "$in": [new objectID(assignedBy)]
                }
            }, {
                "assignedTo": {
                    "$in": [new objectID(assignedTo)]
                }
            }];
        } else {
            if (assignedBy != null && assignedBy != "" && assignedBy != undefined) { //use $in instead of regex toavaoid nosql injection
                queryJson['assignedBy'] = {
                    "$in": [new objectID(assignedBy)]
                };
            }
            if (assignedTo != null && assignedTo != "" && assignedTo != undefined) { //use $in instead of regex toavaoid nosql injection
                queryJson['assignedTo'] = {
                    "$in": [new objectID(assignedTo)]
                };
            }
        }
        if (tag != null && tag != "" && tag != undefined) {
            queryJson['tag'] = {
                "$all": [tag]
            };
        }
        if (_.has(ConstantKeys.category, _.toUpper(category))) {
            queryJson['category'] = {
                "$in": [_.toUpper(category)]
            };
        }
        if (_.has(ConstantKeys.subcategory, _.toUpper(subcategory))) {
            queryJson['subcategory'] = {
                "$in": [_.toUpper(subcategory)]
            };
        }
        if (_.has(ConstantKeys.demand_source, _.toUpper(source))) {
            queryJson['source'] = _.toUpper(source);
        }
        console.log(queryJson, userOperator);
        return db;
    }

    function findDocument(db) {
        return db.collection('commitments').aggregate([{
            $match: queryJson
        }, {
            $lookup: {
                from: "users",
                localField: "assignedTo",
                foreignField: "_id",
                as: "assignedTo"
            }
        }, {
            $lookup: {
                from: "users",
                localField: "createdBy",
                foreignField: "_id",
                as: "createdBy"
            }
        }, {
            $lookup: {
                from: "users",
                localField: "assignedBy",
                foreignField: "_id",
                as: "assignedBy"
            }
        }, {
            $lookup: {
                from: "resources",
                localField: "category",
                foreignField: "key",
                as: "category"
            }
        }, {
            $lookup: {
                from: "resources",
                localField: "subcategory",
                foreignField: "key",
                as: "subcategory"
            }
        }, {
            $lookup: {
                from: "resources",
                localField: "status",
                foreignField: "key",
                as: "status"
            }
        }, {
            $unwind: {
                path: "$assignedTo",
                preserveNullAndEmptyArrays: true
            }
        }, {
            $unwind: {
                path: "$createdBy",
                preserveNullAndEmptyArrays: true
            }
        }, {
            $unwind: {
                path: "$assignedBy",
                preserveNullAndEmptyArrays: true
            }
        }, {
            $unwind: {
                path: "$category",
                preserveNullAndEmptyArrays: true
            }
        }, {
            $unwind: {
                path: "$subcategory",
                preserveNullAndEmptyArrays: true
            }
        }, {
            $unwind: {
                path: "$status",
                preserveNullAndEmptyArrays: true
            }
        }, {
            $sort: {
                "updatedDate": -1
            }
        },
        // { $sort: { "deadline": 1 } },
        {
            $project: {
                "title": 1,
                "status": {
                    "key": "$status.key",
                    "label": "$status.label"
                },
                "read": 1,
                "parent_id": 1,
                "parent": 1,
                "category": {
                    "key": "$category.key",
                    "label": "$category.label"
                },
                "subcategory": {
                    "key": "$subcategory.key",
                    "label": "$subcategory.label"
                },
                "meetingAssociated": 1,
                "createdDate": 1,
                "assignedTo": {
                    "_id": "$assignedTo._id",
                    "firstName": "$assignedTo.firstName",
                    "lastName": "$assignedTo.lastName",
                    "displayName": "$assignedTo.displayName",
                    "title": "$assignedTo.title",
                    "fullName": "$assignedTo.fullName",
                    "email": "$assignedTo.email",
                    "phone": "$assignedTo.phone",
                    "division": "$assignedTo.division",
                    "imageUrl": "$assignedTo.imageUrl",
                    "indexNo": "$assignedTo.indexNo"
                },
                "createdBy": {
                    "_id": "$createdBy._id",
                    "firstName": "$createdBy.firstName",
                    "lastName": "$createdBy.lastName",
                    "displayName": "$createdBy.displayName",
                    "title": "$createdBy.title",
                    "fullName": "$createdBy.fullName",
                    "email": "$createdBy.email",
                    "phone": "$createdBy.phone",
                    "division": "$createdBy.division",
                    "imageUrl": "$createdBy.imageUrl",
                    "indexNo": "$createdBy.indexNo"
                },
                // "timeframe": {
                // 	$cond: [{ $or: [{ $eq: ["$status.key", ConstantKeys.demand_status.ONGOING] }, { $eq: ["$status.key", ConstantKeys.demand_status.SUBMITTED] }] },
                // 	{
                // 		$cond: [{ $gte: ["$deadline", _.get(ConstantKeys.timeframe_Ongoing_status, 'ONTIME')] }, "ONTIME",
                // 		{ $cond: [{ $lt: ["$deadline", _.get(ConstantKeys.timeframe_Ongoing_status, 'EXPIRED')] }, "EXPIRED", "EXPIRING"] }]
                // 	}, ""]
                // },
                "assignedBy": {
                    "_id": "$assignedBy._id",
                    "firstName": "$assignedBy.firstName",
                    "lastName": "$assignedBy.lastName",
                    "displayName": "$assignedBy.displayName",
                    "title": "$assignedBy.title",
                    "fullName": "$assignedBy.fullName",
                    "email": "$assignedBy.email",
                    "phone": "$assignedBy.phone",
                    "division": "$assignedBy.division",
                    "imageUrl": "$assignedBy.imageUrl",
                    "indexNo": "$assignedBy.indexNo"
                },
                "deadline": 1,
                "source": 1,
                "attachments": 1,
            }
        }
        ]).skip(parseInt(pageSize, 10) * (parseInt(pageNo, 10) - 1)).limit(parseInt(pageSize, 10)).toArray().then(function (result) {
            return {
                db: db,
                result: result
            };
        }, function (err) {
            db.close();
            ErrorHandler.handleError(err);
        }).catch(ErrorHandler.handleError(res));
    }

    function addTimeframe(params) {
        var varTimeframe;

        console.log("--------------------------------------------------------")
        console.log("--------------------------------------------------------")
        console.log("--------------------------------------------------------")
        console.log("--------------------------------------------------------")
        console.log("--------------------------------------------------------")
        console.log("--------------------------------------------------------")
        console.log(params.result.length)
        params.result.forEach(function (commitment) {
            console.log("addTimeframe : " + commitment.deadline);
            try {
                if (new Date(commitment.deadline) < new Date()) {
                    console.log("EXPIRED");
                    varTimeframe = "EXPIRED";
                    commitment.timeframe = varTimeframe;
                } else if (new Date(commitment.deadline) < new Date(new Date().getTime() + 1000 * 3600 * 24 * 3)) {
                    console.log("EXPIRING");
                    varTimeframe = "EXPIRING";
                    commitment.timeframe = varTimeframe;
                } else if (new Date(commitment.deadline) > new Date(new Date().getTime() + 1000 * 3600 * 24 * 3)) {
                    console.log("ONTIME");
                    varTimeframe = "ONTIME";
                    commitment.timeframe = varTimeframe;
                }
                console.log("addTimeframe " + varTimeframe);
            } catch (error) {
                console.log("error " + error);
                ErrorHandler.handleError(res);
            }
        });
        try {
           // params.db.close();
        } catch (error) {
            console.log("error " + error);
            ErrorHandler.handleError(res);
        }
        return {
            db: params.db,
            result: params.result
        }
    }



    function addActions(params) {
        var varTimeframe;


        console.log(params.result.length)
        params.result.forEach(function (commitment) {
            console.log("addActions : " + commitment._id);
            try {
                findActions(params.db, commitment._id, commitment.createdBy, res, commitment)
                    .then(function (params) {
                      //  console.log("addActions : " + JSON.stringify(params.result));
                        commitment.actions = params.result;
                        //return params.commitment;

                    })
            } catch (error) {
                console.log("error " + error);
                ErrorHandler.handleError(res);
            }
        });
        try {
            params.db.close();
        } catch (error) {
            console.log("error " + error);
            ErrorHandler.handleError(res);
        }
        return {
            db: params.db,
            result: params.result
        }
    }

    function nestChildrenDemands(params) {
        return new Promise((resolve, reject) => {
            let nested = []
            let ids = []
            let demands = params.result
            demands.forEach(function (demand) {
                if (demand.parent) {
                    ids.push(String(demand._id))
                }
            });

            if(ids.length > 0)
            {
                  getChildren(ids)
                .then((children) => {
                    demands.forEach((demand) => {

                        let childs = children.filter(c => String(c.parent_id) === String(demand._id));
                        if (childs.length) {
                            demand.children = childs;
                        }
                    })
                    resolve(demands)
                })

            }else{
                 resolve(demands)
            }
          
        })
    }

    function returnResult(result) {
        res.send(result);
        next();
    }
}