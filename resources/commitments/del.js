var ErrorHandler = require('../common/error');
var Database = require('../common/database');
var BasicValidation = require('../common/basicValidation');

module.exports = function delCommitment(req, res, next) {
  var cid = req.params.id;
  var objectID = require('mongodb').ObjectID;

  BasicValidation.userHasPermision().then(
    function (hasPermission) {
      BasicValidation.documentExists("commitments", cid).then(
        function (validData) {
          Database().connect()
            .then(deleteAssociatedComments)
            .then(deleteCommitment)
            .then(returnResult)
            .catch(ErrorHandler.handleError(res));
        }, ErrorHandler.handleCustomError(res, 412));//basic data validation fail
    }, ErrorHandler.handleCustomError(res, 401));//user doesnt have permission

  function deleteAssociatedComments(db) {
    return db.collection('comments').remove({ "commitmentId": new objectID(cid) })
      .then(function (result) { return db; },
      function (err) { db.close(); ErrorHandler.handleError(err); })
      .catch(ErrorHandler.handleError(res));
  }

  function deleteCommitment(db) {
    return db.collection('commitments').deleteOne({ _id: new objectID(cid) })
      .then(function (result) { db.close(); return result; },
      function (err) { db.close(); ErrorHandler.handleError(err); })
      .catch(ErrorHandler.handleError(res));
  }

  function returnResult(result) {
    // console.log(result);
    if (result.result.n == 1 && result.result.ok == 1) {
      res.send({ message: 'commitment deleted' });
    } else {
      res.statusCode = 404;
      res.send({ message: 'commitment Not Fount' });
    }
    next();
  }
}