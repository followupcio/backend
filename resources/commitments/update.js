var ErrorHandler = require('../common/error');
var Database = require('../common/database');
var FsdAPI = require('../common/fsd-api');
var BasicValidation = require('../common/basicValidation');
var _ = require('lodash');
var ConstantKeys = require('../common/constantKeys');
var findActions = require('../common/findActions');
var sendMail = require('../common/sendMail');

module.exports = function updateCommitment(req, res, next) {
  var body = req.body;
  var cid = req.params.id;
  var objectID = require('mongodb').ObjectID;

  BasicValidation.userHasPermision()
    .then(function (hasPermission) {
      BasicValidation.updateCommitmentValidation(body, cid).then(
        function (validData) {
          Database().connect()
            .then(prepareDataToUpdate)
            .then(updateCommitment)
            .then(addTimeframe)
            .then(insertAction)
            .then(function (params) {
              return findActions(params.db, cid, params.commitment.createdBy, res, params.commitment)
            })
            .then(function (params) {
              params.commitment.actions = params.result;
              return params;
            })

            .then(returnResult)
            .catch(ErrorHandler.handleError(res));
        }, ErrorHandler.handleCustomError(res, 412));//basic data validation fail
    }, ErrorHandler.handleCustomError(res, 401));//user doesnt have permission

  function prepareDataToUpdate(db) {

    if (body.hasOwnProperty('assignedBy')) {
      body.assignedBy = new objectID(body.assignedBy);
    }
    if (body.hasOwnProperty('assignedTo')) {
      body.assignedTo = new objectID(body.assignedTo);
    }

    if (_.has(ConstantKeys.demand_source, _.toUpper(body.source))) {
      body.source = _.toUpper(body.source);
    }


    body.updatedDate = new Date();

    if (body.hasOwnProperty('finalApproval')) {
      if (body.finalApproval) {
        body.approvedBy = body.updatedBy;
        body.approvedDate = new Date();
      } else {
        body.approvedBy = null;
        body.approvedDate = null;
      }
    }
    console.log("prepareDataToUpdate");
    return db;
  }

  function updateCommitment(db) {
    console.log("body" + JSON.stringify(body));

    return db.collection('commitments').updateOne({ _id: new objectID(cid) }, { $set: body }, { upsert: false })
      .then(function (updateResult) {
        console.log("updateResult" + JSON.stringify(updateResult));

        if (updateResult.modifiedCount === 1) {
          return db.collection('commitments').aggregate([
            { $match: { _id: new objectID(cid) } },
            { $lookup: { from: "users", localField: "assignedTo", foreignField: "_id", as: "assignedTo" } },
            { $lookup: { from: "users", localField: "assignedBy", foreignField: "_id", as: "assignedBy" } },
            { $lookup: { from: "users", localField: "createdBy", foreignField: "_id", as: "createdBy" } },
            { $lookup: { from: "resources", localField: "category", foreignField: "key", as: "category" } },
            { $lookup: { from: "resources", localField: "subcategory", foreignField: "key", as: "subcategory" } },
            { $lookup: { from: "resources", localField: "status", foreignField: "key", as: "status" } },
            { $lookup: { from: "meetings", localField: "meetingAssociated", foreignField: "_id", as: "meetingAssociated" } },
            { $unwind: { path: "$assignedTo", preserveNullAndEmptyArrays: true } },
            { $unwind: { path: "$createdBy", preserveNullAndEmptyArrays: true } },
            { $unwind: { path: "$assignedBy", preserveNullAndEmptyArrays: true } },
            { $unwind: { path: "$category", preserveNullAndEmptyArrays: true } },
            { $unwind: { path: "$subcategory", preserveNullAndEmptyArrays: true } },
            { $unwind: { path: "$status", preserveNullAndEmptyArrays: true } },
            { $unwind: { path: "$meetingAssociated", preserveNullAndEmptyArrays: true } },
            {
              $project: {
                "title": 1,
                "status": { "key": "$status.key", "label": "$status.label" },
                "read": 1,
                "parent": 1,
                "link": 1,
                "category": { "key": "$category.key", "label": "$category.label" },
                "subcategory": { "key": "$subcategory.key", "label": "$subcategory.label" },
                "meetingAssociated": {
                  "_id": "$meetingAssociated._id",
                  "meetingTitle": "$meetingAssociated.meetingTitle",
                  "authorityName": "$meetingAssociated.authorityName",
                  "authorityRole": "$meetingAssociated.authorityRole",
                  "agenda": "$meetingAssociated.agenda"
                },
                "agendaPoint": 1,                
                "createdBy": {
                  "_id": "$createdBy._id", "firstName": "$createdBy.firstName",
                  "lastName": "$createdBy.lastName", "displayName": "$createdBy.displayName", "title": "$createdBy.title",
                  "fullName": "$createdBy.fullName", "email": "$createdBy.email",
                  "phone": "$createdBy.phone", "division": "$createdBy.division",
                  "imageUrl": "$createdBy.imageUrl", "indexNo": "$createdBy.indexNo"
                }, "createdDate": 1,
                "assignedTo": {
                  "_id": "$assignedTo._id", "firstName": "$assignedTo.firstName",
                  "lastName": "$assignedTo.lastName", "title": "$assignedTo.title",
                  "fullName": "$assignedTo.fullName", "email": "$assignedTo.email",
                  "phone": "$assignedTo.phone", "division": "$assignedTo.division",
                  "imageUrl": "$assignedTo.imageUrl", "indexNo": "$assignedTo.indexNo"
                },
                "assignedBy": {
                  "_id": "$assignedBy._id", "firstName": "$assignedBy.firstName",
                  "lastName": "$assignedBy.lastName", "title": "$assignedBy.title",
                  "fullName": "$assignedBy.fullName", "email": "$assignedBy.email",
                  "phone": "$assignedBy.phone", "division": "$assignedBy.division",
                  "imageUrl": "$assignedBy.imageUrl", "indexNo": "$assignedBy.indexNo"
                },
                "timeframe": {
                  $cond: [{ $eq: ["$status.key", ConstantKeys.demand_status.ONGOING] },
                  {
                    $cond: [{ $gte: ["$deadline", _.get(ConstantKeys.timeframe_Ongoing_status, 'ONTIME')] }, "ONTIME",
                    { $cond: [{ $lt: ["$deadline", _.get(ConstantKeys.timeframe_Ongoing_status, "EXPIRED")] }, "EXPIRED", "EXPIRING"] }]
                  }, ""]
                },
                "deadline": 1, "updatedBy": 1, "updatedDate": 1,
                "attachments": 1,
              }
            }]).toArray().then(function (result) {


              sendMail(db, cid, body, res);
              console.log("Update commitment stampa");
              return { db: db, commitment: result[0] };
            })
        }

        else { throw { "message": "Error Updating Commitment" }; }
      },
      function (err) { db.close(); ErrorHandler.handleError(err); })
      .catch(ErrorHandler.handleError(res));

  }

  function insertAction(params) {
    console.log("Created : " + params.commitment._id);
    return params.db.collection('actions').insertOne({
      commitment: new objectID(params.commitment._id),
      // assignedTo: new objectID(body.assignedTo), //TODO to define in future
      author: new objectID(params.commitment.updatedBy), //TODO: get from json token
      timestamp: body.updatedDate,
      type: ConstantKeys.actions.UPDATE,
      description: "Update of Commitment"

    })
      .then(function (result) {
        console.log("added action : ");
        return { db: params.db, result: result, commitment: params.commitment };
      })

      .catch(ErrorHandler.handleError(res));
  }

  function addTimeframe(params) {

    try {

      var varTimeframe;
      if (new Date(params.commitment.deadline) < new Date()) {
        console.log("EXPIRED");
        varTimeframe = "EXPIRED";
        params.commitment.timeframe = varTimeframe;
      }
      else if (new Date(params.commitment.deadline) < new Date(new Date().getTime() + 1000 * 3600 * 24 * 3)) {
        console.log("EXPIRING");
        varTimeframe = "EXPIRING";
        params.commitment.timeframe = varTimeframe;
      }
      else if (new Date(params.commitment.deadline) > new Date(new Date().getTime() + 1000 * 3600 * 24 * 3)) {
        console.log("ONTIME");
        varTimeframe = "ONTIME";
        params.commitment.timeframe = varTimeframe;
      }

      console.log("addTimeframe " + varTimeframe);
      return { db: params.db, commitment: params.commitment }

    } catch (error) {
      console.log("error " + error);
      ErrorHandler.handleError(res);
    }

  }

  function returnResult(params) {
    res.send(params.commitment);
    next();
  }
}