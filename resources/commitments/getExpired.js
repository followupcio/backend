var ErrorHandler = require('../common/error');
var Database = require('../common/database');
var Promise = require('bluebird');
var moment = require('moment');
var ConstantKeys = require('../common/constantKeys');
/**
 * It Retrives all Commitments that
 * are Soon to Expire
 * @return {[type]} [description]
 */
let getExpired = function () {

    return new Promise(function (resolve, reject) {
        console.log(" getExpired : ");
        var queryJson = { 'status': ConstantKeys.demand_status.ONGOING };
        var assignee = [];
        var now = new Date();
        now.setHours(17, 0, 0, 0);

        // Connect to the DB
        Database().connect()
            // Wait for connection
            .then(function (db) {
                resolve(db.collection('commitments').aggregate([
                    { $match: queryJson },
                    { $lookup: { from: "users", localField: "assignedTo", foreignField: "_id", as: "assignedTo" } },
                ]).toArray()
                    .then(function (result) {
                        result.forEach(function (commitment) {
                            if (
                                (new Date(commitment.deadline).setHours(0, 0, 0, 0) < now.setHours(0, 0, 0, 0))
                            ) {
                                commitment.assignedTo[0].cid = commitment._id;
                                commitment.assignedTo[0].deadline = new Date(commitment.deadline).toISOString().substring(0, 10);
                                commitment.assignedTo[0].title = commitment.title;
                                assignee.push(commitment.assignedTo[0]);
                                console.log("Expired : " + commitment._id);
                            }
                        });
                        db.close();
                        console.log("assignee " + JSON.stringify(assignee));
                        return assignee;
                    })
                );
            }).catch(function (error) {
                reject(error);
            })
    });

}
module.exports = getExpired;