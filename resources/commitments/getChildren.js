let ErrorHandler = require('../common/error');
let Database = require('../common/database');
let Promise = require('bluebird');
let moment = require('moment');
var findActions = require('../common/findActions');
module.exports = function getChildren(parentIds) {

    function findDocument(db, parentIds) {
        return db.collection('commitments').aggregate([{
            $match: { parent_id: { $in: parentIds } }
        }, {
            $lookup: {
                from: "users",
                localField: "assignedTo",
                foreignField: "_id",
                as: "assignedTo"
            }
        }, {
            $lookup: {
                from: "users",
                localField: "createdBy",
                foreignField: "_id",
                as: "createdBy"
            }
        }, {
            $lookup: {
                from: "users",
                localField: "assignedBy",
                foreignField: "_id",
                as: "assignedBy"
            }
        }, {
            $lookup: {
                from: "resources",
                localField: "category",
                foreignField: "key",
                as: "category"
            }
        }, {
            $lookup: {
                from: "resources",
                localField: "subcategory",
                foreignField: "key",
                as: "subcategory"
            }
        }, {
            $lookup: {
                from: "resources",
                localField: "status",
                foreignField: "key",
                as: "status"
            }
        }, {
            $unwind: {
                path: "$assignedTo",
                preserveNullAndEmptyArrays: true
            }
        }, {
            $unwind: {
                path: "$createdBy",
                preserveNullAndEmptyArrays: true
            }
        }, {
            $unwind: {
                path: "$assignedBy",
                preserveNullAndEmptyArrays: true
            }
        }, {
            $unwind: {
                path: "$category",
                preserveNullAndEmptyArrays: true
            }
        }, {
            $unwind: {
                path: "$subcategory",
                preserveNullAndEmptyArrays: true
            }
        }, {
            $unwind: {
                path: "$status",
                preserveNullAndEmptyArrays: true
            }
        }, {
            $sort: {
                "updatedDate": -1
            }
        },
        // { $sort: { "deadline": 1 } },
        {
            $project: {
                "title": 1,
                "status": {
                    "key": "$status.key",
                    "label": "$status.label"
                },
                "read": 1,
                "parent_id": 1,
                "parent": 1,
                "category": {
                    "key": "$category.key",
                    "label": "$category.label"
                },
                "subcategory": {
                    "key": "$subcategory.key",
                    "label": "$subcategory.label"
                },
                "meetingAssociated": 1,
                "createdDate": 1,
                "assignedTo": {
                    "_id": "$assignedTo._id",
                    "firstName": "$assignedTo.firstName",
                    "lastName": "$assignedTo.lastName",
                    "displayName": "$assignedTo.displayName",
                    "title": "$assignedTo.title",
                    "fullName": "$assignedTo.fullName",
                    "email": "$assignedTo.email",
                    "phone": "$assignedTo.phone",
                    "division": "$assignedTo.division",
                    "imageUrl": "$assignedTo.imageUrl",
                    "indexNo": "$assignedTo.indexNo"
                },
                "createdBy": {
                    "_id": "$createdBy._id",
                    "firstName": "$createdBy.firstName",
                    "lastName": "$createdBy.lastName",
                    "displayName": "$createdBy.displayName",
                    "title": "$createdBy.title",
                    "fullName": "$createdBy.fullName",
                    "email": "$createdBy.email",
                    "phone": "$createdBy.phone",
                    "division": "$createdBy.division",
                    "imageUrl": "$createdBy.imageUrl",
                    "indexNo": "$createdBy.indexNo"
                },
                // "timeframe": {
                // 	$cond: [{ $or: [{ $eq: ["$status.key", ConstantKeys.demand_status.ONGOING] }, { $eq: ["$status.key", ConstantKeys.demand_status.SUBMITTED] }] },
                // 	{
                // 		$cond: [{ $gte: ["$deadline", _.get(ConstantKeys.timeframe_Ongoing_status, 'ONTIME')] }, "ONTIME",
                // 		{ $cond: [{ $lt: ["$deadline", _.get(ConstantKeys.timeframe_Ongoing_status, 'EXPIRED')] }, "EXPIRED", "EXPIRING"] }]
                // 	}, ""]
                // },
                "assignedBy": {
                    "_id": "$assignedBy._id",
                    "firstName": "$assignedBy.firstName",
                    "lastName": "$assignedBy.lastName",
                    "displayName": "$assignedBy.displayName",
                    "title": "$assignedBy.title",
                    "fullName": "$assignedBy.fullName",
                    "email": "$assignedBy.email",
                    "phone": "$assignedBy.phone",
                    "division": "$assignedBy.division",
                    "imageUrl": "$assignedBy.imageUrl",
                    "indexNo": "$assignedBy.indexNo"
                },
                "deadline": 1,
                "source": 1,
                "attachments": 1,
            }
        }
        ]).toArray().then(function (result) {
            return { db: db, result: result }

        }, function (err) {
            db.close();
            ErrorHandler.handleError(err);
        })
    }

    function addTimeframe(children) {
        let varTimeframe;
        children.forEach(function (commitment) {
            console.log("addTimeframe : " + commitment.deadline);
            try {
                if (new Date(commitment.deadline) < new Date()) {
                    console.log("EXPIRED");
                    varTimeframe = "EXPIRED";
                    commitment.timeframe = varTimeframe;
                } else if (new Date(commitment.deadline) < new Date(new Date().getTime() + 1000 * 3600 * 24 * 3)) {
                    console.log("EXPIRING");
                    varTimeframe = "EXPIRING";
                    commitment.timeframe = varTimeframe;
                } else if (new Date(commitment.deadline) > new Date(new Date().getTime() + 1000 * 3600 * 24 * 3)) {
                    console.log("ONTIME");
                    varTimeframe = "ONTIME";
                    commitment.timeframe = varTimeframe;
                }
                console.log("addTimeframe " + varTimeframe);
            } catch (error) {
                console.log("error " + error);
                ErrorHandler.handleError(res);
            }
        });
    }

    function addActions(params) {

        return new Promise(function (resolve, reject) {

            var varTimeframe;
            var children = params.result;
            children.forEach(function (commitment,index) {
        
            try {
                    findActions(params.db, commitment._id, commitment.createdBy, null, commitment)
                        .then(function (params) {                         
                            commitment.actions = params.result;
                            //return params.commitment;
                            if(index == children.length -1)
                            {
                                resolve(children);
                            }
                            

                        })
                } catch (error) {
                    console.log("error " + error);
                    ErrorHandler.handleError(res);
                }
            });
            try {
                params.db.close();
            } catch (error) {
                console.log("error " + error);
                ErrorHandler.handleError(res);
            }
        

        });

    }

    return new Promise(function (resolve, reject) {

        // Connect to the DB
        Database().connect()
            // Wait for connection
            .then(function (db) {

                findDocument(db, parentIds)
                    .then((params) => {
    
                        addTimeframe(params.result)
                        addActions(params)
                        
                        .then((result) => {
                            console.log("----------------------***************************************************************************************");
                            console.log("----------------------***************************************************************************************");
                            console.log(JSON.stringify(result));

                            resolve(result)
                        })

                    })

            }).catch(function (error) {
                reject(error);
            })
    });
};