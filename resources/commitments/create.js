var ErrorHandler = require('../common/error');
var Database = require('../common/database');
var FsdAPI = require('../common/fsd-api');
var BasicValidation = require('../common/basicValidation');
var ConstantKeys = require('../common/constantKeys');
var _ = require('lodash');
var sendMail = require('../common/sendMail');

module.exports = function createCommitment(req, res, next) {
	var objectID = require('mongodb').ObjectID;
	var body = req.body;

	BasicValidation.userHasPermision().then(
		function (hasPermission) {
			BasicValidation.createCommitmentValidation(body).then(
				function (validData) {
					Database().connect()
						.then(prepareDataToInsert)
						.then(insertCommitment)
						.then(insertAction)
						.then(returnResult)
						.catch(ErrorHandler.handleError(res));
				}, ErrorHandler.handleCustomError(res, 412));//basic data validation fail
		}, ErrorHandler.handleCustomError(res, 401));//user doesnt have permission

	function prepareDataToInsert(db) {
		if (!body.hasOwnProperty('assignedBy')) {
			if (process.env.environment == "development")
				body.assignedBy = new objectID("58e3afb08f1801419a128bed"); //TODO: get from json token
			else if (process.env.environment == "demo")
				body.assignedBy = new objectID("58a3085f7d994f79a0cbecb1"); //TODO: get from json token
		} else {
			body.assignedBy = new objectID(body.assignedBy);
			body.createdBy = new objectID(body.assignedBy);
		}

		if (body.hasOwnProperty('meetingAssociated')) {
			body.meetingAssociated = new objectID(body.meetingAssociated);
		}

		if (_.has(ConstantKeys.demand_source, _.toUpper(body.source))) {
			body.source = _.toUpper(body.source);
		} else {
			body.source = ConstantKeys.demand_source.STAFF;
		}

		body.assignedTo = new objectID(body.assignedTo);
		// if (body.createdBy) {
		// 	if (process.env.environment == "development")
		// 		body.createdBy = new objectID("58e3afb08f1801419a128bed"); //TODO: get from json token
		// 	else if (process.env.environment == "demo")
		// 		body.createdBy = new objectID("58a3085f7d994f79a0cbecb1"); //TODO: get from json token
		// }

		body.createdDate = new Date();
		body.updatedDate = new Date();
		body.viewers = [];
		// console.log("Data Prepared to insert");
		return db;
	}

	function insertCommitment(db) {
		return db.collection('commitments').insertOne(body).then(function (insResult) {

			//  return db.collection('commitments').findOne({ _id: new objectID(insResult.insertedId) }).

			return db.collection('commitments').aggregate([
				{ $match: { _id: new objectID(insResult.insertedId) } },
				{ $lookup: { from: "users", localField: "assignedTo", foreignField: "_id", as: "assignedTo" } },
				{ $lookup: { from: "users", localField: "assignedBy", foreignField: "_id", as: "assignedBy" } },
				{ $lookup: { from: "resources", localField: "category", foreignField: "key", as: "category" } },
				{ $lookup: { from: "resources", localField: "subcategory", foreignField: "key", as: "subcategory" } },
				{ $lookup: { from: "resources", localField: "status", foreignField: "key", as: "status" } },
				{ $lookup: { from: "meetings", localField: "meetingAssociated", foreignField: "_id", as: "meetingAssociated" } },
				{ $unwind: { path: "$assignedTo", preserveNullAndEmptyArrays: true } },
				{ $unwind: { path: "$assignedBy", preserveNullAndEmptyArrays: true } },
				{ $unwind: { path: "$category", preserveNullAndEmptyArrays: true } },
				{ $unwind: { path: "$subcategory", preserveNullAndEmptyArrays: true } },
				{ $unwind: { path: "$status", preserveNullAndEmptyArrays: true } },
				{ $unwind: { path: "$meetingAssociated", preserveNullAndEmptyArrays: true } },
				{
					$project: {
						"title": 1,
						"status": { "key": "$status.key", "label": "$status.label" },
						"read": 1,
						"parent": 1,
						"parent_id": 1,
						"description":1,
						"category": { "key": "$category.key", "label": "$category.label" },
						"subcategory": { "key": "$subcategory.key", "label": "$subcategory.label" },
						"meetingAssociated": {
							"_id": "$meetingAssociated._id",
							"meetingTitle": "$meetingAssociated.meetingTitle",
							"authorityName": "$meetingAssociated.authorityName",
							"authorityRole": "$meetingAssociated.authorityRole",
							"agenda": "$meetingAssociated.agenda"
						},
						"agendaPoint": 1,
						"createdBy": 1, "createdDate": 1,
						"assignedTo": {
							"_id": "$assignedTo._id", "firstName": "$assignedTo.firstName",
							"lastName": "$assignedTo.lastName", "title": "$assignedTo.title",
							"fullName": "$assignedTo.fullName", "email": "$assignedTo.email",
							"phone": "$assignedTo.phone", "division": "$assignedTo.division",
							"imageUrl": "$assignedTo.imageUrl", "indexNo": "$assignedTo.indexNo"
						},
						"assignedBy": {
							"_id": "$assignedBy._id", "firstName": "$assignedBy.firstName",
							"lastName": "$assignedBy.lastName", "title": "$assignedBy.title",
							"fullName": "$assignedBy.fullName", "email": "$assignedBy.email",
							"phone": "$assignedBy.phone", "division": "$assignedBy.division",
							"imageUrl": "$assignedBy.imageUrl", "indexNo": "$assignedBy.indexNo"
						},
						"timeframe": {
							$cond: [{ $eq: ["$status.key", ConstantKeys.demand_status.ONGOING] },
							{
								$cond: [{ $gte: ["$deadline", _.get(ConstantKeys.timeframe_Ongoing_status, 'ONTIME')] }, "ONTIME",
								{ $cond: [{ $lt: ["$deadline", _.get(ConstantKeys.timeframe_Ongoing_status, "EXPIRED")] }, "EXPIRED", "EXPIRING"] }]
							}, ""]
						},
						"deadline": 1, "updatedBy": 1, "updatedDate": 1,
						"attachments": 1,
					}
				}]).toArray().then(function (result) {
					//db.close();
					//Here send the notification of the creation of commitment 


					sendMail(db, insResult.insertedId, body, res);
					return { db: db, result: result[0] };
				})
		}, function (err) { db.close(); ErrorHandler.handleError(err); })
			.catch(ErrorHandler.handleError(res));
	}


	function insertAction(params) {
		console.log("Created : " + params.result._id);
		return params.db.collection('actions').insertOne({
			commitment: new objectID(params.result._id),
			// assignedTo: new objectID(body.assignedTo), //TODO to define in future
			author: new objectID(params.result.createdBy), //TODO: get from json token
			timestamp: params.result.createdDate,
			type: ConstantKeys.actions.CREATION,
			description: "Creation of Commitment",
			attachment: new objectID(body.attachmentId)
		})
			.then(function (result) {
				// console.log("added action : " + result);
				return { db: params.db, result: result, commitment: params.result };
			})

			.catch(ErrorHandler.handleError(res));
	}

	function returnResult(params) {
		res.send(params.commitment);
		next();
	}
}