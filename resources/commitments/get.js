var ErrorHandler = require('../common/error');
var Database = require('../common/database');
var BasicValidation = require('../common/basicValidation');
var findActions = require('../common/findActions');
var ConstantKeys = require('../common/constantKeys');
var _ = require('lodash');

module.exports = function listCommitment(req, res, next) {
	var cid = req.params.id;
	var objectID = require('mongodb').ObjectID;

	BasicValidation.userHasPermision()
		.then(function (hasPermission) {
			Database().connect()
				.then(findDocument)
				.then(addTimeframe)
				.then(function (params) {
					return findActions(params.db, cid, params.result.createdBy, res, params.result)
				})
				.then(function (params) {

					params.commitment.actions = params.result;
					return params.commitment;

				})
				.then(returnResult)
				.catch(ErrorHandler.handleError(res));
		}, ErrorHandler.handleCustomError(res, 401));//user doesnt have permission

	function findDocument(db) {
		// return db.collection('commitments').findOne({ _id: new objectID(cid) });
		return db.collection('commitments').aggregate([
			{ $match: { _id: new objectID(cid) } },
			{ $lookup: { from: "users", localField: "assignedTo", foreignField: "_id", as: "assignedTo" } },
			{ $lookup: { from: "users", localField: "assignedBy", foreignField: "_id", as: "assignedBy" } },
			{ $lookup: { from: "users", localField: "createdBy", foreignField: "_id", as: "createdBy" } },
			{ $lookup: { from: "resources", localField: "category", foreignField: "key", as: "category" } },
			{ $lookup: { from: "resources", localField: "subcategory", foreignField: "key", as: "subcategory" } },
			{ $lookup: { from: "resources", localField: "status", foreignField: "key", as: "status" } },
			{ $lookup: { from: "meetings", localField: "meetingAssociated", foreignField: "_id", as: "meetingAssociated" } },
			{ $unwind: { path: "$assignedTo", preserveNullAndEmptyArrays: true } },
			{ $unwind: { path: "$assignedBy", preserveNullAndEmptyArrays: true } },
			{ $unwind: { path: "$createdBy", preserveNullAndEmptyArrays: true } },
			{ $unwind: { path: "$category", preserveNullAndEmptyArrays: true } },
			{ $unwind: { path: "$subcategory", preserveNullAndEmptyArrays: true } },
			{ $unwind: { path: "$status", preserveNullAndEmptyArrays: true } },
			{ $unwind: { path: "$meetingAssociated", preserveNullAndEmptyArrays: true } },
			{
				$project: {
					"title": 1,
					"status": { "key": "$status.key", "label": "$status.label" },
					"read": 1,
					"parent": 1,
					"parent_id": 1,
					"description":1,
					"category": { "key": "$category.key", "label": "$category.label" },
					"subcategory": { "key": "$subcategory.key", "label": "$subcategory.label" },
					"meetingAssociated": {
						"_id": "$meetingAssociated._id",
						"meetingTitle": "$meetingAssociated.meetingTitle",
						"authorityName": "$meetingAssociated.authorityName",
						"authorityRole": "$meetingAssociated.authorityRole",
						"agenda": "$meetingAssociated.agenda"
					},
					"agendaPoint": 1,
					"createdBy": {
						"_id": "$createdBy._id", "firstName": "$createdBy.firstName",
						"lastName": "$createdBy.lastName", "displayName": "$createdBy.displayName", "title": "$createdBy.title",
						"fullName": "$createdBy.fullName", "email": "$createdBy.email",
						"phone": "$createdBy.phone", "division": "$createdBy.division",
						"imageUrl": "$createdBy.imageUrl", "indexNo": "$createdBy.indexNo"
					},
					"createdDate": 1,
					"assignedTo": {
						"_id": "$assignedTo._id", "firstName": "$assignedTo.firstName",
						"lastName": "$assignedTo.lastName", "displayName": "$assignedTo.displayName", "title": "$assignedTo.title",
						"fullName": "$assignedTo.fullName", "email": "$assignedTo.email",
						"phone": "$assignedTo.phone", "division": "$assignedTo.division",
						"imageUrl": "$assignedTo.imageUrl", "indexNo": "$assignedTo.indexNo"
					},
					"assignedBy": {
						"_id": "$assignedBy._id", "firstName": "$assignedBy.firstName",
						"lastName": "$assignedBy.lastName", "displayName": "$assignedBy.displayName", "title": "$assignedBy.title",
						"fullName": "$assignedBy.fullName", "email": "$assignedBy.email",
						"phone": "$assignedBy.phone", "division": "$assignedBy.division",
						"imageUrl": "$assignedBy.imageUrl", "indexNo": "$assignedBy.indexNo"
					},
					"deadline": 1, "updatedBy": 1, "updatedDate": 1,
					"attachments": 1,
				}
			}]).toArray().then(function (result) { return { db: db, result: result[0] } },
			function (err) { db.close(); ErrorHandler.handleError(err); })
			.catch(ErrorHandler.handleError(res));
	}

	function addTimeframe(params) {
		console.log("addTimeframe" + params.result.deadline);
		console.log("NOW : " + new Date());


		try {

			var varTimeframe;
			if (new Date(params.result.deadline) < new Date()) {
				console.log("EXPIRED");
				varTimeframe = "EXPIRED";
				params.result.timeframe = varTimeframe;
			}
			else if (new Date(params.result.deadline) < new Date(new Date().getTime() + 1000 * 3600 * 24 * 3)) {
				console.log("EXPIRING");
				varTimeframe = "EXPIRING";
				params.result.timeframe = varTimeframe;
			}
			else if (new Date(params.result.deadline) > new Date(new Date().getTime() + 1000 * 3600 * 24 * 3)) {
				console.log("ONTIME");
				varTimeframe = "ONTIME";
				params.result.timeframe = varTimeframe;
			}

			console.log("addTimeframe " + varTimeframe);
			return { db: params.db, result: params.result }

		} catch (error) {
			console.log("error " + error);
			ErrorHandler.handleError(res);
		}

	}

	function returnResult(result) {
		res.send(result);
		next();
	}
};