
var _ = require('lodash');
//require('shelljs/global');
const path = require('path');
const fs = require('fs');
const writeFile = fs.writeFileSync;

const outputPath = "C:/work/CTS-followup/toodoo/resources/commitments/";
const dbPath = path.join(outputPath, 'db.json');

console.log('Building the Follow up system database...');

// utils

const pickRandomItemInArray = (array) =>
    array[Math.floor(Math.random() * array.length)];

const randomDate = (start, end) =>
    getRandomInteger(start.getTime(), end.getTime());

const getRandomInteger = (min, max) => {

    min = min || 0;
    max = max || 36;
    return Math.floor(Math.random() * (max - min + 1)) + min;
}


const times = (n, iterator) => {
    const accum = Array(Math.max(0, n));
    for (var i = 0; i < n; i++) accum[i] = iterator.call();
    return accum;
};

// initial models
const booleans = [true, false];
const sdg = ['SDG1', 'SDG2', 'SDG3', 'SDG4', 'SDG5', 'SDG6', 'SDG7', 'SDG8', 'SDG9', 'SDG10', 'SDG11', 'SDG12', 'SDG13', 'SDG14', 'SDG15', 'SDG16', 'SDG17'];
const sp = ['SP1', 'SP2', 'SP3', 'SP4', 'SP5'];
// const statuses = ['ONGOING', 'COMPLETED'];
const categories = ['SDG', 'SP'];
const names = ['58a308ae7d994f79a0cbecb3', '58a308bc7d994f79a0cbecb4', '58a308cd7d994f79a0cbecb5', '58b059a4c6ad691fe4af7524', '58b05ad7ed568261ac92e665', '58b05b0cd5386d094c42aafd', '58a308127d994f79a0cbecac',];
const demands = [
    'Preparation for UN summit on Climate Change',
    'Guidelines on Social Analysis for Rural area Developing Planning', 'Food Security and Nutrition Policy Assistance Programme Closure',
    'Support to policy seminars on Animal Health Investments', 'Report on Agricultural Reforms and Trade Liberalization',
    'Workshop on Policy impacts on income inquality and poverty',
    'Improving livelihoods of indigenous women in Nicaragua', 'State of the World: Ten reports you need in your library',
    'Improving food and nutrition security in the Philippines', 'Schools – the beginning of the end of malnutrition',
    'Promoting nutrition through schools can create benefits that extend beyond the classroom', 'FAO helps establish early warning systems for food and nutrition security',
    'FAO helps establish early warning systems for food and nutrition security', 'Centroamérica promueve sistemas financieros innovadores para la agricultura familiar',
    'Pulses and Soils – promoting symbiosis through crop rotation', 'The inclusion of pulses in multiple cropping systems is key to maintaining and increasing soil',
    'A dynamic tool for FAO Members to facilitate and support their participation in the work of the Organization.', 'A website with information for our resource partners',
    'Food security and complex emergencies', 'The Canary Current Large Marine Ecosystem project',
    'Linking smallholder potato farmers to the market while caring for the environment', 'FAO helps African universities incorporate trainings in nutrition education',
    'FAO helps build resilience in communities affected by the El Niño in Mindanao', 'FAO supports land rehabilitation in Angola’s indigenous and pastoral communities',
    'FAO provides disaster risk reduction and management training to Laotian farmers', 'FAO enables market access to agricultural inputs for smallholder farmers in Mozambique',
    'FAO helps Lesotho develop a comprehensive land cover database',
    'FAO provides recovery support to Zamboanga fisherfolk through creation of sustainable livelihood opportunities',
    'Income-generating livestock distribution helps conflict-affected families recover after devastating floods',
    'FAO supports sustainable agricultural growth in Burundi through farmer field schools', 'FAO introduces sustainable employment opportunities to assist poor rural households',
    'FAO’s aquaculture project helps improve household nutrition & food security in Northeastern Haiti', 'FAO destocking activities increase financial stability and livelihoods of herder communities',
    'Helps fight desert locust outbreaks by providing real-time detection tools', 'Supports local households to restore agricultural and good nutrition practices',
    'FAO seeds help Nepali farmers restore their livelihoods and strengthen their resilience', 'Helps costal communities bounce back and build resilient livelihoods',
    'FAO and Mozambique are working to locate, collect and dispose of highly hazardous pesticides', 'FAO reduces unemployment through training in agricultural and entrepreneurial skill',
    'Preparation for UN summit on Climate Change',
    'Guidelines on Social Analysis for Rural area Developing Planning', 'Food Security and Nutrition Policy Assistance Programme Closure',
    'Support to policy seminars on Animal Health Investments', 'Report on Agricultural Reforms and Trade Liberalization',
    'Workshop on Policy impacts on income inquality and poverty',
    'Improving livelihoods of indigenous women in Nicaragua', 'State of the World: Ten reports you need in your library',
    'Improving food and nutrition security in the Philippines', 'Schools – the beginning of the end of malnutrition',
    'Promoting nutrition through schools can create benefits that extend beyond the classroom', 'FAO helps establish early warning systems for food and nutrition security',
    'FAO helps establish early warning systems for food and nutrition security', 'Centroamérica promueve sistemas financieros innovadores para la agricultura familiar',
    'Pulses and Soils – promoting symbiosis through crop rotation', 'The inclusion of pulses in multiple cropping systems is key to maintaining and increasing soil',
    'A dynamic tool for FAO Members to facilitate and support their participation in the work of the Organization.', 'A website with information for our resource partners',
    'Food security and complex emergencies', 'The Canary Current Large Marine Ecosystem project',
    'Linking smallholder potato farmers to the market while caring for the environment', 'FAO helps African universities incorporate trainings in nutrition education',
    'FAO helps build resilience in communities affected by the El Niño in Mindanao', 'FAO supports land rehabilitation in Angola’s indigenous and pastoral communities',
    'FAO provides disaster risk reduction and management training to Laotian farmers', 'FAO enables market access to agricultural inputs for smallholder farmers in Mozambique',
    'FAO helps Lesotho develop a comprehensive land cover database',
    'FAO provides recovery support to Zamboanga fisherfolk through creation of sustainable livelihood opportunities',
    'Income-generating livestock distribution helps conflict-affected families recover after devastating floods',
    'FAO supports sustainable agricultural growth in Burundi through farmer field schools', 'FAO introduces sustainable employment opportunities to assist poor rural households',
    'FAO’s aquaculture project helps improve household nutrition & food security in Northeastern Haiti', 'FAO destocking activities increase financial stability and livelihoods of herder communities',
    'Helps fight desert locust outbreaks by providing real-time detection tools', 'Supports local households to restore agricultural and good nutrition practices',
    'FAO seeds help Nepali farmers restore their livelihoods and strengthen their resilience', 'Helps costal communities bounce back and build resilient livelihoods',
    'FAO and Mozambique are working to locate, collect and dispose of highly hazardous pesticides', 'FAO reduces unemployment through training in agricultural and entrepreneurial skill',
    'Preparation for UN summit on Climate Change',
    'Guidelines on Social Analysis for Rural area Developing Planning', 'Food Security and Nutrition Policy Assistance Programme Closure',
    'Support to policy seminars on Animal Health Investments', 'Report on Agricultural Reforms and Trade Liberalization',
    'Workshop on Policy impacts on income inquality and poverty',
    'Improving livelihoods of indigenous women in Nicaragua', 'State of the World: Ten reports you need in your library',
    'Improving food and nutrition security in the Philippines', 'Schools – the beginning of the end of malnutrition',
    'Promoting nutrition through schools can create benefits that extend beyond the classroom', 'FAO helps establish early warning systems for food and nutrition security',
    'FAO helps establish early warning systems for food and nutrition security', 'Centroamérica promueve sistemas financieros innovadores para la agricultura familiar',
    'Pulses and Soils – promoting symbiosis through crop rotation', 'The inclusion of pulses in multiple cropping systems is key to maintaining and increasing soil',
    'A dynamic tool for FAO Members to facilitate and support their participation in the work of the Organization.', 'A website with information for our resource partners',
    'Food security and complex emergencies', 'The Canary Current Large Marine Ecosystem project',
    'Linking smallholder potato farmers to the market while caring for the environment', 'FAO helps African universities incorporate trainings in nutrition education',
    'FAO helps build resilience in communities affected by the El Niño in Mindanao', 'FAO supports land rehabilitation in Angola’s indigenous and pastoral communities',
    'FAO provides disaster risk reduction and management training to Laotian farmers', 'FAO enables market access to agricultural inputs for smallholder farmers in Mozambique',
    'FAO helps Lesotho develop a comprehensive land cover database',
    'FAO provides recovery support to Zamboanga fisherfolk through creation of sustainable livelihood opportunities',
    'Income-generating livestock distribution helps conflict-affected families recover after devastating floods',
    'FAO supports sustainable agricultural growth in Burundi through farmer field schools', 'FAO introduces sustainable employment opportunities to assist poor rural households',
    'FAO’s aquaculture project helps improve household nutrition & food security in Northeastern Haiti', 'FAO destocking activities increase financial stability and livelihoods of herder communities',
    'Helps fight desert locust outbreaks by providing real-time detection tools', 'Supports local households to restore agricultural and good nutrition practices',
    'FAO seeds help Nepali farmers restore their livelihoods and strengthen their resilience', 'Helps costal communities bounce back and build resilient livelihoods',
    'FAO and Mozambique are working to locate, collect and dispose of highly hazardous pesticides', 'FAO reduces unemployment through training in agricultural and entrepreneurial skill',
    'Preparation for UN summit on Climate Change',
    'Guidelines on Social Analysis for Rural area Developing Planning', 'Food Security and Nutrition Policy Assistance Programme Closure',
    'Support to policy seminars on Animal Health Investments', 'Report on Agricultural Reforms and Trade Liberalization',
    'Workshop on Policy impacts on income inquality and poverty',
    'Improving livelihoods of indigenous women in Nicaragua', 'State of the World: Ten reports you need in your library',
    'Improving food and nutrition security in the Philippines', 'Schools – the beginning of the end of malnutrition',
    'Promoting nutrition through schools can create benefits that extend beyond the classroom', 'FAO helps establish early warning systems for food and nutrition security',
    'FAO helps establish early warning systems for food and nutrition security', 'Centroamérica promueve sistemas financieros innovadores para la agricultura familiar',
    'Pulses and Soils – promoting symbiosis through crop rotation', 'The inclusion of pulses in multiple cropping systems is key to maintaining and increasing soil',
    'A dynamic tool for FAO Members to facilitate and support their participation in the work of the Organization.', 'A website with information for our resource partners',
    'Food security and complex emergencies', 'The Canary Current Large Marine Ecosystem project',
    'Linking smallholder potato farmers to the market while caring for the environment', 'FAO helps African universities incorporate trainings in nutrition education',
    'FAO helps build resilience in communities affected by the El Niño in Mindanao', 'FAO supports land rehabilitation in Angola’s indigenous and pastoral communities',
    'FAO provides disaster risk reduction and management training to Laotian farmers', 'FAO enables market access to agricultural inputs for smallholder farmers in Mozambique',
    'FAO helps Lesotho develop a comprehensive land cover database',
    'FAO provides recovery support to Zamboanga fisherfolk through creation of sustainable livelihood opportunities',
    'Income-generating livestock distribution helps conflict-affected families recover after devastating floods',
    'FAO supports sustainable agricultural growth in Burundi through farmer field schools', 'FAO introduces sustainable employment opportunities to assist poor rural households',
    'FAO’s aquaculture project helps improve household nutrition & food security in Northeastern Haiti', 'FAO destocking activities increase financial stability and livelihoods of herder communities',
    'Helps fight desert locust outbreaks by providing real-time detection tools', 'Supports local households to restore agricultural and good nutrition practices',
    'FAO seeds help Nepali farmers restore their livelihoods and strengthen their resilience', 'Helps costal communities bounce back and build resilient livelihoods',
    'FAO and Mozambique are working to locate, collect and dispose of highly hazardous pesticides', 'FAO reduces unemployment through training in agricultural and entrepreneurial skill',
    'Preparation for UN summit on Climate Change',
    'Guidelines on Social Analysis for Rural area Developing Planning', 'Food Security and Nutrition Policy Assistance Programme Closure',
    'Support to policy seminars on Animal Health Investments', 'Report on Agricultural Reforms and Trade Liberalization',
    'Workshop on Policy impacts on income inquality and poverty',
    'Preparation for UN summit on Climate Change',
    'Guidelines on Social Analysis for Rural area Developing Planning', 'Food Security and Nutrition Policy Assistance Programme Closure',
    'Support to policy seminars on Animal Health Investments', 'Report on Agricultural Reforms and Trade Liberalization',
    'Workshop on Policy impacts on income inquality and poverty',
    'Improving livelihoods of indigenous women in Nicaragua', 'State of the World: Ten reports you need in your library',
    'Improving food and nutrition security in the Philippines', 'Schools – the beginning of the end of malnutrition',
    'Promoting nutrition through schools can create benefits that extend beyond the classroom', 'FAO helps establish early warning systems for food and nutrition security',
    'FAO helps establish early warning systems for food and nutrition security', 'Centroamérica promueve sistemas financieros innovadores para la agricultura familiar',
    'Pulses and Soils – promoting symbiosis through crop rotation', 'The inclusion of pulses in multiple cropping systems is key to maintaining and increasing soil',
    'A dynamic tool for FAO Members to facilitate and support their participation in the work of the Organization.', 'A website with information for our resource partners',
    'Food security and complex emergencies', 'The Canary Current Large Marine Ecosystem project',
    'Linking smallholder potato farmers to the market while caring for the environment', 'FAO helps African universities incorporate trainings in nutrition education',
    'FAO helps build resilience in communities affected by the El Niño in Mindanao', 'FAO supports land rehabilitation in Angola’s indigenous and pastoral communities',
    'FAO provides disaster risk reduction and management training to Laotian farmers', 'FAO enables market access to agricultural inputs for smallholder farmers in Mozambique',
    'FAO helps Lesotho develop a comprehensive land cover database',
    'FAO provides recovery support to Zamboanga fisherfolk through creation of sustainable livelihood opportunities',
    'Income-generating livestock distribution helps conflict-affected families recover after devastating floods',
    'FAO supports sustainable agricultural growth in Burundi through farmer field schools', 'FAO introduces sustainable employment opportunities to assist poor rural households',
    'FAO’s aquaculture project helps improve household nutrition & food security in Northeastern Haiti', 'FAO destocking activities increase financial stability and livelihoods of herder communities',
    'Helps fight desert locust outbreaks by providing real-time detection tools', 'Supports local households to restore agricultural and good nutrition practices',
    'FAO seeds help Nepali farmers restore their livelihoods and strengthen their resilience', 'Helps costal communities bounce back and build resilient livelihoods',
    'FAO and Mozambique are working to locate, collect and dispose of highly hazardous pesticides', 'FAO reduces unemployment through training in agricultural and entrepreneurial skill',
    'Preparation for UN summit on Climate Change',
    'Guidelines on Social Analysis for Rural area Developing Planning', 'Food Security and Nutrition Policy Assistance Programme Closure',
    'Support to policy seminars on Animal Health Investments', 'Report on Agricultural Reforms and Trade Liberalization',
    'Workshop on Policy impacts on income inquality and poverty',
    'Improving livelihoods of indigenous women in Nicaragua', 'State of the World: Ten reports you need in your library',
    'Improving food and nutrition security in the Philippines', 'Schools – the beginning of the end of malnutrition',
    'Promoting nutrition through schools can create benefits that extend beyond the classroom', 'FAO helps establish early warning systems for food and nutrition security',
    'FAO helps establish early warning systems for food and nutrition security', 'Centroamérica promueve sistemas financieros innovadores para la agricultura familiar',
    'Pulses and Soils – promoting symbiosis through crop rotation', 'The inclusion of pulses in multiple cropping systems is key to maintaining and increasing soil',
    'A dynamic tool for FAO Members to facilitate and support their participation in the work of the Organization.', 'A website with information for our resource partners',
    'Food security and complex emergencies', 'The Canary Current Large Marine Ecosystem project',
    'Linking smallholder potato farmers to the market while caring for the environment', 'FAO helps African universities incorporate trainings in nutrition education',
    'FAO helps build resilience in communities affected by the El Niño in Mindanao', 'FAO supports land rehabilitation in Angola’s indigenous and pastoral communities',
    'FAO provides disaster risk reduction and management training to Laotian farmers', 'FAO enables market access to agricultural inputs for smallholder farmers in Mozambique',
    'FAO helps Lesotho develop a comprehensive land cover database',
    'FAO provides recovery support to Zamboanga fisherfolk through creation of sustainable livelihood opportunities',
    'Income-generating livestock distribution helps conflict-affected families recover after devastating floods',
    'FAO supports sustainable agricultural growth in Burundi through farmer field schools', 'FAO introduces sustainable employment opportunities to assist poor rural households',
    'FAO’s aquaculture project helps improve household nutrition & food security in Northeastern Haiti', 'FAO destocking activities increase financial stability and livelihoods of herder communities',
    'Helps fight desert locust outbreaks by providing real-time detection tools', 'Supports local households to restore agricultural and good nutrition practices',
    'FAO seeds help Nepali farmers restore their livelihoods and strengthen their resilience', 'Helps costal communities bounce back and build resilient livelihoods',
    'FAO and Mozambique are working to locate, collect and dispose of highly hazardous pesticides', 'FAO reduces unemployment through training in agricultural and entrepreneurial skill',
    'Preparation for UN summit on Climate Change',
    'Guidelines on Social Analysis for Rural area Developing Planning', 'Food Security and Nutrition Policy Assistance Programme Closure',
    'Support to policy seminars on Animal Health Investments', 'Report on Agricultural Reforms and Trade Liberalization',
    'Workshop on Policy impacts on income inquality and poverty',
    'Improving livelihoods of indigenous women in Nicaragua', 'State of the World: Ten reports you need in your library',
    'Improving food and nutrition security in the Philippines', 'Schools – the beginning of the end of malnutrition',
    'Promoting nutrition through schools can create benefits that extend beyond the classroom', 'FAO helps establish early warning systems for food and nutrition security',
    'FAO helps establish early warning systems for food and nutrition security', 'Centroamérica promueve sistemas financieros innovadores para la agricultura familiar',
    'Pulses and Soils – promoting symbiosis through crop rotation', 'The inclusion of pulses in multiple cropping systems is key to maintaining and increasing soil',
    'A dynamic tool for FAO Members to facilitate and support their participation in the work of the Organization.', 'A website with information for our resource partners',
    'Food security and complex emergencies', 'The Canary Current Large Marine Ecosystem project',
    'Linking smallholder potato farmers to the market while caring for the environment', 'FAO helps African universities incorporate trainings in nutrition education',
    'FAO helps build resilience in communities affected by the El Niño in Mindanao', 'FAO supports land rehabilitation in Angola’s indigenous and pastoral communities',
    'FAO provides disaster risk reduction and management training to Laotian farmers', 'FAO enables market access to agricultural inputs for smallholder farmers in Mozambique',
    'FAO helps Lesotho develop a comprehensive land cover database',
    'FAO provides recovery support to Zamboanga fisherfolk through creation of sustainable livelihood opportunities',
    'Income-generating livestock distribution helps conflict-affected families recover after devastating floods',
    'FAO supports sustainable agricultural growth in Burundi through farmer field schools', 'FAO introduces sustainable employment opportunities to assist poor rural households',
    'FAO’s aquaculture project helps improve household nutrition & food security in Northeastern Haiti', 'FAO destocking activities increase financial stability and livelihoods of herder communities',
    'Helps fight desert locust outbreaks by providing real-time detection tools', 'Supports local households to restore agricultural and good nutrition practices',
    'FAO seeds help Nepali farmers restore their livelihoods and strengthen their resilience', 'Helps costal communities bounce back and build resilient livelihoods',
    'FAO and Mozambique are working to locate, collect and dispose of highly hazardous pesticides', 'FAO reduces unemployment through training in agricultural and entrepreneurial skill',
    'Preparation for UN summit on Climate Change',
    'Guidelines on Social Analysis for Rural area Developing Planning', 'Food Security and Nutrition Policy Assistance Programme Closure',
    'Support to policy seminars on Animal Health Investments', 'Report on Agricultural Reforms and Trade Liberalization',
    'Workshop on Policy impacts on income inquality and poverty',
    'Improving livelihoods of indigenous women in Nicaragua', 'State of the World: Ten reports you need in your library',
    'Improving food and nutrition security in the Philippines', 'Schools – the beginning of the end of malnutrition',
    'Promoting nutrition through schools can create benefits that extend beyond the classroom', 'FAO helps establish early warning systems for food and nutrition security',
    'FAO helps establish early warning systems for food and nutrition security', 'Centroamérica promueve sistemas financieros innovadores para la agricultura familiar',
    'Pulses and Soils – promoting symbiosis through crop rotation', 'The inclusion of pulses in multiple cropping systems is key to maintaining and increasing soil',
    'A dynamic tool for FAO Members to facilitate and support their participation in the work of the Organization.', 'A website with information for our resource partners',
    'Food security and complex emergencies', 'The Canary Current Large Marine Ecosystem project',
    'Linking smallholder potato farmers to the market while caring for the environment', 'FAO helps African universities incorporate trainings in nutrition education',
    'FAO helps build resilience in communities affected by the El Niño in Mindanao', 'FAO supports land rehabilitation in Angola’s indigenous and pastoral communities',
    'FAO provides disaster risk reduction and management training to Laotian farmers', 'FAO enables market access to agricultural inputs for smallholder farmers in Mozambique',
    'FAO helps Lesotho develop a comprehensive land cover database',
    'FAO provides recovery support to Zamboanga fisherfolk through creation of sustainable livelihood opportunities',
    'Income-generating livestock distribution helps conflict-affected families recover after devastating floods',
    'FAO supports sustainable agricultural growth in Burundi through farmer field schools', 'FAO introduces sustainable employment opportunities to assist poor rural households',
    'FAO’s aquaculture project helps improve household nutrition & food security in Northeastern Haiti', 'FAO destocking activities increase financial stability and livelihoods of herder communities',
    'Helps fight desert locust outbreaks by providing real-time detection tools', 'Supports local households to restore agricultural and good nutrition practices',
    'FAO seeds help Nepali farmers restore their livelihoods and strengthen their resilience', 'Helps costal communities bounce back and build resilient livelihoods',
    'FAO and Mozambique are working to locate, collect and dispose of highly hazardous pesticides', 'FAO reduces unemployment through training in agricultural and entrepreneurial skill',
    'Preparation for UN summit on Climate Change',
    'Guidelines on Social Analysis for Rural area Developing Planning', 'Food Security and Nutrition Policy Assistance Programme Closure',
    'Support to policy seminars on Animal Health Investments', 'Report on Agricultural Reforms and Trade Liberalization',
    'Workshop on Policy impacts on income inquality and poverty',
    'Improving livelihoods of indigenous women in Nicaragua', 'State of the World: Ten reports you need in your library',
    'Improving food and nutrition security in the Philippines', 'Schools – the beginning of the end of malnutrition',
    'Promoting nutrition through schools can create benefits that extend beyond the classroom', 'FAO helps establish early warning systems for food and nutrition security',
    'FAO helps establish early warning systems for food and nutrition security', 'Centroamérica promueve sistemas financieros innovadores para la agricultura familiar',
    'Pulses and Soils – promoting symbiosis through crop rotation', 'The inclusion of pulses in multiple cropping systems is key to maintaining and increasing soil',
    'A dynamic tool for FAO Members to facilitate and support their participation in the work of the Organization.', 'A website with information for our resource partners',
    'Food security and complex emergencies', 'The Canary Current Large Marine Ecosystem project',
    'Linking smallholder potato farmers to the market while caring for the environment', 'FAO helps African universities incorporate trainings in nutrition education',
    'FAO helps build resilience in communities affected by the El Niño in Mindanao', 'FAO supports land rehabilitation in Angola’s indigenous and pastoral communities',
    'FAO provides disaster risk reduction and management training to Laotian farmers', 'FAO enables market access to agricultural inputs for smallholder farmers in Mozambique',
    'FAO helps Lesotho develop a comprehensive land cover database',
    'FAO provides recovery support to Zamboanga fisherfolk through creation of sustainable livelihood opportunities',
    'Income-generating livestock distribution helps conflict-affected families recover after devastating floods',
    'FAO supports sustainable agricultural growth in Burundi through farmer field schools', 'FAO introduces sustainable employment opportunities to assist poor rural households',
    'FAO’s aquaculture project helps improve household nutrition & food security in Northeastern Haiti', 'FAO destocking activities increase financial stability and livelihoods of herder communities',
    'Helps fight desert locust outbreaks by providing real-time detection tools', 'Supports local households to restore agricultural and good nutrition practices',
    'FAO seeds help Nepali farmers restore their livelihoods and strengthen their resilience', 'Helps costal communities bounce back and build resilient livelihoods',
    'FAO and Mozambique are working to locate, collect and dispose of highly hazardous pesticides', 'FAO reduces unemployment through training in agricultural and entrepreneurial skill',
    'Preparation for UN summit on Climate Change',
    'Guidelines on Social Analysis for Rural area Developing Planning', 'Food Security and Nutrition Policy Assistance Programme Closure',
    'Support to policy seminars on Animal Health Investments', 'Report on Agricultural Reforms and Trade Liberalization',
    'Workshop on Policy impacts on income inquality and poverty',
    'Improving livelihoods of indigenous women in Nicaragua', 'State of the World: Ten reports you need in your library',
    'Improving food and nutrition security in the Philippines', 'Schools – the beginning of the end of malnutrition',
    'Promoting nutrition through schools can create benefits that extend beyond the classroom', 'FAO helps establish early warning systems for food and nutrition security',
    'FAO helps establish early warning systems for food and nutrition security', 'Centroamérica promueve sistemas financieros innovadores para la agricultura familiar',
    'Pulses and Soils – promoting symbiosis through crop rotation', 'The inclusion of pulses in multiple cropping systems is key to maintaining and increasing soil',
    'A dynamic tool for FAO Members to facilitate and support their participation in the work of the Organization.', 'A website with information for our resource partners',
    'Food security and complex emergencies', 'The Canary Current Large Marine Ecosystem project',
    'Linking smallholder potato farmers to the market while caring for the environment', 'FAO helps African universities incorporate trainings in nutrition education',
    'FAO helps build resilience in communities affected by the El Niño in Mindanao', 'FAO supports land rehabilitation in Angola’s indigenous and pastoral communities',
    'FAO provides disaster risk reduction and management training to Laotian farmers', 'FAO enables market access to agricultural inputs for smallholder farmers in Mozambique',
    'FAO helps Lesotho develop a comprehensive land cover database',
    'FAO provides recovery support to Zamboanga fisherfolk through creation of sustainable livelihood opportunities',
    'Income-generating livestock distribution helps conflict-affected families recover after devastating floods',
    'FAO supports sustainable agricultural growth in Burundi through farmer field schools', 'FAO introduces sustainable employment opportunities to assist poor rural households',
    'FAO’s aquaculture project helps improve household nutrition & food security in Northeastern Haiti', 'FAO destocking activities increase financial stability and livelihoods of herder communities',
    'Helps fight desert locust outbreaks by providing real-time detection tools', 'Supports local households to restore agricultural and good nutrition practices',
    'FAO seeds help Nepali farmers restore their livelihoods and strengthen their resilience', 'Helps costal communities bounce back and build resilient livelihoods',
    'FAO and Mozambique are working to locate, collect and dispose of highly hazardous pesticides', 'FAO reduces unemployment through training in agricultural and entrepreneurial skill',
    'Preparation for UN summit on Climate Change',
    'Guidelines on Social Analysis for Rural area Developing Planning', 'Food Security and Nutrition Policy Assistance Programme Closure',
    'Support to policy seminars on Animal Health Investments', 'Report on Agricultural Reforms and Trade Liberalization',
    'Workshop on Policy impacts on income inquality and poverty',
    'Improving livelihoods of indigenous women in Nicaragua', 'State of the World: Ten reports you need in your library',
    'Improving food and nutrition security in the Philippines', 'Schools – the beginning of the end of malnutrition',
    'Promoting nutrition through schools can create benefits that extend beyond the classroom', 'FAO helps establish early warning systems for food and nutrition security',
    'FAO helps establish early warning systems for food and nutrition security', 'Centroamérica promueve sistemas financieros innovadores para la agricultura familiar',
    'Pulses and Soils – promoting symbiosis through crop rotation', 'The inclusion of pulses in multiple cropping systems is key to maintaining and increasing soil',
    'A dynamic tool for FAO Members to facilitate and support their participation in the work of the Organization.', 'A website with information for our resource partners',
    'Food security and complex emergencies', 'The Canary Current Large Marine Ecosystem project',
    'Linking smallholder potato farmers to the market while caring for the environment', 'FAO helps African universities incorporate trainings in nutrition education',
    'FAO helps build resilience in communities affected by the El Niño in Mindanao', 'FAO supports land rehabilitation in Angola’s indigenous and pastoral communities',
    'FAO provides disaster risk reduction and management training to Laotian farmers', 'FAO enables market access to agricultural inputs for smallholder farmers in Mozambique',
    'FAO helps Lesotho develop a comprehensive land cover database',
    'FAO provides recovery support to Zamboanga fisherfolk through creation of sustainable livelihood opportunities',
    'Income-generating livestock distribution helps conflict-affected families recover after devastating floods',
    'FAO supports sustainable agricultural growth in Burundi through farmer field schools', 'FAO introduces sustainable employment opportunities to assist poor rural households',
    'FAO’s aquaculture project helps improve household nutrition & food security in Northeastern Haiti', 'FAO destocking activities increase financial stability and livelihoods of herder communities',
    'Helps fight desert locust outbreaks by providing real-time detection tools', 'Supports local households to restore agricultural and good nutrition practices',
    'FAO seeds help Nepali farmers restore their livelihoods and strengthen their resilience', 'Helps costal communities bounce back and build resilient livelihoods',
    'FAO and Mozambique are working to locate, collect and dispose of highly hazardous pesticides', 'FAO reduces unemployment through training in agricultural and entrepreneurial skill',
    'Preparation for UN summit on Climate Change',
    'Guidelines on Social Analysis for Rural area Developing Planning', 'Food Security and Nutrition Policy Assistance Programme Closure',
    'Support to policy seminars on Animal Health Investments', 'Report on Agricultural Reforms and Trade Liberalization',
    'Workshop on Policy impacts on income inquality and poverty',
    'Improving livelihoods of indigenous women in Nicaragua', 'State of the World: Ten reports you need in your library',
    'Improving food and nutrition security in the Philippines', 'Schools – the beginning of the end of malnutrition',
    'Promoting nutrition through schools can create benefits that extend beyond the classroom', 'FAO helps establish early warning systems for food and nutrition security',
    'FAO helps establish early warning systems for food and nutrition security', 'Centroamérica promueve sistemas financieros innovadores para la agricultura familiar',
    'Pulses and Soils – promoting symbiosis through crop rotation', 'The inclusion of pulses in multiple cropping systems is key to maintaining and increasing soil',
    'A dynamic tool for FAO Members to facilitate and support their participation in the work of the Organization.', 'A website with information for our resource partners',
    'Food security and complex emergencies', 'The Canary Current Large Marine Ecosystem project',
    'Linking smallholder potato farmers to the market while caring for the environment', 'FAO helps African universities incorporate trainings in nutrition education',
    'FAO helps build resilience in communities affected by the El Niño in Mindanao', 'FAO supports land rehabilitation in Angola’s indigenous and pastoral communities',
    'FAO provides disaster risk reduction and management training to Laotian farmers', 'FAO enables market access to agricultural inputs for smallholder farmers in Mozambique',
    'FAO helps Lesotho develop a comprehensive land cover database',
    'FAO provides recovery support to Zamboanga fisherfolk through creation of sustainable livelihood opportunities',
    'Income-generating livestock distribution helps conflict-affected families recover after devastating floods',
    'FAO supports sustainable agricultural growth in Burundi through farmer field schools', 'FAO introduces sustainable employment opportunities to assist poor rural households',
    'FAO’s aquaculture project helps improve household nutrition & food security in Northeastern Haiti', 'FAO destocking activities increase financial stability and livelihoods of herder communities',
    'Helps fight desert locust outbreaks by providing real-time detection tools', 'Supports local households to restore agricultural and good nutrition practices',
    'FAO seeds help Nepali farmers restore their livelihoods and strengthen their resilience', 'Helps costal communities bounce back and build resilient livelihoods',
    'FAO and Mozambique are working to locate, collect and dispose of highly hazardous pesticides', 'FAO reduces unemployment through training in agricultural and entrepreneurial skill',
];
const sources = ['STAFF'];

const getDirector = '58a3085f7d994f79a0cbecb1';

var objectID = require('mongodb').ObjectID;

const createDemands = () => demands.map((c) => {
    const category = pickRandomItemInArray(categories);
    // const status = pickRandomItemInArray(statuses);

    return {
        title: c,
        assignedTo: pickRandomItemInArray(names),
        assignedBy: pickRandomItemInArray(names),
        deadline: new Date(randomDate(new Date(2017, 2, 25), new Date(2017, 3, 20))),
        createdBy: "58a3085f7d994f79a0cbecb1",
        status: "ONGOING",
        source: "STAFF",
        category,
        subcategory: category === 'SP' ? pickRandomItemInArray(sp) : pickRandomItemInArray(sdg)
    };
});

const demandsOngoing = createDemands();

// const db =  JSON.stringify({
//     demands: demandsDb
// });

var mongodb = require('mongodb');
var mongoClient = mongodb.MongoClient;
// var url = "mongodb://168.202.4.172:27017/followup"
var url = "mongodb://localhost:27017/test"

demandsOngoing.map((c) => {
    
    mongoClient.connect(url).then(function PopulateData(db) {
//  console.log(c);
    c.assignedBy = new objectID(c.assignedBy);
    c.assignedTo = new objectID(c.assignedTo);
    c.createdBy = new objectID(c.createdBy); //TODO: get from json token
    c.createdDate = new Date();

    return db.collection('commitments').insertOne(c).then(function returnDb(insResult) {return db;});
    
  }).then(function closeDb(db){db.close();});
});

// const demandsComplete = createDemands();

// demandsComplete.map((c) => {
    
//     mongoClient.connect(url).then(function PopulateData(db) {
// //  console.log(c);
//     c.assignedBy = new objectID(c.assignedBy);
//     c.assignedTo = new objectID(c.assignedTo);
//     c.createdBy = new objectID(c.createdBy); //TODO: get from json token
//     c.createdDate = new Date();
//     c.status = "COMPLETED";

//     return db.collection('commitmentsNew').insertOne(c).then(function returnDb(insResult) {return db;});
    
//   }).then(function closeDb(db){db.close();});
// });

console.log('Follow up system database built successfully!');