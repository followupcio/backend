var ErrorHandler = require('../common/error');
var Database = require('../common/database');
var Promise = require('bluebird');
var moment = require('moment');
var ConstantKeys = require('../common/constantKeys');
/**
 * It Retrives all Commitments that
 * are Soon to Expire
 * @return {[type]} [description]
 */
// let getExpireToday = function() {
//     return new Promise(function(resolve, reject) {
//        resolve()
//     });
// }


// module.exports = function getExpireToday() {
//     console.log(" getExpireToday : ");
//     var queryJson = { 'status': ConstantKeys.demand_status.ONGOING };


//     Database().connect()
//         .then(findDocument)


//     function findDocument(db) {

//         return db.collection('commitments').aggregate([

//             { $match: queryJson },
//             { $lookup: { from: "users", localField: "assignedTo", foreignField: "_id", as: "assignedTo" } },
//         ]).toArray().then(function (result) {

//             var assignee = [];
//             if (result) {

//                 var now = new Date();
//                 now.setHours(15, 0, 0, 0);

//                 result.forEach(function (commitment) {
//                     if (new Date(commitment.deadline) < now
//                     ) {
//                         assignee.push(commitment.assignedTo[0])
//                         console.log("Deadline " + commitment.deadline);
//                     }
//                 });

//                 db.close();
//             }
//             console.log("assignee " + JSON.stringify(assignee));
//             return assignee;
//         },
//             function (err) { db.close(); ErrorHandler.handleError(err); console.log("Error " + err) })
//     }

// }

let getExpireToday = function () {
    return new Promise(function (resolve, reject) {
        console.log(" getExpireToday : ");
        var queryJson = { 'status': ConstantKeys.demand_status.ONGOING };
        var assignee = [];
        var now = new Date();
       

        // Connect to the DB
        Database().connect()
            // Wait for connection
            .then(function (db) {
                resolve(db.collection('commitments').aggregate([
                    { $match: queryJson },
                    { $lookup: { from: "users", localField: "assignedTo", foreignField: "_id", as: "assignedTo" } },
                ]).toArray()
                    .then(function (result) {

                        result.forEach(function (commitment) {
                            /*
                            *Looking for commitments that expire today
                            *
                            */
                            if ( !(new Date(commitment.deadline).setHours(0, 0, 0, 0) < now.setHours(0, 0, 0, 0))
                                &&
                                !(new Date(commitment.deadline).setHours(0, 0, 0, 0) > now.setHours(0, 0, 0, 0))
                            ) {
                                commitment.assignedTo[0].cid = commitment._id;
                                assignee.push(commitment.assignedTo[0]);
                                console.log("Expires today :" +  commitment._id);
                            }
                        });
                        db.close();
                        console.log("assignee " + JSON.stringify(assignee));
                        return assignee;
                    })
                );
            }).catch(function (error) {
                reject(error);
            })
    });
}
module.exports = getExpireToday;
