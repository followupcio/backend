var ErrorHandler = require('../common/error');
var Database = require('../common/database');
var Promise = require('bluebird');
var moment = require('moment');
var ConstantKeys = require('../common/constantKeys');
/**
 * It Retrives all Commitments that
 * are Soon to Expire
 * @return {[type]} [description]
 */
let getSoonToExpire = function () {
    // return new Promise(function(resolve, reject) {
    //     var daysUntilSoon = 2;
    //     var currentDate = moment().format('YYYY-MM-DDTHH:mm:ssZ');
    //     var nextDate = moment().add(1, 'day').format('YYYY-MM-DDTHH:mm:ssZ');
    //     // Connect to the DB
    //     Database().connect()
    //         // Wait for connection
    //         .then(function(db) {
    //             resolve(db.collection('commitments').find({
    //                 "deadLine": {
    //                     "$gte": new Date(currentDate),
    //                     "$lt": new Date(nextDate)
    //                 }
    //             }).toArray());
    //         }).catch(function(error) {
    //             reject(error);
    //         })
    // });


    return new Promise(function (resolve, reject) {
        console.log(" getSoonToExpire : ");
        var queryJson = { 'status': ConstantKeys.demand_status.ONGOING };
        var assignee = [];
        var now = new Date();
        now.setHours(17, 0, 0, 0);

        // Connect to the DB
        Database().connect()
            // Wait for connection
            .then(function (db) {
                resolve(db.collection('commitments').aggregate([
                    { $match: queryJson },
                    { $lookup: { from: "users", localField: "assignedTo", foreignField: "_id", as: "assignedTo" } },
                ]).toArray()
                    .then(function (result) {
                        result.forEach(function (commitment) {
                            if (
                                !(new Date(commitment.deadline).setHours(0, 0, 0, 0) < new Date(new Date().getTime() + 1000 * 3600 * 24 * 2).setHours(0, 0, 0, 0))
                                &&
                                !(new Date(commitment.deadline).setHours(0, 0, 0, 0) > new Date(new Date().getTime() + 1000 * 3600 * 24 * 2).setHours(0, 0, 0, 0))
                            ) {
                                commitment.assignedTo[0].deadline = new Date(commitment.deadline).toISOString().substring(0, 10);
                                commitment.assignedTo[0].cid = commitment._id;
                                assignee.push(commitment.assignedTo[0]);
                                console.log("Expire soon : " + commitment._id);
                            }
                        });
                        db.close();
                        console.log("assignee " + JSON.stringify(assignee));
                        return assignee;
                    })
                );
            }).catch(function (error) {
                reject(error);
            })
    });





}
module.exports = getSoonToExpire;