var ErrorHandler = require('../../common/error');
var Database = require('../../common/database');
var BasicValidation = require('../../common/basicValidation');
var findActions = require('../../common/findActions');

module.exports = function listOfComments(req, res, next) {
    var cid = req.params.commitmentid;
    var createdBy = req.params.createdBy;
    var objectID = require('mongodb').ObjectID;

    BasicValidation.userHasPermision()
        .then(function (hasPermission) {
            BasicValidation.documentExists("commitments", cid)
                .then(function (validCommitment) {
                    Database().connect()
                        .then(function (db){
                            return findActions(db,cid,createdBy,res)
                        })
                        .then(returnResult)
                        .catch(ErrorHandler.handleError(res));
                }, ErrorHandler.handleCustomError(res, 412));//commitment doesnt exist
        }, ErrorHandler.handleCustomError(res, 401));//user doesnt have permission

    function returnResult(result) {
        res.send(result);
        next();
    }
};