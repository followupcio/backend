var ErrorHandler = require('../../common/error');
var Database = require('../../common/database');
var FsdAPI = require('../../common/fsd-api');
var BasicValidation = require('../../common/basicValidation');
var _ = require('lodash');
var ConstantKeys = require('../../common/constantKeys');
var findActions = require('../../common/findActions');
var email = require('../../common/email');
var sendMail = require('../../common/sendMail');
var EmailTemplate = require('email-templates').EmailTemplate;
var path = require('path');
let getChildren = require('../getChildren');


module.exports = function updateCommitment(req, res, next) {
    var body = req.body;
    var actionType = req.body.type;
    var cid = req.params.commitmentid;
    var objectID = require('mongodb').ObjectID;
    var commitment = {};
    var statusToGo = {};
    // console.log("Il commitment : " + cid);

    BasicValidation.userHasPermision()
        .then(function (hasPermission) {
            BasicValidation.updateCommitmentValidation(body, cid, true).then(
                function (validData) {
                    Database().connect()
                        .then(prepareDataToUpdate)
                        .then(updateCommitment)
                        .then(createAction)
                        .then(returnResult)
                        .then(checkMDCompleted)
                        .catch(ErrorHandler.handleError(res));
                }, ErrorHandler.handleCustomError(res, 412));//basic data validation fail
        }, ErrorHandler.handleCustomError(res, 401));//user doesnt have permission

    function prepareDataToUpdate(db) {
        // console.log("prepareDataToUpdate : ");

        body.type = _.toUpper(body.type);


        if (body.type === ConstantKeys.actions.REJECT
            || body.type === ConstantKeys.actions.REOPEN)
            statusToGo = ConstantKeys.demand_status.ONGOING;

        if (body.type === ConstantKeys.actions.SUBMIT)
            statusToGo = ConstantKeys.demand_status.SUBMITTED;

        if (body.type === ConstantKeys.actions.APPROVE)
            statusToGo = ConstantKeys.demand_status.COMPLETED;

        if (body.type === ConstantKeys.actions.ARCHIVE)
            statusToGo = ConstantKeys.demand_status.ARCHIEVED;

        if (body.type === ConstantKeys.actions.DELETE)
            statusToGo = ConstantKeys.demand_status.DELETED;


        console.log("StatusToGo : " + statusToGo);
        return db;
    }

    function updateCommitment(db) {

        var commitmentUpdate = {};
        if (statusToGo == ConstantKeys.demand_status.COMPLETED) {
            commitmentUpdate = { "completedDate": new Date() };
        }

        if (body.type != ConstantKeys.actions.COMMENT) {
            //return { db: db, result: null };
            commitmentUpdate.status = statusToGo;
        }

        commitmentUpdate.updatedDate = new Date();
        commitmentUpdate.updatedBy = new objectID(body.createdBy);
        // console.log("commitmentUpdate:", commitmentUpdate);


        return db.collection('commitments').updateOne({ _id: new objectID(cid) },
            {
                $set:
                commitmentUpdate
                // status: statusToGo,
                // updatedDate: new Date(),
                // updatedBy: new objectID(body.createdBy) //TODO: get from json token 
                // // assignedTo: new objectID(body.assignedTo)

            },
            { upsert: false })
            .then(function (updateResult) {
                return { db: db, result: updateResult }
            },
            function (err) {
                console.log("error : " + err);
                params.db.close(); ErrorHandler.handleError(err);
            })
            .catch(ErrorHandler.handleError(res));
    }

    function checkMDCompleted(params){
        let demand = params.demand

        if(checkIfChildren(demand))
        {
            getChildren([String(demand.parent_id)])
                .then((children)=>{

                    let mdCompleted =  children.reduce((acc, child)=>{
                        return acc && child.status.key === 'COMPLETED'

                    }, true)

                    if(mdCompleted){
                        updateParentMD(params.db, demand.parent_id).then(()=>{
                            res.send(demand);
                            next();

                        })
                    }else{
                        res.send(demand);
                        next()
                    }
                })
        }else{
            res.send(demand);
            next();

        }


    }

    function updateParentMD(db, parentID){

        return new Promise((resolve,reject)=>{
            return db.collection('commitments').updateOne({ _id: new objectID(parentID) },
                {
                    $set:
                        {status : "COMPLETED",
                        completedDate: new Date()
                        },

                },
                { upsert: false })
                .then(function (updateResult) {
                       resolve()
                })

        })

    }

    function checkIfChildren(demand)
    {
        if(!demand.parent_id)
        {
            return false
        }
        return true
    }
    function createAction(params) {
        // console.log("Body type : " + _.toUpper(body.type));

        body.type = _.toUpper(body.type);

        var templateDir;
        var commentEmail;

        /*Not create an Action if not changed status*/
        if (body.type != ConstantKeys.actions.COMMENT &&
            (params.result.matchedCount == 0 || params.result.modifiedCount == 0)) {
            params.db.close();
            return params.result;
        }
        return params.db.collection('actions').insertOne({
            commitment: new objectID(cid),
            // assignedTo: new objectID(body.assignedTo), //TODO to define in future
            author: new objectID(body.createdBy), //TODO: get from json token
            timestamp: new Date(),
            type: body.type,
            description: body.description,
            attachment: new objectID(body.attachmentId)
        })
            .then(function (result) {
                // console.log("added action : " + result);
                return { db: params.db, result: result };
            })

            .catch(ErrorHandler.handleError(res));
    }

    function returnResult(params) {
        // console.log("returnResult" + params);

        if (params.result.insertedId) {
            // console.log("insertedId : " + params.result.insertedId + "");

            //Here send the notification of the action 
            sendMail(params.db, cid, body, res);

            return params.db.collection('commitments').aggregate([
                { $match: { _id: new objectID(cid) } },
                { $lookup: { from: "users", localField: "assignedTo", foreignField: "_id", as: "assignedTo" } },
                { $lookup: { from: "users", localField: "assignedBy", foreignField: "_id", as: "assignedBy" } },
                { $lookup: { from: "users", localField: "createdBy", foreignField: "_id", as: "createdBy" } },
                { $lookup: { from: "resources", localField: "category", foreignField: "key", as: "category" } },
                { $lookup: { from: "resources", localField: "subcategory", foreignField: "key", as: "subcategory" } },
                { $lookup: { from: "resources", localField: "status", foreignField: "key", as: "status" } },
                { $lookup: { from: "meetings", localField: "meetingAssociated", foreignField: "_id", as: "meetingAssociated" } },
                { $unwind: { path: "$assignedTo", preserveNullAndEmptyArrays: true } },
                { $unwind: { path: "$createdBy", preserveNullAndEmptyArrays: true } },
                { $unwind: { path: "$assignedBy", preserveNullAndEmptyArrays: true } },
                { $unwind: { path: "$category", preserveNullAndEmptyArrays: true } },
                { $unwind: { path: "$subcategory", preserveNullAndEmptyArrays: true } },
                { $unwind: { path: "$status", preserveNullAndEmptyArrays: true } },
                { $unwind: { path: "$meetingAssociated", preserveNullAndEmptyArrays: true } },
                {
                    $project: {
                        "title": 1,
                        "status": { "key": "$status.key", "label": "$status.label" },
                        "read": 1,
                        "parent": 1,
                        "parent_id" :1,
                        "category": { "key": "$category.key", "label": "$category.label" },
                        "subcategory": { "key": "$subcategory.key", "label": "$subcategory.label" },
                        "meetingAssociated": {
                            "_id": "$meetingAssociated._id",
                            "meetingTitle": "$meetingAssociated.meetingTitle",
                            "authorityName": "$meetingAssociated.authorityName",
                            "authorityRole": "$meetingAssociated.authorityRole",
                            "agenda": "$meetingAssociated.agenda"
                        },
                        "agendaPoint": 1,
                        "createdBy": {
                            "_id": "$createdBy._id", "firstName": "$createdBy.firstName",
                            "lastName": "$createdBy.lastName", "displayName": "$createdBy.displayName", "title": "$createdBy.title",
                            "fullName": "$createdBy.fullName", "email": "$createdBy.email",
                            "phone": "$createdBy.phone", "division": "$createdBy.division",
                            "imageUrl": "$createdBy.imageUrl", "indexNo": "$createdBy.indexNo"
                        },
                        "createdDate": 1,
                        "assignedTo": {
                            "_id": "$assignedTo._id", "firstName": "$assignedTo.firstName",
                            "lastName": "$assignedTo.lastName", "title": "$assignedTo.title",
                            "fullName": "$assignedTo.fullName", "email": "$assignedTo.email",
                            "phone": "$assignedTo.phone", "division": "$assignedTo.division",
                            "imageUrl": "$assignedTo.imageUrl", "indexNo": "$assignedTo.indexNo"
                        },
                        "assignedBy": {
                            "_id": "$assignedBy._id", "firstName": "$assignedBy.firstName",
                            "lastName": "$assignedBy.lastName", "title": "$assignedBy.title",
                            "fullName": "$assignedBy.fullName", "email": "$assignedBy.email",
                            "phone": "$assignedBy.phone", "division": "$assignedBy.division",
                            "imageUrl": "$assignedBy.imageUrl", "indexNo": "$assignedBy.indexNo"
                        },
                        // "timeframe": {
                        //     $cond: [{ $or: [{ $eq: ["$status.key", ConstantKeys.demand_status.ONGOING] }, { $eq: ["$status.key", ConstantKeys.demand_status.SUBMITTED] }] },
                        //     {
                        //         $cond: [{ $gte: ["$deadline", _.get(ConstantKeys.timeframe_Ongoing_status, 'ONTIME')] }, "ONTIME",
                        //         { $cond: [{ $lt: ["$deadline", _.get(ConstantKeys.timeframe_Ongoing_status, "EXPIRED")] }, "EXPIRED", "EXPIRING"] }]
                        //     }, ""]
                        // },
                        "deadline": 1, "updatedBy": 1, "updatedDate": 1,
                        "attachments": 1,
                    }
                }]).toArray().then(function (commitment) {

                    return findActions(params.db, cid, commitment[0].createdBy, res, commitment[0])
                })

                .then(function (params) {
                    params.commitment.actions = params.result;
                   return params.commitment;
                })
                .then(function (commitment) {
                    console.log("add timeframe : " + commitment.deadline);
                    try {

                        var varTimeframe;
                        if (new Date(commitment.deadline) < new Date()) {
                            console.log("EXPIRED");
                            varTimeframe = "EXPIRED";
                            commitment.timeframe = varTimeframe;
                        }
                        else if (new Date(commitment.deadline) < new Date(new Date().getTime() + 1000 * 3600 * 24 * 3)) {
                            console.log("EXPIRING");
                            varTimeframe = "EXPIRING";
                            commitment.timeframe = varTimeframe;
                        }
                        else if (new Date(commitment.deadline) > new Date(new Date().getTime() + 1000 * 3600 * 24 * 3)) {
                            console.log("ONTIME");
                            varTimeframe = "ONTIME";
                           commitment.timeframe = varTimeframe;
                        }
                        console.log("addTimeframe " + varTimeframe);
                      return commitment; 

                    } catch (error) {
                        console.log("error " + error);
                        ErrorHandler.handleError(res);
                    }

                })
                .then(function(commitment){

                     return {
                         demand: commitment,
                         db: params.db
                     }

                });

        }
        if (params.result.matchedCount == 0 || params.result.modifiedCount == 0) {
            // res.statusCode = 304;
            res.send(304, { message: 'commitment Not modified' });
        }
        else {
            res.statusCode = 404;
            res.send(404, { message: 'commitment Not Fount' });
        }
        next();
    }


    function addTimeframe(params) {
        console.log("addTimeframe" + params.result.deadline);
        console.log("NOW : " + new Date());


        try {

            var varTimeframe;
            if (new Date(params.result.deadline) < new Date()) {
                console.log("EXPIRED");
                varTimeframe = "EXPIRED";
                params.result.timeframe = varTimeframe;
            }
            else if (new Date(params.result.deadline) < new Date(new Date().getTime() + 1000 * 3600 * 24 * 3)) {
                console.log("EXPIRING");
                varTimeframe = "EXPIRING";
                params.result.timeframe = varTimeframe;
            }
            else if (new Date(params.result.deadline) > new Date(new Date().getTime() + 1000 * 3600 * 24 * 3)) {
                console.log("ONTIME");
                varTimeframe = "ONTIME";
                params.result.timeframe = varTimeframe;
            }

            console.log("addTimeframe " + varTimeframe);
            return { db: params.db, result: params.result }

        } catch (error) {
            console.log("error " + error);
            ErrorHandler.handleError(res);
        }

    }

}