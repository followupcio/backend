var objectID = require('mongodb').ObjectID;
var ErrorHandler = require('./error');
var Database = require('./database');

module.exports = function findActions(db, cid, createdBy, res, commitment) {
     console.log(" findActions : ");
    var queryJson = { "commitment": new objectID(cid) };
    if (createdBy != null && createdBy != "" && createdBy != undefined) {
        // queryJson['author'] = new objectID(createdBy);
    }

    return db.collection('actions').aggregate([
        
        { $match: queryJson },
        { $lookup: { from: "gridfsupload.files", localField: "attachment", foreignField: "_id", as: "attachment" } },
        { $lookup: { from: "users", localField: "assignedTo", foreignField: "_id", as: "assignedTo" } },
        { $lookup: { from: "users", localField: "author", foreignField: "_id", as: "author" } },
        { $unwind: { path: "$author", preserveNullAndEmptyArrays: true } },
        // { $unwind:  { path: "$createdBy", preserveNullAndEmptyArrays: true } },
        // { $project: {"createdBy.trustedList":0, "createdBy.defaultRoles":0}}
    ]).toArray().then(function (result) {
       // db.close();
        if (commitment)
            return { result: result.reverse(), commitment: commitment, db:db };
        else return result.reverse();
    },
        function (err) { db.close(); ErrorHandler.handleError(err); })
        .catch(ErrorHandler.handleError(res));
}