
module.exports =
    {
        handleError: handleError,
        handleCustomError: handleCustomError
    }
    
function handleError(res) {
    return function (err) {
        // console.error("beforeLoggingError:",err.message);
        res.log.error({err:err});
        // res.statusCode = 500;
        res.send(500, { message: 'Some Server error occured', detail: err.message });
    }
};

function handleCustomError(res, statusCode, cstmMsg) {
    return function (err) {
        console.error("beforeLoggingError1:",err.message);
        res.log.error({err:err},{customMessage:cstmMsg});
        if(cstmMsg!=undefined && cstmMsg!=null){
            res.send(statusCode, { message: cstmMsg });
        } else {
            res.send(statusCode, { message: err.message });
        }
    }
};