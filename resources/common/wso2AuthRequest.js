var Promise = require('bluebird');
var rp = require('request-promise');
var _ = require('lodash');
var config = require('./config');
var xmldoc = require('xmldoc');
var email = require('./email');

module.exports = {
    authUser: authUser
}

function authUser(username, password) {
    var body = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ser=\"http://service.ws.um.carbon.wso2.org\">" +
        "<soapenv:Header/>" +
        "<soapenv:Body>" +
        "<ser:authenticate>" +
        "<ser:userName>" + username + "</ser:userName>" +
        "<ser:credential>" + password + "</ser:credential>" +
        "</ser:authenticate>" +
        "</soapenv:Body>" +
        "</soapenv:Envelope>";

    var options = {
        uri: 'https://accounts.fao.org/services/RemoteUserStoreManagerService.RemoteUserStoreManagerServiceHttpsSoap11Endpoint/',
        method: 'POST',
        body: body,
        headers: {
            'Accept-Encoding': 'gzip,deflate',
            "Content-Type": "text/xml;charset=UTF-8",
            "SOAPAction": "urn:authenticate",
            "Authorization": "Basic " + new Buffer(config.wso2BasicAuthUser + ":" + config.wso2BasicAuthPwd).toString("base64"),
            "Host": "accounts.fao.org",
            "Connection": "Keep-Alive"
        },
        json: false // Automatically parses the JSON string in the response
    };

    return rp(options)
        .then(function (obj) {
            if (process.env.environment == "development" || process.env.environment == "local") {
                console.log("process.env.environment : " + process.env.environment);
                return "SuccEss";
            } else {
                var document = new xmldoc.XmlDocument(obj);

                if (document.valueWithPath("soapenv:Body.ns:authenticateResponse.ns:return") == "true") {
                    return "SuccEss";
                } else {
                    return "Error";
                }
            }
        })
        .catch(
        function (err) {

            var message = {
                text: err,
                from: "DG-Monitoring@fao.org",
                to: "Carlos.Bravo@fao.org, Daniele.Salvatore@fao.org, Giovanni.Abblasio@fao.org ",
                // to:      "someone <someone@your-email.com>, another <another@your-email.com>",
                subject: "SOAP error occured while performing wso2 auth request"
            };

            //This Email in case of error 
            email.sendMail(message);

            console.log("some error occured while performing wso2 auth request");
            return new Error("some error occured while performing wso2 auth request");
        }
        );
};