var _ = require('lodash');
var Promise = require('bluebird');
var Database = require('../common/database');
var ConstantKeys = require('../common/constantKeys');

module.exports =
    {
        createCommitmentValidation: createCommitmentValidation,
        updateCommitmentValidation: updateCommitmentValidation,
        userExists: userExists,
        documentExists: documentExists,
        userHasPermision: userHasPermision
    }

function userHasPermision() {
    return Promise.try(function (user, route, action) {


        return true;
    });
}

function userExists(userId) {
    return Promise.try(function () {
        console.log("userExists" + userId);
        return Database().connect().then(function (db) {
            var objectID = require('mongodb').ObjectID;
            return db.collection('users').findOne({ _id: new objectID(userId) })
                .then(function (result) {
                    db.close();
                    if (result) {
                        return true;
                    } else {
                        throw new Error("User does not exist " +userId );
                    }
                });
        })
    });
}

function documentExists(documentName, documentId) {
    return Promise.try(function () {
        return Database().connect().then(function (db) {
            var objectID = require('mongodb').ObjectID;
            console.log(documentName, " documentId", documentId);
            return db.collection(documentName).findOne({ _id: new objectID(documentId) })
                .then(function (result) {
                    db.close();
                    if (result) {
                        return true;
                    } else {
                        db.close();
                        throw new Error(documentName + " Record does not exist for the provided ID:" + documentId);
                    }
                })
        })
    });
}

function createCommitmentValidation(body) {

    return Promise.try(function () {
        if (body.status != null && body.status != undefined) {
            if (!_.has(ConstantKeys.demand_status, _.toUpper(body.status))) {
                throw new Error("invalid Status Code")
            }
        }
        if (body.category != null && body.category != undefined) {
            if (!_.has(ConstantKeys.category, _.toUpper(body.category))) {
                throw new Error("invalid category");
            }
        }
        if (body.subcategory != null && body.subcategory != undefined) {
            if (!_.has(ConstantKeys.subcategory, _.toUpper(body.subcategory))) {
                throw new Error("invalid subcategory");
            }
        }
        if (body.source != null && body.source != undefined) {
            if (!_.has(ConstantKeys.demand_source, _.toUpper(body.source))) {
                throw new Error("invalid source");
            }
        }
        if (body.assignedBy != null && body.assignedBy != undefined) {
            return Promise.all([userExists(body.assignedTo), userExists(body.assignedBy)]);
        } else {
            return userExists(body.assignedTo);
        }
    });
}

function updateCommitmentValidation(body, originalCommitmentId, StatusChange) {

    return Promise.try(function () {

        if (body.category != null && body.category != undefined) {
            if (!_.has(ConstantKeys.category, _.toUpper(body.category))) {
                throw new Error("invalid category");
            }
        }
        if (body.subcategory != null && body.subcategory != undefined) {
            if (!_.has(ConstantKeys.subcategory, _.toUpper(body.subcategory))) {
                throw new Error("invalid subcategory");
            }
        }
        if (body.status != null && body.status != undefined) {
            if (!_.has(ConstantKeys.demand_status, _.toUpper(body.status))) {
                throw new Error("invalid Status Code");
            }
        }
        if (body.source != null && body.source != undefined) {
            if (!_.has(ConstantKeys.demand_source, _.toUpper(body.source))) {
                throw new Error("invalid source");
            }
        }
        //status can change only of finalApproval=false
        //Only if status=COMPLETE then finalapproval change to true.
        if ((body.status != null && body.status != undefined)
            && (body.finalApproval != null && body.finalApproval != undefined)) {
            if (body.finalApproval && body.status != "COMPLETE") {
                throw new Error("Cannot finalApprove before status is COMPLETE");
            }
        }
    }).then(function () {
        return Database().connect().then(function (db) {
            var objectID = require('mongodb').ObjectID;
            return db.collection('commitments').findOne({ _id: new objectID(originalCommitmentId) })
                .then(function (result) {
                    //Check the status for final approval
                    db.close();
                    if (result) {

                        if (body.type)
                            body.type = _.toUpper(body.type);

                        if (StatusChange && body.type != ConstantKeys.actions.COMMENT) {

                            body.assignedTo = body.createdBy;

                            if (body.type === ConstantKeys.actions.REJECT
                                || body.type === ConstantKeys.actions.REOPEN)
                                var StatusToGo = ConstantKeys.demand_status.ONGOING;

                            if (body.type === ConstantKeys.actions.SUBMIT)
                                var StatusToGo = ConstantKeys.demand_status.SUBMITTED;

                            if (body.type === ConstantKeys.actions.APPROVE)
                                var StatusToGo = ConstantKeys.demand_status.COMPLETED;

                            if (body.type === ConstantKeys.actions.ARCHIVE)
                                var StatusToGo = ConstantKeys.demand_status.ARCHIEVED;

                            if (body.type === ConstantKeys.actions.DELETE)
                                var StatusToGo = ConstantKeys.demand_status.DELETED;

                            if (result.status === ConstantKeys.demand_status.ONGOING) {
                                console.log("STATUS ONGOING" + "StatusToGo : " + StatusToGo);
                                if ((StatusToGo != ConstantKeys.demand_status.SUBMITTED) && (StatusToGo != ConstantKeys.demand_status.DELETED))
                                    throw new Error("Cannot apply this status change to an on going commitment");
                            }
                            if (result.status === ConstantKeys.demand_status.SUBMITTED) {
                                if ((StatusToGo === ConstantKeys.demand_status.ARCHIEVED))
                                    throw new Error("Cannot apply this status change to a submitted commitment");
                            }
                            if (result.status === ConstantKeys.demand_status.COMPLETED) {
                                if ((StatusToGo != ConstantKeys.demand_status.ARCHIEVED) && (StatusToGo != ConstantKeys.demand_status.DELETED))
                                    throw new Error("Cannot apply this status change to a completed commitment");
                            }

                        }
                        if (body.status != null && body.status != undefined) {
                            if (result.finalApproval != null && result.finalApproval == true) {
                                throw new Error("Final Approved commitment cannot change status");
                            }
                        } else if (body.finalApproval != null && body.finalApproval != undefined) {
                            if (result.status != "COMPLETE") {
                                throw new Error("Cannot finalApprove before completing");
                            }
                        }
                    } else {
                        throw new Error("Commitment does not exist");
                    }
                });
        });
    }).thenReturn(userExists(body.assignedTo));
}