var ErrorHandler = require('./error');
var Database = require('./database');
var _ = require('lodash');
var ConstantKeys = require('./constantKeys');
var findActions = require('./findActions');
var email = require('./email');
var EmailTemplate = require('email-templates').EmailTemplate;
var path = require('path');
module.exports = function sendEmail(db, cid, body, res) {
    var receiver;
    var queryJson = {};
    var deepLink;
    var objectID = require('mongodb').ObjectID;

    if (process.env.environment == "development") deepLink = "https://hqldvodg1.hq.un.fao.org/dev/demands/" + cid;
    else deepLink = "https://dgms.fao.org/demands/" + cid;
    return db.collection('commitments').aggregate([{
        $match: {
            _id: new objectID(cid)
        }
    }, {
        $lookup: {
            from: "users",
            localField: "assignedTo",
            foreignField: "_id",
            as: "assignedTo"
        }
    }, {
        $lookup: {
            from: "users",
            localField: "assignedBy",
            foreignField: "_id",
            as: "assignedBy"
        }
    }, {
        $unwind: {
            path: "$assignedTo",
            preserveNullAndEmptyArrays: true
        }
    }, {
        $unwind: {
            path: "$assignedBy",
            preserveNullAndEmptyArrays: true
        }
    }, {
        $unwind: {
            path: "$category",
            preserveNullAndEmptyArrays: true
        }
    }, {
        $unwind: {
            path: "$status",
            preserveNullAndEmptyArrays: true
        }
    }, {
        $project: {
            "title": 1,
            "category": 1,
            "status": {
                "key": "$status.key",
                "label": "$status.label"
            },
            "read": 1,
            "assignedTo": {
                "_id": "$assignedTo._id",
                "firstName": "$assignedTo.firstName",
                "lastName": "$assignedTo.lastName",
                "title": "$assignedTo.title",
                "fullName": "$assignedTo.fullName",
                "email": "$assignedTo.email",
                "phone": "$assignedTo.phone",
                "division": "$assignedTo.division",
                "imageUrl": "$assignedTo.imageUrl",
                "indexNo": "$assignedTo.indexNo"
            },
            "assignedBy": {
                "_id": "$assignedBy._id",
                "firstName": "$assignedBy.firstName",
                "lastName": "$assignedBy.lastName",
                "title": "$assignedBy.title",
                "fullName": "$assignedBy.fullName",
                "email": "$assignedBy.email",
                "phone": "$assignedBy.phone",
                "division": "$assignedBy.division",
                "imageUrl": "$assignedBy.imageUrl",
                "indexNo": "$assignedBy.indexNo"
            },
        }
    }]).toArray().then(function (commitments) {
        return findActions(db, cid, commitments[0].createdBy, res, commitments[0])
    }).then(function (params) {
        params.commitment.actions = params.result;
        return params.commitment;
    }).then(function (commitment) {
        // console.log( JSON.stringify(commitment));

        var message;
        //var commitment = commitments[0];
        if (body.title && body.status === ConstantKeys.demand_status.ONGOING) {
            /*
            CREATION OF A DEMAND 
            */
            var model = {
                deepLink: deepLink
            };
            receiver = commitment.assignedTo.email;
            templateDir = path.join(__dirname, '../../templates', 'creation');
            commentEmail = new EmailTemplate(templateDir);
            commentEmail = new EmailTemplate(templateDir);

            commentEmail.render(model, function (err, result) {

                message = {
                    from: "DG-Monitoring@fao.org",
                    to: receiver,
                    bcc: "DG-Monitoring@fao.org" + ", Werner.Deutsch@fao.org",
                    subject: "DGMS : New task  - [" + commitment.assignedTo.title + " " + commitment.assignedTo.firstName + " " + commitment.assignedTo.lastName + " - " + commitment.category + "]",
                    attachment: [{
                        data: result.html,
                        alternative: true
                    }]
                };
                email.sendMail(message);
            })
        }
        if (body.type === ConstantKeys.actions.REJECT) {
            var model = {
                deepLink: deepLink
            };
            templateDir = path.join(__dirname, '../../templates', 'reject');
            receiver = commitment.assignedTo.email;
            commentEmail = new EmailTemplate(templateDir);

            commentEmail.render(model, function (err, result) {

                message = {
                    from: "DG-Monitoring@fao.org",
                    to: receiver,
                    bcc: "DG-Monitoring@fao.org" + ", Werner.Deutsch@fao.org",
                    subject: "DGMS : Task reject  - [" + commitment.assignedTo.title + " " + commitment.assignedTo.firstName + " " + commitment.assignedTo.lastName + " - " + commitment.category + "]",
                    attachment: [{
                        data: result.html,
                        alternative: true
                    }]
                };
                email.sendMail(message);
            })
        }
        if (body.type === ConstantKeys.actions.REOPEN) {
            /*REOPEN A DEMAND*/
            var model = {
                assignee: commitment.assignedTo.title + " " + commitment.assignedTo.firstName + " " + commitment.assignedTo.lastName,
                instructor: commitment.assignedBy.title + " " + commitment.assignedBy.firstName + " " + commitment.assignedBy.lastName,
                deepLink: deepLink
            }
            templateDir = path.join(__dirname, '../../templates', 'reopen');
            receiver = commitment.assignedBy.email;
            commentEmail = new EmailTemplate(templateDir);
            commentEmail.render(model, function (err, result) {
                if (err) console.log("ERROR :" + err);
                else console.log("result html" + result.html + " receiver " + receiver);
                message = {
                    from: "DG-Monitoring@fao.org",
                    to: receiver,
                    bcc: "DG-Monitoring@fao.org" + ", Werner.Deutsch@fao.org",
                    subject: "DGMS : Assignment submitted for approval - [" + commitment.assignedTo.title + " " + commitment.assignedTo.firstName + " " + commitment.assignedTo.lastName + " - " + commitment.category + "]",
                    attachment: [{
                        data: result.html,
                        alternative: true
                    }]
                };
                email.sendMail(message);
            })
        }
        if (body.type === ConstantKeys.actions.SUBMIT) {
            /* SUBMIT OF A DEMAND */
            var model = {
                assignee: commitment.assignedTo.title + " " + commitment.assignedTo.firstName + " " + commitment.assignedTo.lastName,
                instructor: commitment.assignedBy.title + " " + commitment.assignedBy.firstName + " " + commitment.assignedBy.lastName,
                deepLink: deepLink
            }
            templateDir = path.join(__dirname, '../../templates', 'submit');
            receiver = commitment.assignedBy.email;
            commentEmail = new EmailTemplate(templateDir);

            commentEmail.render(model, function (err, result) {

                message = {
                    from: "DG-Monitoring@fao.org",
                    to: receiver,
                    bcc: "DG-Monitoring@fao.org" + ", Werner.Deutsch@fao.org",
                    subject: "DGMS : Assignment submitted for approval - [" + commitment.assignedTo.title + " " + commitment.assignedTo.firstName + " " + commitment.assignedTo.lastName + " - " + commitment.category + "]",
                    attachment: [{
                        data: result.html,
                        alternative: true
                    }]
                };
                email.sendMail(message);
            })
        }
        if (body.type === ConstantKeys.actions.APPROVE) {
            /* APPROVE OF A DEMAND */
            var model = {
                assignee: commitment.assignedTo.title + " " + commitment.assignedTo.firstName + " " + commitment.assignedTo.lastName,
                instructor: commitment.assignedBy.title + " " + commitment.assignedBy.firstName + " " + commitment.assignedBy.lastName,
                deepLink: deepLink
            }
            templateDir = path.join(__dirname, '../../templates', 'approve');
            receiver = commitment.assignedTo.email;
            commentEmail = new EmailTemplate(templateDir);

            commentEmail.render(model, function (err, result) {

                message = {
                    from: "DG-Monitoring@fao.org",
                    to: receiver,
                    bcc: "DG-Monitoring@fao.org" + ", Werner.Deutsch@fao.org",
                    subject: "DGMS : Task approved  - [" + commitment.assignedTo.firstName + " " + commitment.assignedTo.lastName + " - " + commitment.category + "]",
                    attachment: [{
                        data: result.html,
                        alternative: true
                    }]
                };
                email.sendMail(message);
            })
        }
        if (body.type === ConstantKeys.actions.ARCHIVE) {
            templateDir = path.join(__dirname, 'templates', 'comment');
            commentEmail = new EmailTemplate(templateDir);
        }
        if (body.type === ConstantKeys.actions.COMMENT) {
            /*
               COMMENT OF A DEMAND
                
            */
            var subjectB;
            if (commitment.assignedBy._id == body.createdBy) {
                var model = {
                    assignee: commitment.assignedBy.title + " " + commitment.assignedBy.firstName + " " + commitment.assignedBy.lastName,
                    instructor: commitment.assignedTo.title + " " + commitment.assignedTo.firstName + " " + commitment.assignedTo.lastName,
                    deepLink: deepLink
                }
                templateDir = path.join(__dirname, '../../templates', 'commentByDirector');
                receiver = commitment.assignedTo.email;
                subjectB = "DGMS : Task assigned to you has been commented on - [" + commitment.assignedTo.title + " " + commitment.assignedTo.firstName + " " + commitment.assignedTo.lastName + " - " + commitment.category + "]";
            }
            if (commitment.assignedTo._id == body.createdBy) {
                var model = {
                    assignee: commitment.assignedTo.title + " " + commitment.assignedTo.firstName + " " + commitment.assignedTo.lastName,
                    instructor: commitment.assignedBy.title + " " + commitment.assignedBy.firstName + " " + commitment.assignedBy.lastName,
                    deepLink: deepLink
                }
                templateDir = path.join(__dirname, '../../templates', 'comment');
                receiver = commitment.assignedBy.email;
                subjectB = "DGMS : Task assigned by you has been commented on  - [" + commitment.assignedTo.title + " " + commitment.assignedTo.firstName + " " + commitment.assignedTo.lastName + " - " + commitment.category + "]";
            } else {
                // In the case the comment is by a third person (Follower)
                // Send email to the creator
                var action = commitment.actions[0];

                var model = {
                    assignee: action.author.title + " " + action.author.firstName + " " + action.author.lastName,
                    instructor: commitment.assignedBy.title + " " + commitment.assignedBy.firstName + " " + commitment.assignedBy.lastName,
                    deepLink: deepLink
                }
                templateDir = path.join(__dirname, '../../templates', 'comment');
                receiver = commitment.assignedBy.email;
                subjectB = "DGMS : Task assigned by you has been commented on  - [" + commitment.assignedTo.title + " " + commitment.assignedTo.firstName + " " + commitment.assignedTo.lastName + " - " + commitment.category + "]";
                commentEmail = new EmailTemplate(templateDir);

                commentEmail.render(model, function (err, result) {
                    //console.log(subjectB);
                    message = {
                        from: "DG-Monitoring@fao.org",
                        to: receiver,
                        bcc: "DG-Monitoring@fao.org" + ", Werner.Deutsch@fao.org",
                        subject: JSON.stringify(subjectB),
                        attachment: [{
                            data: result.html,
                            alternative: true
                        }]
                    };
                    email.sendMail(message);
                });
                //Send email to the assignee
                var model = {
                    assignee: action.author.title + " " + action.author.firstName + " " + action.author.lastName,
                    instructor: commitment.assignedTo.title + " " + commitment.assignedTo.firstName + " " + commitment.assignedTo.lastName,
                    deepLink: deepLink
                }
                templateDir = path.join(__dirname, '../../templates', 'commentByFollower');
                receiver = commitment.assignedTo.email;
                subjectB = "DGMS : Task assigned to you has been commented on - [" + commitment.assignedTo.title + " " + commitment.assignedTo.firstName + " " + commitment.assignedTo.lastName + " - " + commitment.category + "]";
                commentEmail = new EmailTemplate(templateDir);

                commentEmail.render(model, function (err, result) {

                    message = {
                        from: "DG-Monitoring@fao.org",
                        to: receiver,
                        bcc: "DG-Monitoring@fao.org" + ", Werner.Deutsch@fao.org",
                        subject: JSON.stringify(subjectB),
                        attachment: [{
                            data: result.html,
                            alternative: true
                        }]
                    };
                    email.sendMail(message);
                });
                sent = true;
            }
            // commentEmail = new EmailTemplate(templateDir);
            // if (!sent) commentEmail.render(model, function(err, result) {
            //     if (err) console.log("ERROR :" + err);
            //     else console.log("result html" + result.html + " receiver " + receiver);
            //     console.log(subjectB);
            //     message = {
            //         from: "DG-Monitoring@fao.org",
            //         to: receiver,
            //         bcc: "DG-Monitoring@fao.org" + ", Werner.Deutsch@fao.org",
            //         subject: JSON.stringify(subjectB),
            //         attachment: [{
            //             data: result.html,
            //             alternative: true
            //         }]
            //     };
            //     email.sendMail(message);
            // })
        }
    }).catch(ErrorHandler.handleError(res));
}