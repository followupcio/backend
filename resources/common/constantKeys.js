
	//timeframe for demand with status ongoing
	var timeframe_Ongoing = {
		ALL: { $gte: new Date(0000 - 00 - 00) },
		ONTIME: { $gte: new Date(new Date().getTime() + 1000 * 3600 * 24 * 3) },
		EXPIRING: { $lt: new Date(new Date().getTime() + 1000 * 3600 * 24 * 3), $gte: new Date() },
		EXPIRED: { $lt: new Date() }
	};

	var timeframe_Ongoing_status = {
		ONTIME: new Date(new Date().getTime() + 1000 * 3600 * 24 * 3) ,
		EXPIRED: new Date(new Date().getTime() - 1000 * 3600 * 24 * 1) 
	};

	//timeframe for demand with status complete
	var timeframe_Complete = {
		ALL: { $gte: new Date(0000 - 00 - 00) },
		RECENT: { $lt: new Date(new Date().getTime() + 1000 * 3600 * 24 * 15) }
	};

	//allowed status of demand
	var demand_status = {
		ONGOING: "ONGOING",
		COMPLETED: "COMPLETED",
		ARCHIEVED: "ARCHIEVED",
		SUBMITTED: "SUBMITTED",
		DELETED: "DELETED",
		CREATED: "CREATED"
	};

	//allowed actions
	var actions = {
		SUBMIT: "SUBMIT",
		REJECT: "REJECT",
		REOPEN: "REOPEN",
		APPROVE: "APPROVE",
		ARCHIVE: "ARCHIVE",
		COMMENT: "COMMENT",
		CREATION: "CREATION",
		DELETE: "DELETE",
		UPDATE: "UPDATE",
		READ: "READ"
	};
	
	//allowed source of demand
	var demand_source = {
		STAFF: "STAFF",
		MEETING: "MEETING",
		COUNCILMEETING: "COUNCILMEETING",
		TRAVEL: "TRAVEL",
		HIGHLEVEL: "HIGHLEVEL",
		OTHER: "OTHER"
	};

    //allowed category key for demand
    var category = {
		NOCATEGORY: "NOCATEGORY",
        SDG: "SDG",
        SP: "SP",
		APEX: "APEX",
		DDO: "DDO",
		DDN: "DDN",
		DDP: "DDP",
		ES: "ES",
		SP1: "SP1",
		SP2: "SP2",
		SP3: "SP3",
		SP4: "SP4",
		SP5: "SP5"

    };

    //allowed sub-category key for demand
    var subcategory = {
        SDG1: "SDG1", SDG2: "SDG2", SDG3: "SDG3", SDG4: "SDG4", SDG5: "SDG5",
        SDG6: "SDG6", SDG7: "SDG7", SDG8: "SDG8", SDG9: "SDG9", SDG10: "SDG10",
        SDG11: "SDG11", SDG12: "SDG12", SDG13: "SDG13", SDG14: "SDG14", SDG15: "SDG15",
        SDG16: "SDG16", SDG17: "SDG17",
        SP1: "SP1", SP2: "SP2", SP3: "SP3", SP4: "SP4", SP5: "SP5",
		OED:"OED", OIG:"OIG", LEG:"LEG", OSP:"OSP", OCC:"OCC", OHR:"OHR", CAB:"CAB",
		DDOD:"DDOD", DDOS: "DDOS", CSD: "CSD", CSF: "CSD", CSA: "CSA", CSS: "CSS", CPA: "CPA", CIO: "CIO", OSD :"OSD", ROs: "ROs",
		DDND:"DDND", FI: "FI", FO: "FO", CB: "CB", AG: "AG",
		DDPD:"DDPD", OCS:"OCS", OPC: "OPC", TC: "TC", LOs: "LOs",
		ESD: "ESD", ESA: "ESA", ESP :"ESP", EST :"EST", ESS :"ESS", ESN :"ESN",
		SP1D:"SP1D", SP2D:"SP2D", SP3D:"SP3D", SP4D:"SP4D", SP5D:"SP5D"
		


    };

	module.exports =
    {
        timeframe_Ongoing: timeframe_Ongoing,
        timeframe_Complete: timeframe_Complete,
        demand_status: demand_status,
        demand_source, demand_source,
		actions: actions,
        category: category,
        subcategory: subcategory,
		timeframe_Ongoing_status: timeframe_Ongoing_status
    }