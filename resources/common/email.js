var email = require('emailjs');

module.exports = {
    sendMail: sendMail
}

function sendMail(message) {
    var server = email.server.connect({
        /*user:"DG-Monitoring@fao.org",
        password:"tRludietI1T0",   
        host: "smtp.office365.com",
        port: process.env.MAIL_PORT,
        tls: { ciphers: "SSLv3" }*/
       user: process.env.MAIL_USERNAME,
        password: process.env.MAIL_PASSWORD,
        host: process.env.MAIL_HOST,
        port: process.env.MAIL_PORT,
        ssl:     process.env.MAIL_ENCRYPTION
    });

    // send the message and get a callback with an error or details of the message that was sent
    server.send(message, function (err, message) { console.log(err || message); });
}

