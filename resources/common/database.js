var config = require('config');
var mongodb = require('mongodb');

module.exports = function () {
    var mongoClient = mongodb.MongoClient;
    var dbConfig;
    if (process.env.environment == "development")
        dbConfig = config.get('configuration.dbConfigDev');
    else if (process.env.environment == "demo")
        dbConfig = config.get('configuration.dbConfig');
    else
        dbConfig = config.get('configuration.dbConfigLocal');

    var url = 
        'mongodb://' + process.env.DB_USERNAME +':' + process.env.DB_PASSWORD +'@' +process.env.DB_HOST+'/followup' ;

    return {
        connect: function () {
            return mongoClient.connect(url);
        }
    }
}