var ErrorHandler = require('../common/error');
var Database = require('../common/database');
var BasicValidation = require('../common/basicValidation');
var _ = require('lodash');

module.exports = function listResoucesByType(req, res, next) {

	var type = req.params.type;
	var locale = req.params.locale;
	var parentId = req.params.parentId;

	BasicValidation.userHasPermision().then(
		function (hasPermission) {
			Database().connect()
				.then(findDocument)
				.then(returnResult)
				.catch(ErrorHandler.handleError(res));
		}, ErrorHandler.handleCustomError(res, 401)
	);

	function findDocument(db) {
			return db.collection('resources').find({
				type: { $in: [_.upperCase(type)] },
				parentId: parentId,
				locale: locale
			}, { key: 1, label: 1, _id: 0 }).toArray().then(function (result) { return result; },
			 function(err){db.close(); ErrorHandler.handleError(err);})
			.catch(ErrorHandler.handleError(res));
	}
	
	function returnResult(result) {
		res.send(result);
		next();
	}
}