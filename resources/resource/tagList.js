var ErrorHandler = require('../common/error');
var Database = require('../common/database');
var BasicValidation = require('../common/basicValidation');

module.exports = function listCommitments(req, res, next) {
    var t = req.params.tag;
    if (t == undefined || t == null || t == "") { 
        t = "";
    } else { t += "*" }

    BasicValidation.userHasPermision().then(
        function (hasPermission) {
            Database().connect()
                .then(findDocument)
                .then(returnResult)
                .catch(ErrorHandler.handleError(res));
        }, ErrorHandler.handleCustomError(res, 401)
    );

    function findDocument(db) {
            return db.collection('commitments').distinct("tag")
                .then(function (result) {
                    db.close();
                    result.sort();
                    // console.log(result);
                    var finRes = [];
                    for (i = 0; i < result.length; i++) {
                        if (result[i] != null && result[i].match(RegExp(t, 'i'))) {
                            finRes.push(result[i]);
                        }
                    }
                    return finRes;
                }, function(err){db.close(); ErrorHandler.handleError(err);})
			.catch(ErrorHandler.handleError(res));

    }

    function returnResult(result) {
        res.send(result);
        next();
    }
}