var ErrorHandler = require('../common/error');
var Database = require('../common/database');
var BasicValidation = require('../common/basicValidation');
var Promise = require('bluebird');
var fs = Promise.promisifyAll(require("fs"));
var _ = require('lodash');

module.exports = function listResoucesByType(req, res, next) {

	var type = req.params.type;
	var locale = req.params.locale;
	var label = req.params.label;
	if (label == null) label = "";

	BasicValidation.userHasPermision().then(
		function (hasPermission) {
			Database().connect()
				.then(insertResourceIfEmpty)
				.then(findResource)
				.then(returnResult)
				.catch(ErrorHandler.handleError(res));
		}, ErrorHandler.handleCustomError(res, 401)
	);

	function insertResourceIfEmpty(db) {
		return db.collection('resources').findOne({})
			.then(function (result) {
				if (result) {
					return true;
				} else {
					return readFiles("./././dataResource/data_place.json",
						"./././dataResource/data_place1.json",
						"./././dataResource/data_place2.json",
						"./././dataResource/data_category.json",
						"./././dataResource/data_status.json",
						"./././dataResource/data_subcategory.json",
						"./././dataResource/data_timeframe.json")
						.then(function (files) {
							return Promise.all([
								insertResources(db, files[0]),
								insertResources(db, files[1]),
								insertResources(db, files[2]),
								insertResources(db, files[3]),
								insertResources(db, files[4]),
								insertResources(db, files[5]),
								insertResources(db, files[6])]);
						}).then({ function() { return true; } });
				}

			}).then(function () { return db; });
	}

	function readFiles(file1, file2, file3, file4, file5, file6, file7) {
		return Promise.all([fs.readFileAsync(file1),
		fs.readFileAsync(file2),
		fs.readFileAsync(file3),
		fs.readFileAsync(file4),
		fs.readFileAsync(file5),
		fs.readFileAsync(file6),
		fs.readFileAsync(file7)]);
	}

	function insertResources(db, data) {
		return db.collection('resources').insertMany(JSON.parse(data)).then(function (result) { return true; });;
	}

	function findResource(db) {
		return db.collection('resources').find({
			type: { $in: [_.upperCase(type)] },
			label: { $regex: label, $options: "i" },
			locale: locale
		}, { key: 1, label: 1, _id: 0 })
			.toArray()
			.then(function (result) { db.close(); return result; },
			function (err) { db.close(); ErrorHandler.handleError(err); })
			.catch(ErrorHandler.handleError(res));

	}

	function returnResult(result) {
		res.send(result);
		next();
	}
}