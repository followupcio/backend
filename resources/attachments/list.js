var ErrorHandler = require('../common/error');
var Database = require('../common/database');
var GridFSBucket = require('mongodb').GridFSBucket;
var BasicValidation = require('../common/basicValidation');
var ErrorHandler = require('../common/error');

module.exports = function list(req, res, next) {
    var cid = req.params.commitmentid;
    var objectID = require('mongodb').ObjectID;

    BasicValidation.userHasPermision()
        .then(function (hasPermission) {
            BasicValidation.documentExists("commitments", cid)
                .then(function (validCommitment) {
                    console.log("commitmentid is valid");
                    Database().connect()
                        .then(findAttachments)
                        .then(parseAttachments)
                        .then(returnResult)
                        .catch(ErrorHandler.handleError(res));
                }, ErrorHandler.handleCustomError(res, 412));//commitment doesnt exist
        }, ErrorHandler.handleCustomError(res, 401));//user doesnt have permission


    function findAttachments(db) {
        console.log("CID : " + cid);
        return db
            .collection('commitments')
            .findOne({ _id: new objectID(cid) })
            .then(function (commitment) {
                if (!commitment) {
                    console.log("Commitment not found");
                    res.send(404, "Commitment does not exist");
                    next();
                }
                else
                    return { db: db, commitment: commitment }
            });
    }

    function parseAttachments(params) {
        console.log(params.commitment);
        var files = params.commitment.attachments;
        var bucket = new GridFSBucket(params.db, { bucketName: 'gridfsupload' });
        var IdOfFiles = [];

        for (var index in files) {
            IdOfFiles.push(new objectID(files[index]))
        }
        
        try {
            return bucket
                .find({
                    _id: { $in: IdOfFiles }
                }).toArray()
        } catch (error) {
            throw (error);
        }
        next();
    }

    function returnResult(result) {
        res.send(result);
        next();
    }

};