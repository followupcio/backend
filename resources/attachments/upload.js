var mongodb = require('mongodb');
var GridFSBucket = require('mongodb').GridFSBucket;
var busboy = require('busboy');
var mongoClient = require('mongodb').MongoClient;
var objectID = mongodb.ObjectID;
var config = require('config');
var BasicValidation = require('../common/basicValidation');
var ErrorHandler = require('../common/error');
var Database = require('../common/database');

module.exports = function upload(req, res, next) {

    var cid = req.params.commitmentid;
    var closure = req.query.closure;
    var objectID = require('mongodb').ObjectID;
    var closing ; 

    if(closure)
        closing = {"closure" : "true"};
    else     
        closing = {"closure" : "false"};

    BasicValidation.userHasPermision()
        .then(function (hasPermission) {
            BasicValidation.documentExists("commitments", cid)
                .then(function (validCommitment) {
                    console.log("commitmentid is valid");

                    Database().connect()
                        .then(findCommitment)
                        .then(uploadCallBack)
                        .catch(ErrorHandler.handleError(res));


                }, ErrorHandler.handleCustomError(res, 412));//commitment doesnt exist
        }, ErrorHandler.handleCustomError(res, 401));//user doesnt have permission


    function findCommitment(db) {

        var objectID = require('mongodb').ObjectID;
        return db.collection("commitments").findOne({ _id: new objectID(cid) })
            .then(function (result) {
                if (result) {
                    return { db: db, result: result };
                } else {
                    throw new Error(" Commitment does not exist for the provided ID:" + documentId);
                }
            })
    }


    function uploadCallBack(params) {

        var bucket = new GridFSBucket(params.db, { bucketName: 'gridfsupload' });
        var fileStream = new busboy({ headers: req.headers });
        req.pipe(fileStream);

        fileStream.on('file', function (fieldname, readStream, filename, encoding, contentType) {
            console.log('uploading:', filename);
            var uploadStream = bucket.openUploadStream(filename, {
                contentType: contentType,
                metadata: closing
            });

            uploadStream.on('error', function (err) {
                console.log('upload error:', err);
                res.send(500, err);
                next(err);
            });

            uploadStream.once('finish', function () {
                console.log('upload complete:', uploadStream.id);
                updateDocument(params.db, uploadStream.id, params.result, function () {   
                    console.log("Returning the result......")                
                     res.send(201,{ message: 'attachment added', 'attachmentId': uploadStream.id });                   
                });
            });

            readStream.pipe(uploadStream);
        });
    }

    var updateDocument = function (db, fid, item, callback) {
        if (item.attachments) {
            console.log("attachments: " + item.attachments);

            db.collection('commitments').updateOne({ _id: new mongodb.ObjectID(cid) }, { $push: { attachments: fid.toString() } }, { upsert: true }, function (err, result) {
                if (err) {
                    console.log(err);
                    db.close();
                    throw new Error(" Error updating commitment :" + item._id);
                } else {
                    console.log("Updated a document into the commitments collection.");
                    db.close();
                    callback();
                }
            });
        }
        else {
            var loadAttachment = [fid.toString()];
            db.collection('commitments').updateOne({ _id: new mongodb.ObjectID(cid) }, { $set: { attachments: loadAttachment } }, { upsert: true }, function (err, result) {

                if (err) {
                    console.log(err);
                    db.close();
                    throw new Error(" Error updating commitment :" + item._id);
                } else {
                    console.log("Updated a document into the commitments collection.");
                    db.close();
                    callback();
                }
            });
        };
    }


}
























//   };

//   function uploadCallBack(db){

//       var bucket = new GridFSBucket(db, { bucketName: 'gridfsupload' });
//       var fileStream = new busboy({ headers: req.headers }); 
//       req.pipe(fileStream);

//       fileStream.on('file', function (fieldname, readStream, filename, encoding, contentType) {
//         console.log('uploading:', filename);
//         var uploadStream = bucket.openUploadStream(filename, {
//           contentType: contentType
//         });

//         uploadStream.on('error', function (err) {
//           console.log('upload error:', err);
//           res.send(500, err);
//           next(err);
//         });

//         uploadStream.once('finish', function () {
//           console.log('upload complete:', uploadStream.id);

//              findCommitments(db, function (item){

//               if(item){
//               updateDocument(db,uploadStream.id,item,function() {
//                 res.send(201, uploadStream.id);
//                 next();
//               });

//               } else{
//                 res.send(404, "Commitment does not exist");
//                 db.close();
//                 next();
//               }           
// 	        	});
//         });

//         readStream.pipe(uploadStream);
//       }); 


//   }






	// 	function handleError(err) {
	// 	console.error(err);
	// 	res.statusCode = 500;
	// 	res.send({ message: 'Some Server error occured: custom Message' });
	// 	next();
	// };

	// function handlingError(cb) {
	// 	return function (err, result) {
	// 		if (err) {
	// 			handleError(err);
	// 		} else {
	// 			cb(result);
	// 		}
	// 	};
	// }


