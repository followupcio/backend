var mongodb = require('mongodb');
var GridFSBucket = require('mongodb').GridFSBucket;
var busboy = require('busboy');
var mongoClient = require('mongodb').MongoClient;
var objectID = mongodb.ObjectID;
var config = require('config');
var BasicValidation = require('../common/basicValidation');
var ErrorHandler = require('../common/error');
var Database = require('../common/database');

module.exports = function download(req, res, next) {

	var fid = req.params.id;
	//var cid = req.params.id;
	var objectID = require('mongodb').ObjectID;

    BasicValidation.userHasPermision()
        .then(function (hasPermission) {          

                    Database().connect()                  
                        .then(downloadFile)
                        .catch(ErrorHandler.handleError(res));
               
        }, ErrorHandler.handleCustomError(res, 401));//user doesnt have permission

function downloadFile(db){

      var bucket = new GridFSBucket(db, { bucketName: 'gridfsupload' });
      var id = mongodb.ObjectID(fid);
		  var downloadStream = bucket.openDownloadStream(id);

		downloadStream.on('file', function (file) {
			console.log('downloading:', file);
		    res.status(200);
		    res.set({
		      'Content-Type': file.contentType,
		      'Content-Length': file.length,
		      'Content-Disposition': 'inline;filename="' + encodeURIComponent(file.filename) + '"'
		    });
		
	});

	downloadStream.on("error", function(err) {
							console.log("Got error while processing stream " + err.message);
							res.end();
					});

		downloadStream.pipe(res).once('finish', function () {
			console.log('download complete');
			db.close();
			next();
		});
   
}

		function handleError(err) {
		console.error(err);
		res.statusCode = 500;
		res.send({ message: 'Some Server error occured: custom Message' });
		next();
	};

	function handlingError(cb) {
		return function (err, result) {
			if (err) {
				handleError(err);
			} else {
				cb(result);
			}
		};
	}

}