var mongodb = require('mongodb');
var GridFSBucket = require('mongodb').GridFSBucket;
var busboy = require('busboy');
var mongoClient = require('mongodb').MongoClient;
var objectID = mongodb.ObjectID;
var config = require('config');
var BasicValidation = require('../common/basicValidation');
var ErrorHandler = require('../common/error');
var Database = require('../common/database');

module.exports = function uploadCreation(req, res, next) {

    var cid = req.params.commitmentid;
    var closure = req.query.closure;
    var objectID = require('mongodb').ObjectID;
    var closing;



    BasicValidation.userHasPermision()
        .then(function (hasPermission) {
            console.log("Qui inizio upload");

            Database().connect()
                .then(uploadCallBack)
                .catch(ErrorHandler.handleError(res));



        }, ErrorHandler.handleCustomError(res, 401));//user doesnt have permission





    function uploadCallBack(db) {
        console.log("In callback");
        var bucket = new GridFSBucket(db, { bucketName: 'gridfsupload' });
        var fileStream = new busboy({ headers: req.headers });
        req.pipe(fileStream);

        fileStream.on('file', function (fieldname, readStream, filename, encoding, contentType) {
            console.log('uploading:', filename);
            var uploadStream = bucket.openUploadStream(filename, {
                contentType: contentType,
                metadata: closing
            });

            uploadStream.on('error', function (err) {
                console.log('upload error:', err);
                res.send(500, err);
                next(err);
            });

            uploadStream.once('finish', function () {
                console.log('upload complete:', uploadStream.id);
                console.log("Returning the result......")
                res.send(201, { message: 'attachment added', 'attachmentId': uploadStream.id });

            });

            readStream.pipe(uploadStream);
        });
    }




}






