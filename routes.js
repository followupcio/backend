var server = require('./server');
var resources = require('./resources');
var validating = require('./middleware/validating');
var schemas = require('./schemas');

//Meetings
server.post('/meetings', validating(schemas.meeting), resources.meeting.create);
server.get('/meetings/:id', resources.meeting.get);
server.get('/meetings', resources.meeting.list);
server.patch('/meetings/:id', validating(schemas.meetingUpdate),resources.meeting.update);

//Meeting Commitment
server.post('/meetings/:meetingid/demands', validating(schemas.commitment), resources.meeting.commitments.create);
server.patch('/meetings/demands/:id', validating(schemas.commitmentUpdate), resources.commitments.update);
server.del('/meetings/:meetingid/demands/:id', resources.meeting.commitments.del);
server.get('/meetings/:meetingid/demands', resources.meeting.commitments.list);

//Meeting Agenda
server.del('/meetings/:meetingid/agenda/:id', resources.meeting.agenda.del);

//Commitments

server.get('/demands', resources.commitments.list);
server.get('/demands/:id', resources.commitments.get);
server.post('/demands', validating(schemas.commitment), resources.commitments.create);
server.patch('/demands/:id', validating(schemas.commitmentUpdate),resources.commitments.update);
server.post('/demands/:id/read',resources.commitments.read);
server.get('/demands/:id/multiple',resources.commitments.getMultiple);
//server.post('/demands/:id/reject',resources.commitments.reject);
server.del('/demands/:id', resources.commitments.del);

//ACTIONS
server.post('/demands/:commitmentid/actions/create',validating(schemas.action),resources.commitments.actions.create);
server.post('/demands/:commitmentid/actions/list',resources.commitments.actions.list);


// server.get('/assignedToCommitments/:userId', resources.commitments.listCommitmentsAssignedTo);
// server.get('/createdByCommitments/:userId', resources.commitments.listCommitmentsCreatedBy);

//Users
server.get('/users', resources.users.list);
//server.get('/usersWithStats', resources.users.listWithStats);
server.get('/users/:id', resources.users.get);
//server.post('/users/create/', validating(schemas.user),resources.users.create);
//server.put('/users/update/:id',validating(schemas.user), resources.users.update);
//server.del('/users/delete/:id', resources.users.del);
server.get('/fsdUsers/:id', resources.users.fsdUser);

server.get('/trustedUsersList/:ownerId', resources.users.trustedList);
server.patch('/trustedUserAdd/:ownerId', validating(schemas.userListAdd), resources.users.trustedUserAdd);
server.patch('/trustedUserRemove/:ownerId', validating(schemas.userListRemove), resources.users.trustedUserRemove);

server.get('/basicDemandStat', resources.dashboard.basicStat);
server.get('/demandStatByDate', resources.dashboard.demandStatByDate);
server.get('/demandStatByCategory', resources.dashboard.demandStatByCategory);
server.get('/userDemandStatList', resources.dashboard.listUsersStatList);
server.get('/trustedUserStat/:ownerid', resources.dashboard.trustedUserStat);

//server.get('/assignedToUsers/:commitmentCreatyedByUserId', resources.users.listCommitmentUsersByUser); //after authentication, we will remove :ownerId and get it from token

//Resource
server.get('/resources/:type/:locale/:label', resources.resource.list);
server.get('/resources/:type/:locale', resources.resource.list);
server.get('/childresources/:type/:locale/:parentId', resources.resource.listChild);
server.get('/tags/:tag', resources.resource.tagList);

//Attachments

server.post('/demands/:commitmentid/attachments/upload', resources.attachments.upload);
server.post('/demands/attachments/uploadcreation', resources.attachments.uploadCreation);
server.get('/demands/:commitmentid/attachments', resources.attachments.list); //returns list of attachement with attachement name and ID
server.get('/attachments/:id', resources.attachments.download); //dowloads particulat attachement

//Authentication
server.post('/login', validating(schemas.login), resources.authentication.login); 
// server.get('/login', resources.authentication.logout);