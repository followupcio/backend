const axios = require('axios');

if (typeof localStorage === "undefined" || localStorage === null) {
  let LocalStorage = require('node-localstorage').LocalStorage;
  localStorage = new LocalStorage('./storage');
}

/**
 * Sets the default URL for API Calls
 * and sets fixed headers for JWT Auth
 * @type {[type]}
 */
const axiosInstance = axios.create({
  baseURL: "http://10.202.35.131:3001/api",
  headers: {
    'Accept': 'application/json',
    'Authorization': ''
  }
});

axiosInstance.interceptors.request.use(function (config) {

  const token = localStorage.getItem('token');

  if (token) {
    config.headers['Authorization'] = token
  }

  return config
});

module.exports = axiosInstance;
