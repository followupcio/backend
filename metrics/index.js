const axios = require('./axios');
const moment = require('moment');
const _ = require('underscore');
const chalk = require('chalk');
const log = console.log;
const Timeframe = require("../common/repositories/Demands/Timeframe");

if (typeof localStorage === "undefined" || localStorage === null) {
  let LocalStorage = require('node-localstorage').LocalStorage;
  localStorage = new LocalStorage('./storage');
}

const includeWithDemand = [
  //{relation: "issuer"},
  {relation: "receiver"},
  //{relation: "readBy"},
  //{relation: "iterations"},
  //{relation: "attachments"},
  /*{
   relation: "children", scope: {
   include: [
   //{relation: "attachments"},
   {relation: "issuer"},
   {relation: "iterations"},
   {relation: "readBy"},
   {relation: "receiver"},
   {relation: "children"},
   {relation: "requester"},
   {relation: "category"},
   {relation: "subcategory"},
   {relation: "events", scope: {include: ["author", "attachments"]}},
   ]
   }
   },
   {relation: "requester"},
   {relation: "category"},
   {relation: "subcategory"},*/
  {relation: "events", scope: {include: ["author", "attachments"]}},
];

let authenticate = (username = "graziano", password = "fake") =>
  axios.post("employees/login", {username, password})
    .then(response => {
      localStorage.setItem("token", response.data.id);
    });

let getDemands = () =>
  axios.get("demands?filter=" + JSON.stringify({include: includeWithDemand}));

let avg = (arr) => _.reduce(arr, function (memo, num) {
  return memo + num;
}, 0) / (arr.length === 0 ? 1 : arr.length);

let getAvgMinMax = (arr) => {

  let durations = _.map(arr, d => {
    const from = moment(d.from);
    const to = moment(d.to);
    return to.diff(from)
  });

  let maxDuration = _.max(durations);
  let minDuration = _.min(durations);
  let avgDuration = avg(durations);
  let momentAvgDuration = moment.duration(parseInt(avgDuration));
  let momentMinDuration = moment.duration(parseInt(minDuration));
  let momentMaxDuration = moment.duration(parseInt(maxDuration));

  return {
    max: {
      days: momentMaxDuration.asDays().toFixed(2),
      hours: momentMaxDuration.asHours().toFixed(2)
    },
    min: {
      days: momentMinDuration.asDays().toFixed(2),
      hours: momentMinDuration.asHours().toFixed(2)
    },
    avg: {
      days: momentAvgDuration.asDays().toFixed(2),
      hours: momentAvgDuration.asHours().toFixed(2)
    }
  };

};

let getTimeframeInfo = (arr) => {

  let demands = _.map(arr, d => {
    return Object.assign(d, {timeframe: Timeframe.calculate(d, new Date(moment(d.performed_at).toDate()))})
  });

  return _.groupBy(demands, "timeframe");

};

let printMaxMinAvgInfo = (info) => {
  log("Max: ", info.max.hours + "h (" + info.max.days + " days)");
  log("Min: ", info.min.hours + "h (" + info.min.days + " days)");
  log("Avg: ", info.avg.hours + "h (" + info.avg.days + " days)");
};

let printTimefameInfo = (info) => {

  _.each(info, (arr, timeframe) => {
    log(timeframe + ": ", arr.length);
  })

};

let getMostRecentEvent = ({demand, types}) => {

  const events = demand.events || [];
  const ordered = [];
  const indexes = [];

  types.forEach(type => {
    const event = events.find(e => e.type === type) || {};
    let index = events.indexOf(event);
    if (index === -1) {
      index = Infinity;
    }
    ordered[index] = event;
    indexes.push(index);
  });

  const mostRecentEvent = ordered[Math.min.apply(null, indexes)] || {};

  return {
    description: mostRecentEvent.description,
    attachments: mostRecentEvent.attachments || [],
    event: mostRecentEvent
  };

};

let printDeadline = (demands) => {

  let durations = _.map(demands, d => {
    return {
      from: moment(d.created_at),
      to: moment(d.deadline),
      deadline: d.deadline,
    }
  });
  let info = getAvgMinMax(durations);

  printSubtitle("Deadline duration metrics");
  printMaxMinAvgInfo(info);
  printTimefameInfo(getTimeframeInfo(durations));


};

let printDemandsAmount = (demands) => {

  log("TOTAL: ", demands.length);

};

let printAmountByReceiver = (demands) => {

  let groups = _.groupBy(demands, "assigned_to");
  let couples = [];

  _.forEach(groups, (arr) => {

    const receiver = arr[0].receiver;
    const display_name = receiver.display_name;

    couples.push({
      display_name,
      amount: arr.length
    })
  });

  couples.sort(compare);

  function compare(a, b) {
    if (a.amount > b.amount)
      return -1;
    if (a.amount < b.amount)
      return 1;
    return 0;
  }

  _.forEach(couples, (obj, index) => {
    log(index + 1 + ")", obj.display_name, obj.amount);
  });

};

let printDemandAmountByStatus = (demands) => {

  let groups = _.groupBy(demands, "status");

  printSubtitle("Amounts by Status");

  _.forEach(groups, (arr, status) => {
    log(status, arr.length);
  })

};

let printDemandAmountByType = (demands) => {

  let parents = demands.filter(d => !!d.parent);
  let children = demands.filter(d => !!d.parent_id);
  let single = demands.filter(d => !d.parent_id && !d.parent);

  printSubtitle("Amounts by Type");
  log("Parents", parents.length);
  log("Children", children.length);
  log("Single", single.length);

};

let printCompletionDuration = (completed) => {

  completed = _.map(completed, demand => {
    let approveEvent = getMostRecentEvent({
      demand,
      types: ["APPROVE"]
    }).event;

    let submitEvent = getMostRecentEvent({
      demand,
      types: ["SUBMIT"]
    }).event;

    return {
      created_at: demand.created_at,
      deadline: demand.deadline,
      approved_at: approveEvent.created_at,
      submitted_at: submitEvent.created_at,
    }

  });

  let submitted = _.map(completed, d => ({
    from: d.created_at,
    to: d.submitted_at,
    deadline: d.deadline
  }));
  let submitInfo = getAvgMinMax(submitted);
  let submittedTimeframeInfo = getTimeframeInfo(submitted);

  printSubtitle("Submit duration metrics");
  printMaxMinAvgInfo(submitInfo);
  printTimefameInfo(submittedTimeframeInfo);

  let approved = _.map(completed, d => ({
    from: d.created_at,
    deadline: d.deadline,
    to: d.approved_at
  }));
  let approvedInfo = getAvgMinMax(approved);
  let completedTimeframeInfo = getTimeframeInfo(approved);

  printSubtitle("Approve duration metrics");
  printMaxMinAvgInfo(approvedInfo);
  printTimefameInfo(completedTimeframeInfo);

};

let warning = (text) => {
  log(chalk.yellow.italic(text));
};

let printTitle = (text) => {
  log(chalk.green.bold.underline(text));
};

let printSubtitle = (text) => {
  log(chalk.green(text));
};


let printGeneralInfo = (demands) => {
  printDemandsAmount(demands);
  printDemandAmountByStatus(demands);
  printDemandAmountByType(demands);
};

authenticate()
  .then(getDemands)
  .then(response => {

    let demands = response.data;

    printTitle("All demand metrics");
    warning("Considering 'ONGOING', 'COMPLETED', 'DELETED', 'DRAFT', 'parent', 'child' and 'single' demand");
    printGeneralInfo(demands);

    demands = demands.filter(d => d.status !== "DELETED" && d.status !== "DRAFT");
    printTitle("Active demand metrics");
    warning("Considering 'ONGOING', 'COMPLETED', 'parent', 'child' and 'single' demand");
    printGeneralInfo(demands);

    demands = demands.filter(d => !d.parent);
    printTitle("Assignable demand metrics");
    warning("Considering 'ONGOING', 'COMPLETED', 'child' and 'single' demand");
    printGeneralInfo(demands);
    printDeadline(demands);

    demands = demands.filter(d => d.status === "COMPLETED");
    printTitle("Completed demand metrics");
    warning("Considering 'COMPLETED', 'child' and 'single' demand");
    printGeneralInfo(demands);
    printCompletionDuration(demands);

    return;
    printTitle("Amounts by Receiver");
    printAmountByReceiver(demands);

  });
