let Mailable = req('lib/Mail/Mailable');
let CONF = req('common/email');
let moment = require("moment");

class ExpireToday extends Mailable {


  /**
   * Create a new Mail Instance
   * @param  {Object} parameter [Holds the parameters for the view]
   */
  constructor(parameters) {
    super();
    this.parameters = parameters;
  }

  /**
   * Build the message.
   *
   * @return $this
   */
  static build(mail) {
    let params = mail.parameters || {};
    let demand = params.demand || {};
    let category = demand.category || {};
    let receiver = demand.receiver || {};
    let mail_receiver = receiver;

    let subject = !!params.iterations ?
      `${CONF.PROJECT_ACRONYM}: A recurring delivery expires today - [ ${receiver.display_name} - ${category.label}]` :
      `${CONF.PROJECT_ACRONYM}: An assignment expires today - [ ${receiver.display_name} - ${category.label}]`;

    let deepLink = `${CONF.BASE_URL}/demands/${demand.id}`;

    return mail.view('expireToday')

      .inject({
        deepLink: deepLink,
        title: demand.title,
        deadline: new moment(new Date(demand.deadline)).format("Do MMMM YYYY"),
        project_name: CONF.PROJECT_NAME,
        project_acronym: CONF.PROJECT_ACRONYM,
        sender: CONF.SENDER_NAME,
        iterations: params.iterations
      })
      .from(CONF.FROM)
      .to(mail_receiver.email)
      .title(subject)
      .bcc(CONF.BCC)
      .cc(CONF.CC)

  }

}
module.exports = ExpireToday;
