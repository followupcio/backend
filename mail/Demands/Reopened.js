let Mailable = req('lib/Mail/Mailable');
let CONF = req('common/email');

class Reopened extends Mailable {


  /**
   * Create a new Mail Instance
   * @param  {Object} parameter [Holds the parameters for the view]
   */
  constructor(parameters) {
    super();
    this.parameters = parameters;
  }

  /**
   * Build the message.
   *
   * @return $this
   */
  static build(mail) {

    let params = mail.parameters || {};
    let demand = params.demand || {};
    let category = demand.category || {};
    let receiver = demand.receiver || {};
    let issuer = demand.issuer || {};
    let mail_receiver = receiver;

    let subject = !!params.iteration ?
      `${CONF.PROJECT_ACRONYM}: Delivery reopened - [ ${receiver.display_name} - ${category.label}]` :
      `${CONF.PROJECT_ACRONYM}: Assignment reopened - [ ${receiver.display_name} - ${category.label}]`;
    let deepLink = `${CONF.BASE_URL}/demands/${demand.id}`;

    let inject = {
      deepLink: deepLink,
      title: demand.title,
      iteration: params.iteration,
      project_name: CONF.PROJECT_NAME,
      sender: CONF.SENDER_NAME,
      project_acronym: CONF.PROJECT_ACRONYM
    };

    /*
     * Mode 1 : the demand assignee reopen the demand
     * Mode 2 : the demand creator reopen the demand
     * Mode 3 a third person reopen the demand
     * */

    /*
     * Mode 1
     * */
    if (params.author_id === receiver.id) {
      inject.performedByTheReceiver = true;
      inject.action_actor = receiver;
      inject.mail_receiver = issuer;

    }

    /*
     * Mode 2
     * */
    if (params.author_id === issuer.id) {
      inject.performedByTheIssuer = true;
      inject.action_actor = issuer;
      inject.mail_receiver = receiver;
    }

    /*
     * Mode 3
     * */
    if (params.author_id !== issuer.id && params.author_id !== receiver.id) {
      inject.performedByThirdPerson = true;
      inject.action_actor = {
        display_name: CONF.SENDER_NAME
      };
      inject.mail_receiver = issuer;
    }

    return mail
      .view('reopen')
      .inject(inject)
      .from(CONF.FROM)
      .to(mail_receiver.email)
      .title(subject)
      .bcc(CONF.BCC)
      .cc(CONF.CC)

  }

}
module.exports = Reopened;
