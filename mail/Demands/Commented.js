let Mailable = req('lib/Mail/Mailable');
let CONF = req('common/email');

class Commented extends Mailable {

  /**
   * Create a new Mail Instance
   * @param  {Object} parameter [Holds the parameters for the view]
   */
  constructor(parameters) {
    super();
    this.parameters = parameters;
  }

  /**
   * Build the message.
   *
   * @return $this
   */
  static build(mail) {

    let params = mail.parameters || {};
    let demand = params.demand || {};
    let category = demand.category || {};
    let receiver = demand.receiver || {};
    let issuer = demand.issuer || {};
    let action_actor = receiver || {};
    let mail_receiver = issuer || {};

    let subject = `${CONF.PROJECT_ACRONYM}: An assignment has been commented - [ ${receiver.display_name} - ${category.label}]`;
    let deepLink = `${CONF.BASE_URL}/demands/${demand.id}`;

    if (params.author_id === issuer.id) {

      action_actor = issuer;
      mail_receiver = receiver;

      return mail.view('commentByDirector')
        .inject({
          deepLink: deepLink,
          title: demand.title,
          project_name: CONF.PROJECT_NAME,
          project_acronym: CONF.PROJECT_ACRONYM,
          sender: CONF.SENDER_NAME,
          mail_receiver : mail_receiver,
          action_actor: action_actor
        })
        .from(CONF.FROM)
        .to(mail_receiver.email)
        .title(subject)
        .bcc(CONF.BCC)
        .cc(CONF.CC)
    }

    if (params.author_id === receiver.id) {

      action_actor = receiver;
      mail_receiver = issuer;

      return mail.view('comment')
        .inject({
          deepLink: deepLink,
          title: demand.title,
          project_name: CONF.PROJECT_NAME,
          project_acronym: CONF.PROJECT_ACRONYM,
          mail_receiver : mail_receiver,
          action_actor: action_actor
        })
        .from(CONF.FROM)
        .to(mail_receiver.email)
        .title(subject)
        .bcc(CONF.BCC)
        .cc(CONF.CC)
    }

    mail_receiver = receiver;

    return mail.view('commentGeneric')
      .inject({
        deepLink: deepLink,
        title: demand.title,
        project_name: CONF.PROJECT_NAME,
        project_acronym: CONF.PROJECT_ACRONYM,
        mail_receiver : mail_receiver
      })
      .from(CONF.FROM)
      .to(mail_receiver.email)
      .title(subject)
      .bcc(CONF.BCC)
      .cc(CONF.CC)
  }

}

module.exports = Commented;
