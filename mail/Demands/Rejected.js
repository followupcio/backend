let Mailable = req('lib/Mail/Mailable');
let CONF = req('common/email');

class Rejected extends Mailable {


  /**
   * Create a new Mail Instance
   * @param  {Object} parameter [Holds the parameters for the view]
   */
  constructor(parameters) {
    super();
    this.parameters = parameters;
  }

  /**
   * Build the message.
   *
   * @return $this
   */
  static build(mail) {

    let params = mail.parameters || {};
    let demand = params.demand || {};
    let category = demand.category || {};
    let receiver = demand.receiver || {};
    let issuer = demand.issuer || {};
    //let action_actor = issuer || {};
    let mail_receiver = receiver || {};

    let subject = !!params.iteration ?
      `${CONF.PROJECT_ACRONYM}: Delivery rejected - [ ${receiver.display_name} - ${category.label}]` :
      `${CONF.PROJECT_ACRONYM}: Assignment rejected - [ ${receiver.display_name} - ${category.label}]`;
    let deepLink = `${CONF.BASE_URL}/demands/${demand.id}`;

    return mail
      .view('reject')
      .inject({
        deepLink: deepLink,
        title: demand.title,
        project_name: CONF.PROJECT_NAME,
        project_acronym: CONF.PROJECT_ACRONYM,
        mail_receiver : mail_receiver,
        sender: CONF.SENDER_NAME,
        //action_actor: action_actor,
        iteration: params.iteration
      })
      .from(CONF.FROM)
      .to(mail_receiver.email)
      .title(subject)
      .bcc(CONF.BCC)
      .cc(CONF.CC)

  }

}
module.exports = Rejected;
