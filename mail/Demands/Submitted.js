let Mailable = req('lib/Mail/Mailable');
let Demand = req('common/repositories/Demands/Demand');
let CONF = req('common/email');

class Submitted extends Mailable {


  /**
   * Create a new Mail Instance
   * @param  {Object} parameter [Holds the parameters for the view]
   */
  constructor(parameters) {
    super();
    this.parameters = parameters;
  }

  /**
   * Build the message.
   *
   * @return $this
   */
  static build(mail) {

    let params = mail.parameters || {};
    let demand = params.demand || {};
    let category = demand.category || {};
    let receiver = demand.receiver || {};
    let issuer = demand.issuer || {};
    let action_actor = receiver;
    let mail_receiver =issuer;

    let subject = !!params.iteration ?
      `${CONF.PROJECT_ACRONYM}: Delivery submitted for approval - [ ${receiver.display_name} - ${category.label}]` :
      `${CONF.PROJECT_ACRONYM}: Assignment submitted for approval - [ ${receiver.display_name} - ${category.label}]`;

    let deepLink = `${CONF.BASE_URL}/demands/${demand.id}`;

    return mail
      .view('submit')
      .inject({
        deepLink: deepLink,
        title: demand.title,
        iteration: params.iteration,
        project_name: CONF.PROJECT_NAME,
        project_acronym: CONF.PROJECT_ACRONYM,
        sender: CONF.SENDER_NAME,
        mail_receiver : mail_receiver,
        action_actor: action_actor
      })
      .from(CONF.FROM)
      .to(mail_receiver.email)
      .title(subject)
      .bcc(CONF.BCC)
      .cc(CONF.CC)

  }

}
module.exports = Submitted;
