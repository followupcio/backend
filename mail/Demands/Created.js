let Mailable = req('lib/Mail/Mailable');
let moment = require("moment");
let Timeframe = req("common/repositories/Demands/Timeframe");
let CONF = req('common/email');

class Created extends Mailable {


  /**
   * Create a new Mail Instance
   * @param  {Object} parameter [Holds the parameters for the view]
   */
  constructor(parameters) {
    super();
    this.parameters = parameters;
  }

  /**
   * Build the message.
   *
   * @return $this
   */
  static build(mail) {

    let params = mail.parameters || {};
    let demand = params.demand || {};
    let category = demand.category || {};
    let receiver = demand.receiver || {};
    let issuer = demand.issuer || {};
    let action_actor = issuer || {};
    let mail_receiver = receiver || {};

    let subject = `${CONF.PROJECT_ACRONYM}: New assignment - [${receiver.display_name} - ${category.label}]`;
    let deepLink = `${CONF.BASE_URL}/demands/${demand.id}`;

    let dates = demand.repeatDates || [];
    dates = dates.map((d) => ({
      timeframe : Timeframe.calculate({deadline : d}),
      deadline : moment(d).format('Do MMMM YYYY'),
      raw_deadline : d
    }));
    dates = dates.sort((a, b) => new Date(a.raw_deadline) - new Date(b.raw_deadline));

    return mail
      .view('creation')
      .inject({
        deepLink: deepLink,
        title: demand.title,
        project_name: CONF.PROJECT_NAME,
        project_acronym: CONF.PROJECT_ACRONYM,
        sender: CONF.SENDER_NAME,
        mail_receiver : mail_receiver,
        action_actor: action_actor,
        dates: dates
      })
      .from(CONF.FROM)
      .to(mail_receiver.email)
      .title(subject)
      .bcc(CONF.BCC)
      .cc(CONF.CC)
  }

}
module.exports = Created;
