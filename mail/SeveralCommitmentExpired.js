let Mailable = req('lib/Mail/Mailable');
let CONF = req('common/email');
let moment = require("moment");

class SeveralCommitmentExpired extends Mailable {


  /**
   * Create a new Mail Instance
   * @param  {Object} parameter [Holds the parameters for the view]
   */
  constructor(parameters) {
    super();
    this.parameters = parameters;
  }

  /**
   * Build the message.
   *
   * @return $this
   */
  static build(mail) {

    let params = mail.parameters || {};
    let mail_receiver = params.receiver || {};
    let demands = params.demands || [];

    let subject = !!params.iteration ?
      `${CONF.PROJECT_ACRONYM}: Several recurring deliveries expired - [ ${mail_receiver.display_name}]` :
      `${CONF.PROJECT_ACRONYM}: Several assignments expired - [ ${mail_receiver.display_name}]`;
    let deepLink = `${CONF.BASE_URL}`;

    demands = demands.map( d => {
      return Object.assign({}, d, {
        deadline : new moment(new Date(d.deadline)).format("Do MMMM YYYY"),
      })
    });

    return mail.view('severalCommitmentExpired')
      .inject({
        iterations: params.iterations,
        demands: demands,
        deepLink: deepLink,
        project_name: CONF.PROJECT_NAME,
        project_acronym: CONF.PROJECT_ACRONYM,
        sender: CONF.SENDER_NAME,
      })
      .from(CONF.FROM)
      .to(mail_receiver.email)
      .title(subject)
      .bcc(CONF.BCC)
      .cc(CONF.CC)

  }

}

module.exports = SeveralCommitmentExpired;
