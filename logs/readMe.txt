requestLog stores all request till last 5 days.
auditLog stores all audits for last 5 days.
generalLog stores all the caught errors for last 10 days.
uncaughtException stores rest of error that was not captured and stores for last 30 days.