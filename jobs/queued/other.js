var Queue = require('lib/Jobs/Queue')
let CommitmentExpired = require('mail/CommitmentExpired');

module.exports = function() {
    var queue = Queue.create();
    // console.log(queue);
    function newJob() {
        var job = queue.create('new2_job');
        job.save();
    }
    queue.process('new2_job', function(job, done) {
        console.log('The email-job was sent', job.id, 'is done');

        (new CommitmentExpired({'text':'This is a parameter sent to the mail'})).send();
        done && done();
    });
    setInterval(newJob, 6000);
}