let GetExpired = req('common/repositories/Demands/GetExpired');
let Expired = req('mail/CommitmentExpired');
let severalExpired = req('mail/SeveralCommitmentExpired');
let _ = require('lodash');
let CronJob = require('cron').CronJob;

let SendExpiredEmail = function () {
  let job = new CronJob({
    cronTime: process.env.AGENDA_CRON,
    onTick: function () {
      handle()
    },
    start: true,
    timeZone: 'Europe/Rome'
  });
  job.start();
};

/**
 * [SendExpiredEmail description]
 */
let handle = function () {
  GetExpired.get().then(function (result) {

    let transformedAssignee = _.groupBy(result, 'assigned_to');
    transformedAssignee = JSON.parse(JSON.stringify(transformedAssignee));

    Object.keys(transformedAssignee).forEach(function (key) {

      let demands = transformedAssignee[key];
      let email;
      let demand = demands[0];

      if (demands.length === 1) {

        email = new Expired({
          demand: demand
        });

      }
      else {

        email = new severalExpired({
          receiver: demand.receiver,
          demands: demands,
        });

      }
      email.send();
    });

  }).catch(function (error) {
    console.log('There was and error', error);
  });
};

module.exports = SendExpiredEmail;


