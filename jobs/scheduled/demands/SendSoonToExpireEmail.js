let GetSoonToExpire = req('common/repositories/Demands/GetSoonToExpire');
let SoonToExpire = req('mail/SoonToExpire');
let CronJob = require('cron').CronJob;

let SendSoonToExpireEmail = function () {

  let job = new CronJob({
    cronTime: process.env.AGENDA_CRON,
    onTick: function () {
      handle()
    },
    start: true,
    timeZone: 'Europe/Rome'
  });
  job.start();
};

/**
 * [SendSoonToExpireEmail description]
 */
let handle = function () {

  GetSoonToExpire.get({datasource: 'db'}).then(function (result) {

    result.forEach(function (demand) {
      demand = JSON.parse(JSON.stringify(demand));

      if (!demand.receiver || !demand.receiver.email) {
        return
      }

      let email = new SoonToExpire({
        demand: demand
      });

      email.send();
    });

  }).catch(function (error) {
    console.log('There was an error', error);
  });
};

module.exports = SendSoonToExpireEmail;
