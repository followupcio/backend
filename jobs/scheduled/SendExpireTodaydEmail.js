let getExpireToday = require('resources/commitments/getExpireToday');
let ExpireToday = require('mail/ExpireToday');

/**
 * [SendExpireTodayEmail description]
 * @param {[type]} agenda [description]
 */
let SendExpireTodayEmail = function (agenda) {
    // otherQueue();

    agenda.define('SendExpireTodayEmail', function (job, done) {
        getExpireToday().then(function (result) {
          
            var deeplink;
            result.forEach(function (assignee) {


                if (process.env.environment == "development") deeplink = "https://hqldvodg1.hq.un.fao.org/dev/demands/" + assignee.cid;
                else deeplink = "https://dgms.fao.org/demands/" + assignee.cid;

                console.log('#######################################################');
                console.log('Email sent SendExpireTodayEmail');
                console.log('#######################################################');
                console.log(new Date());
                var email = new ExpireToday({ 'email': assignee.email, 'deeplink': deeplink });

                email.send();
            });


            done();
        }).catch(function (error) {
            console.log('There was and error', error);
            done();
        });
    });

    agenda.now('SendExpireTodayEmail');

    //agenda.every("1 9 * * 1-5", 'SendExpireTodayEmail');
}
module.exports = SendExpireTodayEmail