let GetExpireToday = req('common/repositories/Iterations/GetExpireToday');
let ExpireToday = req('mail/ExpireToday');
let CronJob = require('cron').CronJob;

let SendExpireTodayEmail = function () {

  let job = new CronJob({
    cronTime: process.env.AGENDA_CRON,
    onTick: function () {
      handle()
    },
    start: true,
    timeZone: 'Europe/Rome'
  });
  job.start();
};

/**
 * [SendExpireTodayEmail description]
 */
let handle = function () {
  // otherQueue();

  GetExpireToday.get({datasource: 'db'}).then(function (result) {

    result.forEach(function (demand) {
      demand = JSON.parse(JSON.stringify(demand));

      if (!demand.receiver || !demand.receiver.email) {
        return
      }

      let email = new ExpireToday({
        demand: demand,
        iterations: true,
      });

      email.send();
    });
  }).catch(function (error) {
    console.log('There was and error', error);
  });

};

module.exports = SendExpireTodayEmail;
