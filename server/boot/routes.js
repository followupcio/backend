module.exports = function (app) {
  var router = app.loopback.Router();
  router.get('/api/health', function (req, res) {
    res.send({running: true});
  });
  app.use(router);
}
