let connectionUrl = 'mongodb://' + process.env.DB_USERNAME + ':' + process.env.DB_PASSWORD + '@' + process.env.DB_HOST + ":" +process.env.DB_PORT + '/' + process.env.DB_DATABASE;

if (process.env.DB_URL_STRING_PARAMETERS){
  connectionUrl += "?" + process.env.DB_URL_STRING_PARAMETERS;
}

module.exports = {
  db: {
    name: 'db',
    connector: 'memory'
  },
  mongodb: {
    host: process.env.DB_HOST,
    port: process.env.DB_PORT,
    url: connectionUrl,
    database: process.env.DB_DATABASE,
    password: process.env.DB_PASSWORD,
    name: "mongodb",
    user: process.env.DB_USERNAME,
    connector: "mongodb"
  },
  storage: {
    name: 'storage',
    connector: 'loopback-component-storage',
    provider: 'filesystem',
    root: './files',
    maxFileSize: "10485760"
  },
  // s3storage: {
  //   name: process.env.AWS_BUCKET_NAME,
  //   region: process.env.AWS_BUCKET_REGION,
  //   connector: "loopback-component-storage",
  //   provider: "amazon",
  //   key: process.env.AWS_SECRET_ACCESS_KEY,
  //   keyId: process.env.AWS_ACCESS_KEY_ID,
  //   nameConflict: 'makeUnique'
  // }
};
