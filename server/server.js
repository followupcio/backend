'use strict';
global.req = require('app-root-path').require;
var loopback = require('loopback');
var boot = require('loopback-boot');
global.dd = require('dump-die');
let Agenda = require('../lib/Jobs/Agenda');
let Mails = require('../lib/Mail/Mailable')
var app = module.exports = loopback();
require('dotenv-safe').load({
  allowEmptyValues: true
});

/**
 * Processes All Jobs defined By the agenda.
 */

if (process.env.AGENDA_SEND_EMAIL === 'true') {
  Mails.process();
}

if (process.env.AGENDA_SEND_SCHEDULED_EMAIL  === 'true'){
  Agenda.process();
}

app.start = function () {
  // start the web server
  return app.listen(function () {
    app.emit('started');
    let baseUrl = app.get('url').replace(/\/$/, '');
    console.log('Web server listening at: %s', baseUrl);
    if (app.get('loopback-component-explorer')) {
      let explorerPath = app.get('loopback-component-explorer').mountPath;
      console.log('Browse your REST API at %s%s', baseUrl, explorerPath);
    }
  });
};
// Bootstrap the application, configure models, datasources and middleware.
// Sub-apps like REST API are mounted via boot scripts.
//boot(app, __dirname, function(err) {
// if (err) throw err;
// start the server if `$ node server.js`
//
//
boot(app, {
  appRootDir: __dirname,
}, (err) => {
  if (err) {
    throw err;
  }

  // start the server if `$ node server.js`
  if (require.main === module) {
    // Extend the life of the access token to 30 minutes from now when a new
    // call is made using it.
    app.use(loopback.token({
      model: app.models.AccessToken,
    }));
    app.use((req, res, next) => {
      const token = req.accessToken;


      if (!token) {
        return next();
      }
      const now = new Date();

      /* uncomment to have refresh window

       const VALIDITY_PERCENTAGE_WINDOW = 0.8;
       if (now.getTime() - token.created.getTime() < token.ttl * 1000 *VALIDITY_PERCENTAGE_WINDOW) {
       return next();
       }*/
      req.accessToken.created = now; // eslint-disable-line
      return req.accessToken.save(next);
    });
    app.start();
  }
});
