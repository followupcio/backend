require('dotenv-safe').load({allowEmptyValues: true});

class Agenda {
  /**
   *
   */
  static process() {

    /*
     |--------------------------------------------------------------------------
     | App Agenda
     |--------------------------------------------------------------------------
     |
     | Here you may define all the Jobs that you need to schudle and process
     | in Agenda by putting theme in the /scheduled Folder. Jobs will give
     | you a convenient way to create tasks that you want to execute.
     | File free to add as many folders or subfolders as you want.
     */

    //Demands
    require('../../jobs/scheduled/demands/SendExpiredEmail')();
    require('../../jobs/scheduled/demands/SendSoonToExpireEmail')();
    require('../../jobs/scheduled/demands/SendExpireTodayEmail')();

    //Iterations
    require('../../jobs/scheduled/iterations/SendExpiredEmail')();
    require('../../jobs/scheduled/iterations/SendSoonToExpireEmail')();
    require('../../jobs/scheduled/iterations/SendExpireTodayEmail')();
    /* ---------------- End of App Jobs*----------------*/

  }

}
module.exports = Agenda;
