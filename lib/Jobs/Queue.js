var Bull = require('bull');

class Queue {

	/**
	 * Generates a new Bulljs Queue
	 * instance
	 * @return {Queue} [Bulljs instance]
	 */
	 static create() {
	 	var queue = new Bull(
	 		'bull_queue', 
	 		{
	 			redis: 
	 			{
	 				port: process.env.REDIS_PORT,
	 				 host: process.env.REDIS_HOST
	 			}
	 		}
	 	);
	    return queue
	  }

}

module.exports = Queue;