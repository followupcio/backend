var Queue = require('../Jobs/Queue')
var path = require('path');
var Promise = require('bluebird');
const Log = req("common/repositories/Logs/Email")
var nodemailer = require('nodemailer');
var hbs = require('nodemailer-express-handlebars');

/**
 *
 */
class Mailable {
  /**
   * [send description]
   * @return {[type]} [description]
   */
  send() {
    (this.constructor.build)(this);
    this.parameters = this[Object.keys(this)[0]];
    this.buildView().buildFrom().buildTo().buildBcc().buildCc().queue();
  }

  /**
   * Contruction the View from the Child Class
   * @param  {[type]} view [description]
   * @param  {[type]} data [description]
   * @return {[type]}      [description]
   */
  view(view, data) {
    this.view = view;
    return this;
  }

  /**
   * Pass data from the Client to the View
   * @param  {[type]} data [description]
   * @return {[type]}      [description]
   */
  inject(data) {
    this.data = data;
    return this;
  }

  /**
   * [handle description]
   * @return {[type]} [description]
   */
  queue() {
    var queue = Queue.create();
    // Creating a new Job for the email

    var job = queue.add('email',
      //This is the data object
      {
        title: this.emailTitle,
        to: this.toAddress,
        cc: this.ccAddress,
        bcc: this.bccAddress,
        from: this.fromAddress,
        template: this.view,
        injected: this.data,
        parameters: this.parameters

      },
      {
        attempts: 10,
        backoff: 60 * 1000,
        removeOnComplete: true,
        removeOnfail: false
      }
    )
    return this;
  }

  buildView() {
    return this;
  }

  /**
   * Proccesses all emails
   */
  static process() {
    let queue = Queue.create();
    let self = this;

    // Every time that we read and email from the QUEUE
    // this function gets triggered
    queue.process('email', 1, function (job, done) {
      //here we should actually send the email
      setTimeout(() => {

        let promise = self.buildAndSendEmail(job);
        promise.then(() => {
          done && done();
        }).catch((error) => {
          done(error);
        });

      }, 2000);

    });
  }

  /**
   * Gets the from information from the Child
   * Class
   * @param  {[type]} $address [description]
   * @return {[type]}          [description]
   */
  from(address) {
    this.fromAddress = address;
    return this;
  }

  /**
   * [buildFrom description]
   * @return {[type]} [description]
   */
  buildFrom() {
    this.fromAddress = this.fromAddress;
    return this;
  }

  /**
   * [to description]
   * @param  {[type]} $address [description]
   * @return {[type]}          [description]
   */
  to(address) {
    this.toAddress = address;
    return this;
  }

  /**
   * [buildTo description]
   * @return {[type]} [description]
   */
  buildTo() {
    this.toAddress = this.toAddress;
    return this;
  }

  /**
   * [to description]
   * @param  {[type]} $address [description]
   * @return {[type]}          [description]
   */
  title(title) {
    this.emailTitle = title;
    return this;
  }

  /**
   * [buildTo description]
   * @return {[type]} [description]
   */
  buildTitle() {
    this.emailTitle = this.emailTitle;
    return this;
  }

  /**
   * Get the cc from the client
   * @param  {[type]} $cc [description]
   * @return {[type]}     [description]
   */
  cc(cc) {
    this.ccAddress = cc;
    return this;
  }

  /**
   * Process the cc
   * @return {[type]} [description]
   */
  buildCc() {
    this.ccAddress = this.ccAddress;
    return this;
  }

  /**
   * Get the bcc from the client
   * @param  {[type]} $bcc [description]
   * @return {[type]}      [description]
   */
  bcc(bcc) {
    this.bccAddress = bcc;
    return this;
  }


  /**
   * Process the Bcc
   * @return {[type]} [description]
   */
  buildBcc() {
    this.bccAddress = this.bccAddress;
    return this;
  }

  /**
   * [buildEmail description]
   * @return {[type]} [description]
   */
  static buildAndSendEmail(job) {
    let self = this;
    return new Promise((resolve, reject) => {
      //Get the template for the email
      let template = path.join(__dirname, '../../templates', job.data.template);
      let options = {
        viewEngine: {
          extname: '.hbs',
          layoutsDir: template,
          defaultLayout: 'html',
          partialsDir: 'views/partials/'
        },
        viewPath: template,
        extName: '.hbs'
      };
      let mailer = nodemailer.createTransport(self.getConnectionOption());
      mailer.use('compile', hbs(options));

      //Inject the parameters given
      let context = {'data': job.data.injected, 'parameters': job.data.parameters};

      let content = {
        from: job.data.from,
        subject: job.data.title,
        template: 'html',
        to: job.data.to,
        cc: job.data.cc,
        bcc: job.data.bcc,
        context: context
      };

      Log.attempt(job, content);

      //Force a random error
      /* var random = Math.random()
       console.log("Error gen", random)
       if (random < 0.75) {
       console.log("Error thrown")
       Log.logError(job, "This is the fake error");
       reject(new Error());
       return
       }
       console.log("Tutto ok!")*/

      mailer.sendMail(content, function (error, response) {
        if (error) {
          Log.logError(job, error);
          mailer.close();
          reject(error);
        } else {
          Log.success(job, response);
          mailer.close();

          resolve(response)
        }

      });


    })
  }


  /**
   * Gets the current connection String
   * to the Mail Server
   * @return {Object} [Mail Creadentials]
   */
  static getConnectionOption() {
    //  email.server.connect(options)
    let con = {
      host: process.env.MAIL_HOST,
      port: process.env.MAIL_PORT
    };

    let user = process.env.MAIL_USERNAME;
    let pass = process.env.MAIL_PASSWORD;

    if (!!user && !!pass) {
      con.auth = {
        user: user,
        pass: pass
      }
    }

    if (process.env.MAIL_ENCRYPTION === 'true') {
      con.tls = {
        rejectUnauthorized: false // don't verify certificates
      }
      //con.tls = {rejectUnauthorized: false}
    }

    return con;

  }
}

module.exports = Mailable;
